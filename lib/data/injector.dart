import 'package:be_staff/data/repositories/busy_calendar_repositories.dart';
import 'package:be_staff/data/repositories/busy_calendar_repository_impl.dart';
import 'package:be_staff/data/repositories/cede_shift_impl.dart';
import 'package:be_staff/data/repositories/cede_shift_repositories.dart';
import 'package:be_staff/data/repositories/home_repositories.dart';
import 'package:be_staff/data/repositories/home_repositories_impl.dart';
import 'package:be_staff/data/repositories/i_request_management_repository.dart';
import 'package:be_staff/data/repositories/intro_impl.dart';
import 'package:be_staff/data/repositories/intro_repositories.dart';
import 'package:be_staff/data/repositories/login_impl.dart';
import 'package:be_staff/data/repositories/login_repositories.dart';
import 'package:be_staff/data/repositories/overtime_repositories.dart';
import 'package:be_staff/data/repositories/overtime_repositories_impl.dart';
import 'package:be_staff/data/repositories/profile_repositories.dart';
import 'package:be_staff/data/repositories/profile_repositories_impl.dart';
import 'package:be_staff/data/repositories/receive_cancel_lack_shift_impl.dart';
import 'package:be_staff/data/repositories/receive_cancel_lack_shift_repository.dart';
import 'package:be_staff/data/repositories/register_impl.dart';
import 'package:be_staff/data/repositories/register_repositories.dart';
import 'package:be_staff/data/repositories/calendar_repository_impl.dart';
import 'package:be_staff/data/repositories/calendar_repositories.dart';
import 'package:be_staff/data/repositories/request_management_impl.dart';
import 'package:be_staff/data/repositories/take_leave_impl.dart';
import 'package:be_staff/data/repositories/take_leave_repositories.dart';
import 'package:be_staff/data/repositories/timekeeping_impl.dart';
import 'package:be_staff/data/repositories/timekeeping_repositories.dart';

class Injector {
  factory Injector() {
    return Injector._internal();
  }

  Injector._internal();

  CalendarRepositories get onGetWorkingCalendar => CalendarRepositoryImpl();

  HomeRepositories get onGetHome => HomeRepositoriesImpl();

  RegisterRepositories get onPostRegister => RegisterImpl();

  LoginRepositories get onLogin => LoginImpl();

  IntroRepositories get onCheckAccount => IntroImpl();

  BusyCalendarRepositories get onGetBusyTime => BusyCalendarRepositoryImpl();

  TimeKeepingRepositories get onGetTimeKeeping => TimeKeepingImpl();

  ProfileRepositories get onGetProfile => ProfileRepositoriesImpl();

  IRequestManagementRepository get onGetRequestManagement =>
      RequestManagementImpl();

  ReceiveCancelLackShiftRepository get onGetReceiveCancelLackShift =>
      ReceiveCancelLackShiftImpl();

  CedeShiftRepositories get onGetCedeShift => CedeShiftImpl();

  TakeLeaveRepositories get onGetTakeLeave => TakeLeaveImpl();

  OvertimeRepositories get onGetOvertime => OvertimeRepositoriesImpl();
}
