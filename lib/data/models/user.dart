class User {
  String fullName;
  String email;
  String mobile;
  String code;
  String testDateAt;
  String joinDateAt;
  int salary;
  String updatedAt;
  String createdAt;
  int id;
  String avatar;
  String token;
  int userId;
  Null dept;
  String company;
  List<Null> branchName;
  int firstLogin;

  User(
      {this.fullName,
        this.email,
        this.mobile,
        this.code,
        this.testDateAt,
        this.joinDateAt,
        this.salary,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.token});

  User.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    email = json['email'];
    mobile = json['mobile'];
    code = json['code'];
    testDateAt = json['test_date_at'];
    joinDateAt = json['join_date_at'];
    salary = json['salary'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['full_name'] = this.fullName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['code'] = this.code;
    data['test_date_at'] = this.testDateAt;
    data['join_date_at'] = this.joinDateAt;
    data['salary'] = this.salary;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    data['token'] = this.token;
    return data;
  }
}