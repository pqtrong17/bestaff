import 'dart:io';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class NetworkClient {
  static Future<Response> onGet(String url,
      {Map<String, dynamic> header}) async {
    Client client = Client();
    final Response responseJson = await client.get(url, headers: header);
    client.close();
    return responseJson;
  }
  static Future<Response> onPost(String url, {String body, Map<String, dynamic> header}) async {
    Client client = Client();
    final Response responseJson =
    await client.post(url, body: body, headers: header);
    client.close();
    return responseJson;
  }

  static Future<Response> onPut(String url, {String body, Map<String, dynamic> header}) async {
    Client client = Client();
    final Response responseJson =
    await client.put(url, body: body, headers: header);
    client.close();
    return responseJson;
  }

  static Future<Response> onPostMultipart(String url, {Map<String, dynamic> header, String keyName, File file}) async {
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(
      http.MultipartFile(
        keyName,
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: file.path,
        contentType: MediaType('image','jpeg'),
      ),
    );
    request.headers.addAll(header);
    StreamedResponse res = await request.send();
    var response = await http.Response.fromStream(res);
    return response;
  }
}
