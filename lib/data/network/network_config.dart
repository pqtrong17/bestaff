import 'dart:io';

import 'package:be_staff/data/global/storage_info_user.dart';

class NetworkConfig {
  static const String URL_SERVER = "https://app.bestaff.io/api";

  static Map<String, String> onBuildHeader({bool isMultipart = false}) {
    String token = StorageInfoUser()?.getInfoUser?.token;
    final header = {
      HttpHeaders.contentTypeHeader:
          isMultipart ? 'multipart/form-data' : "application/json",
      HttpHeaders.authorizationHeader : token != null ? "Bearer $token" : null
    };
    return header;
  }

  static const String CALENDAR = URL_SERVER +"/calendar";
  static const String GET_LIST_NEWS = URL_SERVER +"/get_list_post_api";
  static const String SHIFT_USER_LIST = URL_SERVER +"/shift_user_api";
  static const String ALL_SHIFTS = URL_SERVER +"/shift_branch_api";
  static const String LACK_SHIFT_BRANCH = URL_SERVER +"/lack_shift_branch_api";
  static const String PENDING_NOTIFICATION = URL_SERVER +"/pending_notification_api";
  static const String BUSY_TIME = URL_SERVER +"/busytime_api";

  static const String REGISTER = URL_SERVER +"/register-staff";
  static const String LOGIN = URL_SERVER +"/login";
  static const String CHECK_ACCOUNT = URL_SERVER +"/checkLogin";
  static const String TIMEKEEPING = URL_SERVER +"/get_ip";
  static const String SAVE_TIMEKEEPING = URL_SERVER +"/save_timekeeper";
  static const String UPDATE_PROFILE = URL_SERVER +"/profile/update";
  static const String UPLOAD_AVATAR = URL_SERVER +"/upload";
  static const String SHIFT_OTHER_USER = URL_SERVER +"/shift_other_user_api";
  static const String CODE_COMPANY = URL_SERVER +"/code-company";
  static const String DETAIL_USER_INFO = URL_SERVER +"/get_user_info";
  static const String GET_SWITCH_SHIFT_PENDING = URL_SERVER +"/shift_switch_user_api";
  static const String ACCEPT_SWITCH_SHIFT = URL_SERVER +"/shift_done_switch_user_api";
  static const String REJECT_SWITCH_SHIFT = URL_SERVER +"/shift_cancel_switch_user_api";
  static const String RATING = URL_SERVER +"/rating";
  static const String SHIFT_ADD_TAKE_USER = URL_SERVER +"/shift_add_take_user_api";
  static const String SHIFT_CANCEL_TAKE_USER = URL_SERVER +"/shift_cancel_take_user_api";
  static const String GET_USER_FOR_CEDE_SHIFT = URL_SERVER +"/get_user_for_shift";
  static const String CEDE_SHIFT = URL_SERVER +"/shift_add_switch_user_api";
  static const String LIST_OF_TYPE_LEAVE = URL_SERVER +"/get_list_leave";
  static const String ADD_LEAVE_REQUEST = URL_SERVER +"/add_leave";
  static const String SHIFT_NOTIFICATION = URL_SERVER +"/shift_notification_api";
  static const String ADD_OVERTIME = URL_SERVER +"/add_overtime_api";
  static const String GET_INFORMATION_OVERTIME = URL_SERVER +"/get_overtime_api";
  static const String GET_EXPERIENCE = URL_SERVER +"/get_user_info_other";
}