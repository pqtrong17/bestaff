import 'dart:convert';

import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/busy_calendar_repositories.dart';
import 'package:be_staff/data/request/update_busy_calendar_request.dart';
import 'package:be_staff/data/response/busy_time_response.dart';
import 'package:http/http.dart';

class BusyCalendarRepositoryImpl implements BusyCalendarRepositories {
  @override
  Future<BusyTimeResponse> onGetBusyTime() async {
    // TODO: implement onGetBusyTime
    final url = NetworkConfig.BUSY_TIME;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200) {
      return BusyTimeResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

  @override
  Future<String> onUpdateBusyCalendar(List<UpdateBusyCalendarRequest> request) async {
    // TODO: implement onUpdateBusyCalendar
    final url = NetworkConfig.BUSY_TIME;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode(request.map((e) => e.toJson()).toList());
    print(body);
    final Response responseJson = await NetworkClient.onPost(url, header: header, body: body);
    if(responseJson.statusCode == 200){
      final response = jsonDecode(responseJson.body);
      if(response['result'] != null){
        return response['result'];
      }
      return response['result']['error']['message'];
    }
    throw Exception(responseJson.body);
  }

}