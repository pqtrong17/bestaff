import 'package:be_staff/data/response/calendar_response.dart';

abstract class CalendarRepositories {
  Future<CalendarResponse> onGetWorkingCalendar();
}