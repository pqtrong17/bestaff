import 'dart:convert';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/cede_shift_repositories.dart';
import 'package:be_staff/data/response/cede_shift_response.dart';

class CedeShiftImpl implements CedeShiftRepositories {
  @override
  Future<String> onCedeShift(int shiftId, int switchUser) async {
    // TODO: implement onCedeShift
    final url = NetworkConfig.CEDE_SHIFT;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"id": shiftId, "switch_user": switchUser});
    final responseJson = await NetworkClient.onPost(url, body: body, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return response['result'];
    }
    throw NetworkException(response['result']);
  }

  @override
  Future<CedeShiftResponse> onGetCedeShift(int branchId, String date, int shiftDetailId) async {
    // TODO: implement onGetCedeShift
    final url = NetworkConfig.GET_USER_FOR_CEDE_SHIFT;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"branch_id": branchId, "date": date, "shiftdetail_id": shiftDetailId});
    final responseJson = await NetworkClient.onPost(url, body: body, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return CedeShiftResponse.fromJson(response[0][0]);
    }
    throw NetworkException(response['result']);
  }

}