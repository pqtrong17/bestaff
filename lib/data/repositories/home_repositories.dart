import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/data/response/news_response.dart';
import 'package:be_staff/data/response/other_shift_response.dart';
import 'package:be_staff/data/response/pending_response.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';

abstract class HomeRepositories {
  Future<NewsResponse> onGetNews(int type, int page, int limit);

  Future<ShiftUserResponse> onGetShiftUser({String date});

  Future<LackShiftUserResponse> onGetLackShiftUser();

  Future<PendingResponse> onGetWaiting();

  Future<ShiftUserResponse> onGetAllShifts();

  Future<List<OtherShiftResponse>> onGetOtherShift(String date, String branchId);

  Future<ShiftNotificationResponse> onGetShiftNotification();
}
