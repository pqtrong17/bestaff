import 'dart:convert';

import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/home_repositories.dart';
import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/data/response/news_response.dart';
import 'package:be_staff/data/response/other_shift_response.dart';
import 'package:be_staff/data/response/pending_response.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';

class HomeRepositoriesImpl implements HomeRepositories {
  @override
  Future<NewsResponse> onGetNews(int type, int page, int limit) async {
    // TODO: implement onGetNews
    final header = NetworkConfig.onBuildHeader();
    final url =
        NetworkConfig.GET_LIST_NEWS + "?type=$type&page=$page&limit=$limit";
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return NewsResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

  @override
  Future<ShiftUserResponse> onGetShiftUser({String date}) async {
    // TODO: implement onGetShiftUser
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.SHIFT_USER_LIST;
    final body = jsonEncode({"date": date});
    final responseJson =
        await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return ShiftUserResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

  @override
  Future<LackShiftUserResponse> onGetLackShiftUser() async {
    // TODO: implement onGetLackShiftUser
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.LACK_SHIFT_BRANCH;
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return LackShiftUserResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

  @override
  Future<PendingResponse> onGetWaiting() async {
    // TODO: implement onGetWaiting
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.PENDING_NOTIFICATION;
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return PendingResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

  @override
  Future<ShiftUserResponse> onGetAllShifts() async {
    // TODO: implement onGetAllShifts
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.ALL_SHIFTS;
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return ShiftUserResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

  @override
  Future<List<OtherShiftResponse>> onGetOtherShift(
      String date, String branchId) async {
    // TODO: implement onGetOtherShift
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.SHIFT_OTHER_USER;
    final body = jsonEncode({"date": date, "branch_id": branchId});
    final responseJson =
        await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return (response[0] as List)
          .map((e) => OtherShiftResponse.fromJson(e))
          .toList();
    }
    throw Exception(response.toString());
  }

  @override
  Future<ShiftNotificationResponse> onGetShiftNotification() async {
    // TODO: implement onGetShiftNotification
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.SHIFT_NOTIFICATION;
    final responseJson = await NetworkClient.onPost(url, header: header);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return ShiftNotificationResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }
}
