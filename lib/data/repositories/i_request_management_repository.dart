import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/switch_shift_pending_response.dart';

abstract class IRequestManagementRepository {
  Future<SwitchShiftPendingResponse> onGetSwitchPending();

  Future<String> onAcceptSwitchShift(int id);

  Future<String> onRejectSwitchShift(int id);

  Future<ShiftNotificationResponse> onGetShiftNotification(String limit, String page);
}
