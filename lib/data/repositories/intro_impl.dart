import 'dart:convert';

import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/intro_repositories.dart';

class IntroImpl implements IntroRepositories {
  @override
  Future<String> onCheckAccount(String email) async {
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.CHECK_ACCOUNT;
    final body = json.encode({"email": email});
    final responseJson =
        await NetworkClient.onPost(url,header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if (responseJson.statusCode == 200) {
      return response['UserName'];
    }
    throw Exception(response.toString());
  }
}
