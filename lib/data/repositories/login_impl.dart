import 'dart:convert';

import 'package:be_staff/data/models/user.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/login_repositories.dart';
import 'package:be_staff/data/repositories/register_repositories.dart';
import 'package:be_staff/data/request/login_request.dart';
import 'package:be_staff/data/request/register_request.dart';
import 'package:be_staff/data/response/register_response.dart';
import 'package:be_staff/data/response/user_response.dart';

class LoginImpl implements LoginRepositories {

  @override
  Future<UserResponse> onLogin(LoginRequest request) async {
    final header = NetworkConfig.onBuildHeader();
    final url = NetworkConfig.LOGIN;
    final body = json.encode(request);
    final responseJson = await NetworkClient.onPost(url, header: header,body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return UserResponse.fromJson(response);
    }
    throw Exception(response.toString());
  }

}