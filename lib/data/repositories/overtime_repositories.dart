import 'package:be_staff/data/request/overtime_request.dart';
import 'package:be_staff/data/response/information_overtime_response.dart';

abstract class OvertimeRepositories {
  Future<String> onCreateOvertimeRequest(OvertimeRequest request);
  Future<InformationOvertimeResponse> onGetInformationOvertime();
}
