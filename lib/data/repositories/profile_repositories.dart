import 'dart:io';

import 'package:be_staff/data/request/update_profile_request.dart';
import 'package:be_staff/data/response/detail_user_info.dart';
import 'package:be_staff/data/response/experience_response.dart';

abstract class ProfileRepositories {
  Future<String> onUpdateProfile(UpdateProfileRequest request);

  Future<String> onUploadAvatar(File file);

  Future<DetailUserInfo> onGetDetailInfoUser();

  Future<ExperienceResponse> onGetExperience();

  Future<String> onRating(String star, String content);
}
