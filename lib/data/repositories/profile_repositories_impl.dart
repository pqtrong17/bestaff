import 'dart:convert';
import 'dart:io';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/profile_repositories.dart';
import 'package:be_staff/data/request/update_profile_request.dart';
import 'package:be_staff/data/response/detail_user_info.dart';
import 'package:be_staff/data/response/experience_response.dart';

class ProfileRepositoriesImpl implements ProfileRepositories {
  @override
  Future<String> onUpdateProfile(UpdateProfileRequest request) async {
    // TODO: implement onUpdateProfile
    final url = NetworkConfig.UPDATE_PROFILE;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode(request.toJson());
    final responseJson =
        await NetworkClient.onPut(url, header: header, body: body);
    if (responseJson.statusCode == 200) {
      final response = jsonDecode(responseJson.body)['message'];
      return response;
    }
    throw NetworkException(responseJson.body);
  }

  @override
  Future<String> onUploadAvatar(File file) async {
    // TODO: implement onUploadAvatar
    final url = NetworkConfig.UPLOAD_AVATAR;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onPostMultipart(url,
        header: header, file: file, keyName: "file");
    if (responseJson.statusCode == 200) {
      final response = jsonDecode(responseJson.body)['path'];
      return response;
    }
    throw NetworkException(responseJson.body);
  }

  @override
  Future<DetailUserInfo> onGetDetailInfoUser() async {
    // TODO: implement onGetDetailInfoUser
    final url = NetworkConfig.DETAIL_USER_INFO;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onGet(url, header: header);
    if(responseJson.statusCode == 200){
      final response = jsonDecode(responseJson.body);
      return DetailUserInfo.fromJson(response);
    }
    throw NetworkException(responseJson.body);
  }

  @override
  Future<String> onRating(String star, String content) async {
    // TODO: implement onRating
    final url = NetworkConfig.RATING;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"star": star, "content": content});
    print(body);
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return response['msg'];
    }
    throw NetworkException(response);
  }

  @override
  Future<ExperienceResponse> onGetExperience() async {
    // TODO: implement onGetExperience
    final url = NetworkConfig.GET_EXPERIENCE;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return ExperienceResponse.fromJson(response);
    }
    throw NetworkException(response);
  }
}
