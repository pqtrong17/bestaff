import 'dart:convert';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/receive_cancel_lack_shift_repository.dart';

class ReceiveCancelLackShiftImpl implements ReceiveCancelLackShiftRepository {
  @override
  Future<String> onCancelLack(int shiftDetailUserId) async {
    // TODO: implement onCancelLack
    final url = NetworkConfig.SHIFT_CANCEL_TAKE_USER;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"shift_detailuser_id" : shiftDetailUserId});
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return response['result'];
    }
    throw NetworkException(response['result']);
  }

  @override
  Future<String> onReceiveLack(int shiftDetailId, int positionId) async {
    // TODO: implement onReceiveLack
    final url = NetworkConfig.SHIFT_ADD_TAKE_USER;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"shift_detail_id" : shiftDetailId, "position_id": positionId});
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return response['result'];
    }
    throw NetworkException(response['result']);
  }
  
}
