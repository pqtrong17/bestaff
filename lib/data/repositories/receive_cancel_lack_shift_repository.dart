abstract class ReceiveCancelLackShiftRepository {
  Future<String> onReceiveLack(int shiftDetailId, int positionId);

  Future<String> onCancelLack(int shiftDetailUserId);
}