import 'package:be_staff/data/request/register_request.dart';
import 'package:be_staff/data/response/register_response.dart';

abstract class RegisterRepositories {
  Future<RegisterResponse> onRegister(RegisterRequest registerRequest);

  Future<String> onEnterCode(String code);
}
