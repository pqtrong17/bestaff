import 'dart:convert';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/i_request_management_repository.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/switch_shift_pending_response.dart';

class RequestManagementImpl extends IRequestManagementRepository {
  @override
  Future<String> onAcceptSwitchShift(int id) async {
    // TODO: implement onAcceptSwitchShift
    final url = NetworkConfig.ACCEPT_SWITCH_SHIFT;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"id" : id});
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return response['result'];
    }
    throw NetworkException(response);
  }

  @override
  Future<SwitchShiftPendingResponse> onGetSwitchPending() async {
    // TODO: implement onGetSwitchPending
    final url = NetworkConfig.GET_SWITCH_SHIFT_PENDING;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onGet(url, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return SwitchShiftPendingResponse.fromJson(response);
    }
    throw NetworkException(response);
  }

  @override
  Future<String> onRejectSwitchShift(int id) async {
    // TODO: implement onRejectSwitchShift
    final url = NetworkConfig.REJECT_SWITCH_SHIFT;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"id" : id});
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return response['result'];
    }
    throw NetworkException(response);
  }

  @override
  Future<ShiftNotificationResponse> onGetShiftNotification(String limit, String page) async {
    // TODO: implement onGetShiftNotification
    final url = NetworkConfig.SHIFT_NOTIFICATION;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"limit" : limit, "page": page});
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return ShiftNotificationResponse.fromJson(response);
    }
    throw NetworkException(response);
  }

}