import 'dart:convert';

import 'package:be_staff/data/exception/network_exception.dart';
import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/take_leave_repositories.dart';
import 'package:be_staff/data/request/take_leave_request.dart';
import 'package:be_staff/data/response/type_of_leave_response.dart';

class TakeLeaveImpl implements TakeLeaveRepositories {
  @override
  Future<String> onAddLeave(LeaveRequest request) async {
    // TODO: implement onAddLeave
    final url = NetworkConfig.ADD_LEAVE_REQUEST;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode(request.toJson());
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      if(response['result'] == 404){
        throw NetworkException(response['comment']);
      }else{
        return response['comment'];
      }
    }
    throw NetworkException(response);
  }

  @override
  Future<TypeOfLeaveResponse> onGetTypeOfLeave() async {
    // TODO: implement onGetTypeOfLeave
    final url = NetworkConfig.LIST_OF_TYPE_LEAVE;
    final header = NetworkConfig.onBuildHeader();
    final responseJson = await NetworkClient.onPost(url, header: header);
    final response = jsonDecode(responseJson.body);
    if(responseJson.statusCode == 200){
      return TypeOfLeaveResponse.fromJson(response);
    }
    throw NetworkException(response);
  }

}