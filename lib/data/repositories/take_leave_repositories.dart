import 'package:be_staff/data/request/take_leave_request.dart';
import 'package:be_staff/data/response/type_of_leave_response.dart';

abstract class TakeLeaveRepositories {
  Future<TypeOfLeaveResponse> onGetTypeOfLeave();

  Future<String> onAddLeave(LeaveRequest request);
}
