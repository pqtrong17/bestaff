import 'dart:convert';

import 'package:be_staff/data/network/network_client.dart';
import 'package:be_staff/data/network/network_config.dart';
import 'package:be_staff/data/repositories/timekeeping_repositories.dart';
import 'package:be_staff/data/request/check_out_request.dart';

class TimeKeepingImpl implements TimeKeepingRepositories {
  @override
  Future<int> onCheckIn(int ipAddress) async {
    // TODO: implement onTimeKeeping
    final url = NetworkConfig.TIMEKEEPING;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode({"ip_address" : ipAddress});
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    if(responseJson.statusCode == 200) {
      final Map<String, dynamic> response = jsonDecode(responseJson.body);
      final List<int> results = (response['result'] as List).map((e) => e['done']).toList().cast<int>();
      return results != null && results.isNotEmpty ? results[0] : null;
    }
    throw Exception(responseJson.body);
  }

  @override
  Future<int> onSaveTimeKeeping(CheckOutRequest request) async {
    // TODO: implement onCheckOut
    final url = NetworkConfig.SAVE_TIMEKEEPING;
    final header = NetworkConfig.onBuildHeader();
    final body = jsonEncode(request);
    final responseJson = await NetworkClient.onPost(url, header: header, body: body);
    if(responseJson.statusCode == 200) {
      final response = jsonDecode(responseJson.body);
      return response['Result'];
    }
    throw Exception(responseJson.body);
  }

  @override
  Future<String> onGetPublicIPAddress() async {
    // TODO: implement onGetPublicIPAddress
    final url = "https://api.ipify.org?format=json";
    final responseJson = await NetworkClient.onGet(url);
    if(responseJson.statusCode == 200){
      return jsonDecode(responseJson.body)['ip'];
    }
    throw Exception(responseJson.body);
  }

}