import 'package:be_staff/data/request/check_out_request.dart';

abstract class TimeKeepingRepositories {
  Future<int> onCheckIn(int ipAddress);

  Future<int> onSaveTimeKeeping(CheckOutRequest request);

  Future<String> onGetPublicIPAddress();
}
