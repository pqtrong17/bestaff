class OvertimeRequest {
  String postId;
  String fromDate;
  String toDate;
  String startHour;
  String endHour;
  String typeId;
  String reason;
  String buttonAction;
  int id;

  OvertimeRequest(
      {this.postId,
      this.fromDate,
      this.toDate,
      this.startHour,
      this.endHour,
      this.typeId,
      this.reason,
      this.buttonAction,
      this.id});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this.postId;
    data['from_date'] = this.fromDate;
    data['to_date'] = this.toDate;
    data['start_hour'] = this.startHour;
    data['end_hour'] = this.endHour;
    data['type_id'] = this.typeId;
    data['reason'] = this.reason;
    data['button_action'] = this.buttonAction;
    data['id'] = this.id ?? null;
    return data;
  }
}
