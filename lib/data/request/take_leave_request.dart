class LeaveRequest {
  String fromDate;
  String toDate;
  String typeId;
  String reason;

  LeaveRequest({this.fromDate, this.toDate, this.typeId, this.reason});

  LeaveRequest.fromJson(Map<String, dynamic> json) {
    fromDate = json['from_date'];
    toDate = json['to_date'];
    typeId = json['type_id'];
    reason = json['reason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from_date'] = this.fromDate;
    data['to_date'] = this.toDate;
    data['type_id'] = this.typeId;
    data['reason'] = this.reason;
    return data;
  }
}
