class UpdateBusyCalendarRequest {
  final String start;
  final String end;
  final String note;

  UpdateBusyCalendarRequest({this.start, this.end, this.note});

  Map<String, dynamic> toJson() => {
    "start_time": start,
    "end_time": end,
    "note": note
  };
}