class UpdateProfileRequest {
  String avatar;
  String birthday;
  String personalId;
  String email;
  String address;
  String sex;
  String mobile;

  UpdateProfileRequest(
      {this.avatar,
        this.birthday,
        this.personalId,
        this.email,
        this.address,
        this.sex,
        this.mobile});

  UpdateProfileRequest.fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'];
    birthday = json['birthday'];
    personalId = json['persional_id'];
    email = json['email'];
    address = json['address'];
    sex = json['sex'];
    mobile = json['mobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['birthday'] = this.birthday;
    data['persional_id'] = this.personalId;
    data['email'] = this.email;
    data['address'] = this.address;
    data['sex'] = this.sex;
    data['mobile'] = this.mobile;
    return data;
  }
}
