import 'package:intl/intl.dart';

class BusyTimeResponse {
  final BusyCalendar t2;
  final BusyCalendar t3;
  final BusyCalendar t4;
  final BusyCalendar t5;
  final BusyCalendar t6;
  final BusyCalendar t7;
  final BusyCalendar cn;

  BusyTimeResponse(
      {this.t2, this.t3, this.t4, this.t5, this.t6, this.t7, this.cn});

  BusyTimeResponse.fromJson(Map<String, dynamic> map)
      : t2 = BusyCalendar.fromJson(map['T2']),
        t3 = BusyCalendar.fromJson(map['T3']),
        t4 = BusyCalendar.fromJson(map['T4']),
        t5 = BusyCalendar.fromJson(map['T5']),
        t6 = BusyCalendar.fromJson(map['T6']),
        t7 = BusyCalendar.fromJson(map['T7']),
        cn = BusyCalendar.fromJson(map['CN']);
}

class BusyCalendar {
  final List<BusyTime> busyTimes;
  final String date;

  BusyCalendar(this.busyTimes, this.date);

  BusyCalendar.fromJson(Map<String, dynamic> map)
      : busyTimes = (map['data'] as List).map((e) => BusyTime.fromJson(e)).toList(),
        date = map['date'];
}

class BusyTime {
  String startTime;
  String endTime;
  int shiftBranchId;
  int busy;
  ShiftBranch shiftBranch;

  BusyTime(
      {this.startTime, this.endTime, this.shiftBranchId, this.shiftBranch, this.busy});

  BusyTime.fromJson(Map<String, dynamic> json) {
    startTime = time(json['start_time']);
    endTime = time(json['end_time']);
    shiftBranchId = json['shiftbranch_id'];
    busy = json['busy'] ?? null;
    shiftBranch = json['shift_branch'] != null
        ? new ShiftBranch.fromJson(json['shift_branch'])
        : null;
  }
  String time(String input) {
    String format = "HH:mm:ss";
    DateTime dateTime = DateFormat(format).parse(input);
    String result = DateFormat("HH:mm").format(dateTime);
    return result;
  }
}

class ShiftBranch {
  int id;
  int branchId;
  int day;
  int createBy;
  int updateBy;
  String createdAt;
  String updatedAt;

  ShiftBranch(
      {this.id,
        this.branchId,
        this.day,
        this.createBy,
        this.updateBy,
        this.createdAt,
        this.updatedAt});

  ShiftBranch.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    branchId = json['branch_id'];
    day = json['day'];
    createBy = json['create_by'];
    updateBy = json['update_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['branch_id'] = this.branchId;
    data['day'] = this.day;
    data['create_by'] = this.createBy;
    data['update_by'] = this.updateBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
