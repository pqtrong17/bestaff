class CedeShiftResponse {
  int postId;
  String namePosition;
  int deptId;
  String nameDepartment;
  int action;
  List<DataUser> dataUser;

  CedeShiftResponse(
      {this.postId,
        this.namePosition,
        this.deptId,
        this.nameDepartment,
        this.action,
        this.dataUser});

  CedeShiftResponse.fromJson(Map<String, dynamic> json) {
    postId = json['post_id'];
    namePosition = json['name_position'];
    deptId = json['dept_id'];
    nameDepartment = json['name_department'];
    action = json['action'];
    if (json['data_user'] != null) {
      dataUser = new List<DataUser>();
      json['data_user'].forEach((v) {
        dataUser.add(new DataUser.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this.postId;
    data['name_position'] = this.namePosition;
    data['dept_id'] = this.deptId;
    data['name_department'] = this.nameDepartment;
    data['action'] = this.action;
    if (this.dataUser != null) {
      data['data_user'] = this.dataUser.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataUser {
  int id;
  String avatar;
  String fullName;
  String code;

  DataUser({this.id, this.avatar, this.fullName, this.code});

  DataUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    avatar = json['avatar'];
    fullName = json['full_name'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['avatar'] = this.avatar;
    data['full_name'] = this.fullName;
    data['code'] = this.code;
    return data;
  }
}
