class InformationOvertimeResponse {
  List<Result> result;
  List<TypeOvertime> typeOvertime;
  List<DataPosition> dataPosition;

  InformationOvertimeResponse(
      {this.result, this.typeOvertime, this.dataPosition});

  InformationOvertimeResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
    if (json['type_overtime'] != null) {
      typeOvertime = new List<TypeOvertime>();
      json['type_overtime'].forEach((v) {
        typeOvertime.add(new TypeOvertime.fromJson(v));
      });
    }
    if (json['data_position'] != null) {
      dataPosition = new List<DataPosition>();
      json['data_position'].forEach((v) {
        dataPosition.add(new DataPosition.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    if (this.typeOvertime != null) {
      data['type_overtime'] = this.typeOvertime.map((v) => v.toJson()).toList();
    }
    if (this.dataPosition != null) {
      data['data_position'] = this.dataPosition.map((v) => v.toJson()).toList();
    }
    return data;
  }

}

class Result {
  String typeName;
  String fullName;
  int sex;
  String code;
  String deptName;
  String workday;
  int status;
  String reason;
  int id;
  String startHour;
  String endHour;

  Result(
      {this.typeName,
        this.fullName,
        this.sex,
        this.code,
        this.deptName,
        this.workday,
        this.status,
        this.reason,
        this.id,
        this.startHour,
        this.endHour});

  Result.fromJson(Map<String, dynamic> json) {
    typeName = json['type_name'];
    fullName = json['full_name'];
    sex = json['sex'];
    code = json['code'];
    deptName = json['dept_name'];
    workday = json['workday'];
    status = json['status'];
    reason = json['reason'];
    id = json['id'];
    startHour = json['start_hour'];
    endHour = json['end_hour'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type_name'] = this.typeName;
    data['full_name'] = this.fullName;
    data['sex'] = this.sex;
    data['code'] = this.code;
    data['dept_name'] = this.deptName;
    data['workday'] = this.workday;
    data['status'] = this.status;
    data['reason'] = this.reason;
    data['id'] = this.id;
    data['start_hour'] = this.startHour;
    data['end_hour'] = this.endHour;
    return data;
  }
}

class TypeOvertime {
  int id;
  String name;
  int payRate;

  TypeOvertime({this.id, this.name, this.payRate});

  TypeOvertime.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    payRate = json['pay_rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['pay_rate'] = this.payRate;
    return data;
  }
}

class DataPosition {
  int id;
  String name;

  DataPosition({this.id, this.name});

  DataPosition.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
