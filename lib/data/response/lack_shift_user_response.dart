import 'package:intl/intl.dart';

class LackShiftUserResponse {
  List<LackShiftUser> lackShiftUser;

  LackShiftUserResponse({this.lackShiftUser});

  LackShiftUserResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      lackShiftUser = new List<LackShiftUser>();
      json['result'].forEach((v) {
        lackShiftUser.add(new LackShiftUser.fromJson(v));
      });
    }
  }
}

class LackShiftUser {
  String day;
  String nameDay;
  List<DataItem> dataItem;


  LackShiftUser({this.day, this.nameDay, this.dataItem});

  LackShiftUser.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    nameDay = json['name_day'];
    if (json['data_item'] != null) {
      dataItem = new List<DataItem>();
      json['data_item'].forEach((v) {
        dataItem.add(new DataItem.fromJson(v));
      });
    }
  }
  DateTime get getDateTime {
    return DateFormat("yyyy-MM-dd").parse(day);
  }

}

class DataItem {
  String startTime;
  String endTime;
  double totalHours;
  List<DataPosition> dataPosition;

  DataItem({this.startTime, this.endTime, this.totalHours, this.dataPosition});

  DataItem.fromJson(Map<String, dynamic> json) {
    startTime = json['start_time'];
    endTime = json['end_time'];
    totalHours = json['total_hours'].toDouble();
    if (json['data_position'] != null) {
      dataPosition = new List<DataPosition>();
      json['data_position'].forEach((v) {
        dataPosition.add(new DataPosition.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['total_hours'] = this.totalHours;
    if (this.dataPosition != null) {
      data['data_position'] = this.dataPosition.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataPosition {
  int positionId;
  String namePosition;
  int user;
  int action;
  int shiftDetailId;
  int shiftDetailUserId;
  String color;
  String position;

  DataPosition(
      {this.positionId,
        this.namePosition,
        this.user,
        this.action,
        this.shiftDetailId,
        this.shiftDetailUserId,
        this.color,
        this.position
      });

  DataPosition.fromJson(Map<String, dynamic> json) {
    positionId = json['position_id'];
    namePosition = json['name_position'];
    user = json['user'];
    action = json['action'];
    shiftDetailId = json['shift_detail_id'];
    shiftDetailUserId = json['shift_detailuser_id'];
    color = json['color'];
    position = json['location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position_id'] = this.positionId;
    data['name_position'] = this.namePosition;
    data['user'] = this.user;
    data['action'] = this.action;
    data['shift_detail_id'] = this.shiftDetailId;
    data['shift_detailuser_id'] = this.shiftDetailUserId;
    return data;
  }
}

class LackCanReceive {
  String startTime;
  String endTime;
  double totalHours;
  int positionId;
  String namePosition;
  int user;
  int action;
  int shiftDetailId;
  int shiftDetailUserId;
  String color;
  String position;

  LackCanReceive(
      {this.startTime,
      this.endTime,
      this.totalHours,
      this.positionId,
      this.namePosition,
      this.user,
      this.action,
      this.shiftDetailId,
      this.shiftDetailUserId,
      this.color,
      this.position});
}

class LackShiftUserHome {
  String day;
  String nameDay;
  DataItem dataItem;

  LackShiftUserHome({this.day, this.nameDay, this.dataItem});

  String get getTimes {
    final DateTime date = DateFormat("yyyy-MM-dd").parse(day);
    switch (date.difference(DateTime.now()).inDays){
      case 0:
        return "Hôm nay";
      case 1:
        return "Ngày mai";
      default :
        return null;
    }
  }

  String get getDate {
    final DateTime date = DateFormat("yyyy-MM-dd").parse(day);
    return DateFormat("dd-MM").format(date);
  }

  DateTime get getDateTime {
    return DateFormat("yyyy-MM-dd").parse(day);
  }

}
