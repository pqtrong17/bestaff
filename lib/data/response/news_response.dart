class NewsResponse {
  List<News> news;
  int totalPage;

  NewsResponse({this.news, this.totalPage});

  NewsResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      news = new List<News>();
      json['result'].forEach((v) {
        news.add(new News.fromJson(v));
      });
    }
    totalPage = json['total_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.news != null) {
      data['result'] = this.news.map((v) => v.toJson()).toList();
    }
    data['total_page'] = this.totalPage;
    return data;
  }
}

class News {
  int id;
  String fullName;
  String title;
  String content;
  String dayFeel;
  int deviceType;
  int type;
  String createName;
  String image;

  News(
      {this.id,
        this.fullName,
        this.title,
        this.content,
        this.dayFeel,
        this.deviceType,
        this.type,
        this.createName, this.image});

  News.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    title = json['title'];
    content = json['content'];
    dayFeel = json['day_feel'];
    deviceType = json['device_type'];
    type = json['type'];
    createName = json['create_name'];
    image = json['image'] != null ? json['image'] : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['title'] = this.title;
    data['content'] = this.content;
    data['day_feel'] = this.dayFeel;
    data['device_type'] = this.deviceType;
    data['type'] = this.type;
    data['create_name'] = this.createName;
    return data;
  }
}
