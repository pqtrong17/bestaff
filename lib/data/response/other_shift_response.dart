class OtherShiftResponse {
  String fullName;
  double totalHours;
  String totalSalary;
  List<DataDate> dataDate;

  OtherShiftResponse(
      {this.fullName, this.totalHours, this.totalSalary, this.dataDate});

  OtherShiftResponse.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    totalHours = json['total_hours'].toDouble();
    totalSalary = json['total_salary'];
    if (json['data_date'] != null) {
      dataDate = new List<DataDate>();
      json['data_date'].forEach((v) {
        dataDate.add(new DataDate.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['full_name'] = this.fullName;
    data['total_hours'] = this.totalHours;
    data['total_salary'] = this.totalSalary;
    if (this.dataDate != null) {
      data['data_date'] = this.dataDate.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataDate {
  String date;
  String nameDay;
  List<DataItem> dataItem;

  DataDate({this.date, this.nameDay, this.dataItem});

  DataDate.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    nameDay = json['name_day'];
    if (json['data_item'] != null) {
      dataItem = new List<DataItem>();
      json['data_item'].forEach((v) {
        dataItem.add(new DataItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['name_day'] = this.nameDay;
    if (this.dataItem != null) {
      data['data_item'] = this.dataItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataItem {
  int id;
  String namePosition;
  String color;
  int salary;
  double hours;
  String start;
  String end;
  String icon;

  DataItem(
      {this.id,
        this.namePosition,
        this.color,
        this.salary,
        this.hours,
        this.start,
        this.end,
        this.icon});

  DataItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    namePosition = json['name_position'];
    color = json['color'];
    salary = json['salary'];
    hours = json['hours'].toDouble();
    start = json['start'];
    end = json['end'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_position'] = this.namePosition;
    data['color'] = this.color;
    data['salary'] = this.salary;
    data['hours'] = this.hours;
    data['start'] = this.start;
    data['end'] = this.end;
    data['icon'] = this.icon;
    return data;
  }
}
