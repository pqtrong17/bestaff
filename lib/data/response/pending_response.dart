import 'package:intl/intl.dart';

class PendingResponse {
  int menuId;
  int branchId;
  String type;
  int status;
  String msg;
  List<SwitchResponse> switchShifts;

  PendingResponse(
      {this.menuId,
        this.branchId,
        this.type,
        this.status,
        this.msg, this.switchShifts});

  PendingResponse.fromJson(Map<String, dynamic> json) {
    menuId = json['menuid'];
    branchId = json['branchid'];
    type = json['type'];
    status = json['status'];
    msg = json['msg'];
    if (json['datatable'] != null) {
      switchShifts = new List<SwitchResponse>();
      json['datatable'].forEach((v) {
        switchShifts.add(new SwitchResponse.fromJson(v));
      });
    }
  }
}
class SwitchResponse {
  String fullName;
  String avatar;
  String switchName;
  String takeName;
  String namePosition;
  String date;
  String startTime;
  String endTime;
  int id;
  int userId;
  int switchUser;
  int takeUser;
  int status;
  String startTimeToEndTime;

  SwitchResponse({this.fullName,
    this.avatar,
    this.switchName,
    this.takeName,
    this.namePosition,
    this.date,
    this.startTime,
    this.endTime,
    this.id,
    this.userId,
    this.switchUser,
    this.takeUser,
    this.status});

  SwitchResponse.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    avatar = json['avatar'];
    switchName = json['switch_name'];
    takeName = json['take_name'];
    namePosition = json['name_position'];
    date = json['date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    id = json['id'];
    userId = json['user_id'];
    switchUser = json['switch_user'];
    takeUser = json['take_user'];
    status = json['status'];
  }

  String get getStartToEndTime{
    String format = "yyyy-MM-dd HH:mm:ss";
    DateTime start = DateFormat(format).parse(startTime);
    DateTime end = DateFormat(format).parse(endTime);
    return DateFormat("HH:mm").format(start) + " : " + DateFormat("HH:mm").format(end);
  }

  String get getDate {
    DateTime _date = DateFormat("yyyy-MM-dd").parse(date);
    String result = DateFormat("dd-MM").format(_date);
    return result;
  }
}
