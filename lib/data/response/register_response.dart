import 'package:be_staff/data/models/user.dart';

class RegisterResponse {
  User user;

  RegisterResponse({this.user});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    user =
    json['success'] != null ? new User.fromJson(json['success']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['success'] = this.user.toJson();
    }
    return data;
  }
}