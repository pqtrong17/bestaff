import 'package:be_staff/data/response/shift_response/data_user.dart';

class DataItem {
  String startTime;
  String endTime;
  List<DataUser> dataUser;
  int action;
  int createBy;

  DataItem(
      {this.startTime,
        this.endTime,
        this.dataUser,
        this.action,
        this.createBy});

  DataItem.fromJson(Map<String, dynamic> json) {
    startTime = json['start_time'];
    endTime = json['end_time'];
    if (json['data_user'] != null) {
      dataUser = new List<DataUser>();
      json['data_user'].forEach((v) {
        dataUser.add(new DataUser.fromJson(v));
      });
    }
    action = json['action'];
    createBy = json['create_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    if (this.dataUser != null) {
      data['data_user'] = this.dataUser.map((v) => v.toJson()).toList();
    }
    data['action'] = this.action;
    data['create_by'] = this.createBy;
    return data;
  }
}