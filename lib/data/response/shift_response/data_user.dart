class DataUser {
  int id;
  int userId;
  String fullName;
  int positionId;
  String namePosition;
  int switchUser;
  int status;
  int createBy;
  String position;
  String color;
  String avatar;
  int shiftDetailId;

  DataUser(
      {this.id,
        this.userId,
        this.fullName,
        this.positionId,
        this.namePosition,
        this.switchUser,
        this.status,
        this.position,
        this.color,
        this.avatar,
        this.shiftDetailId,
        this.createBy});

  DataUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    positionId = json['position_id'];
    namePosition = json['name_position'];
    switchUser = json['switch_user'];
    status = json['status'];
    createBy = json['create_by'];
    position = json['location'];
    color = json['color'];
    avatar = json['avatar'];
    shiftDetailId = json['shiftdetail_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['full_name'] = this.fullName;
    data['position_id'] = this.positionId;
    data['name_position'] = this.namePosition;
    data['switch_user'] = this.switchUser;
    data['status'] = this.status;
    data['create_by'] = this.createBy;
    return data;
  }
}