import 'package:intl/intl.dart';

import 'data_item.dart';

class ShiftUserHome {
  String day;
  String nameDay;
  DataItem dataItem;
  int action;

  ShiftUserHome({this.day, this.nameDay, this.dataItem, this.action});

  String get getTimes {
    final DateTime date = DateFormat("yyyy-MM-dd").parse(day);
    switch (date.difference(DateTime.now()).inDays){
      case 0:
        return "Hôm nay";
      case 1:
        return "Ngày mai";
      default :
        return null;
    }
  }

  String get getDate {
    final DateTime date = DateFormat("yyyy-MM-dd").parse(day);
    return DateFormat("dd-MM").format(date);
  }

  DateTime get getDateTime {
    return DateFormat("yyyy-MM-dd").parse(day);
  }

}