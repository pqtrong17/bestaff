import 'package:be_staff/data/response/shift_response/data_item.dart';
import 'package:intl/intl.dart';

class ShiftUserResponse {
  List<ShiftUser> shiftUser;
  double totalHours;

  ShiftUserResponse({this.shiftUser, this.totalHours});

  ShiftUserResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      shiftUser = new List<ShiftUser>();
      json['result'].forEach((v) {
        shiftUser.add(new ShiftUser.fromJson(v));
      });
    }
    totalHours = json['total_hours'].toDouble();
  }

}

class ShiftUser {
  String day;
  String nameDay;
  List<DataItem> dataItem;
  int action;

  ShiftUser({this.day, this.nameDay, this.dataItem, this.action});

  ShiftUser.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    nameDay = json['name_day'];
    if (json['data_item'] != null) {
      dataItem = new List<DataItem>();
      json['data_item'].forEach((v) {
        dataItem.add(new DataItem.fromJson(v));
      });
    }
    action = json['action'];



  }

  String get getDate {
    final DateTime date = DateFormat("yyyy-MM-dd").parse(day);
    return DateFormat("dd-MM").format(date);
  }

  DateTime get getDateTime {
    return DateFormat("yyyy-MM-dd").parse(day);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['name_day'] = this.nameDay;
    if (this.dataItem != null) {
      data['data_item'] = this.dataItem.map((v) => v.toJson()).toList();
    }
    data['action'] = this.action;
    return data;
  }

}
