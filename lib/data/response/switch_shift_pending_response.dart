class SwitchShiftPendingResponse {
  List<SwitchShiftPending> result;

  SwitchShiftPendingResponse({this.result});

  SwitchShiftPendingResponse.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<SwitchShiftPending>();
      json['result'].forEach((v) {
        result.add(new SwitchShiftPending.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SwitchShiftPending {
  int id;
  int userId;
  String fullName;
  int positionId;
  String namePosition;
  int switchUser;
  int status;
  int createBy;
  String startTime;
  String endTime;
  String day;
  String nameDay;

  SwitchShiftPending(
      {this.id,
        this.userId,
        this.fullName,
        this.positionId,
        this.namePosition,
        this.switchUser,
        this.status,
        this.createBy,
        this.startTime,
        this.endTime,
        this.day,
        this.nameDay});

  SwitchShiftPending.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    positionId = json['position_id'];
    namePosition = json['name_position'];
    switchUser = json['switch_user'];
    status = json['status'];
    createBy = json['create_by'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    day = json['day'];
    nameDay = json['name_day'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['full_name'] = this.fullName;
    data['position_id'] = this.positionId;
    data['name_position'] = this.namePosition;
    data['switch_user'] = this.switchUser;
    data['status'] = this.status;
    data['create_by'] = this.createBy;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['day'] = this.day;
    data['name_day'] = this.nameDay;
    return data;
  }
}
