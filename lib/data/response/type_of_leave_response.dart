class TypeOfLeaveResponse {
  int leaveYear;
  List<Result> result;

  TypeOfLeaveResponse({this.leaveYear, this.result});

  TypeOfLeaveResponse.fromJson(Map<String, dynamic> json) {
    leaveYear = json['leave_year'];
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['leave_year'] = this.leaveYear;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  int id;
  String name;
  String type;

  Result({this.id, this.name, this.type});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['type'] = this.type;
    return data;
  }
}
