class UserResponse {
  String token;
  int userId;
  String fullName;
  String avatar;
  Dept dept;
  Dept company;
  List<BranchName> branchName;
  String email;
  String mobile;
  int firstLogin;

  UserResponse(
      {this.token,
      this.userId,
      this.fullName,
      this.avatar,
      this.dept,
      this.company,
      this.branchName,
      this.email,
      this.mobile,
      this.firstLogin});

  UserResponse.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    userId = json['user_id'];
    fullName = json['full_name'];
    avatar = json['avatar'];
    dept = json['dept'] != null ? new Dept.fromJson(json['dept']) : null;
    company =
        json['company'] != null ? new Dept.fromJson(json['company']) : null;
    if (json['branch_name'] != null) {
      branchName = new List<BranchName>();
      json['branch_name'].forEach((v) {
        branchName.add(new BranchName.fromJson(v));
      });
    }
    email = json['email'];
    mobile = json['mobile'];
    firstLogin = json['first_login'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['user_id'] = this.userId;
    data['full_name'] = this.fullName;
    data['avatar'] = this.avatar;
    if (this.dept != null) {
      data['dept'] = this.dept.toJson();
    }
    if (this.company != null) {
      data['company'] = this.company.toJson();
    }
    if (this.branchName != null) {
      data['branch_name'] = this.branchName.map((v) => v.toJson()).toList();
    }
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['first_login'] = this.firstLogin;
    return data;
  }
}

class BranchName {
  int id;
  String name;

  BranchName({this.id, this.name});

  BranchName.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class Dept {
  int id;
  String name;

  Dept({this.id, this.name});

  Dept.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
