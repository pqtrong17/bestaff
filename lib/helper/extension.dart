
extension StrCheck on String {
  bool checkPhoneNumber(){
    Pattern pattern =
        r'(84|0[3|5|7|8|9])+([0-9]{8})\b';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(this))
      return true;
    else
      return false;
  }
  bool checkEmail(){
    Pattern pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(this))
      return true;
    else
      return false;
  }
}