import 'package:flutter/material.dart';

class NavigatorHelper {
  static Future<T> onNextPage<T>(BuildContext context, Widget page) =>
      Navigator.push<T>(context, MaterialPageRoute(builder: (context) => page));

  static void onBackPage(BuildContext context, {dynamic result}) => Navigator.pop(context, result);

  static void onNextAndRemoveUtil(BuildContext context, Widget page) {
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => page), (route) => false);
  }

  static void onNextReplacement(BuildContext context, Widget page) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => page));
  }
}
