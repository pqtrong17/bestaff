import 'dart:convert';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/response/user_response.dart';
import 'package:be_staff/ui/home/controller/home_controller.dart';
import 'package:be_staff/ui/home/controller/main_home_controller.dart';
import 'package:be_staff/ui/home/controller/shift_user_controller.dart';
import 'package:be_staff/ui/home/view/main_home_page.dart';
import 'package:be_staff/ui/intro/controller/intro_controller.dart';
import 'package:be_staff/ui/intro/view/intro_page.dart';
import 'package:be_staff/ui/login/controller/login_controller.dart';
import 'package:be_staff/ui/profile/controller/account_controller.dart';
import 'package:be_staff/ui/profile/controller/profile_controller.dart';
import 'package:be_staff/ui/register/controller/enter_code_controller.dart';
import 'package:be_staff/ui/register/controller/register_controller.dart';
import 'package:be_staff/ui/register/controller/tutorial_controller.dart';
import 'package:be_staff/ui/request_management/controller/overtime_controller.dart';
import 'package:be_staff/ui/request_management/controller/request_management_controller.dart';
import 'package:be_staff/ui/request_management/controller/take_leave_controller.dart';
import 'package:be_staff/ui/shift/controller/busy_calendar_controller.dart';
import 'package:be_staff/ui/shift/controller/detail_shift_controller.dart';
import 'package:be_staff/ui/shift/controller/lack_people_controller.dart';
import 'package:be_staff/ui/shift/controller/shift_controller.dart';
import 'package:be_staff/ui/shift/view/busy_view.dart';
import 'package:be_staff/ui/user_manual/controller/user_manual_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  bool isLogged = false;
  SharedPreferences.getInstance().then((preferences) {
    if (preferences.getString("infoUser") != null) {
      final Map<String, dynamic> map =
          jsonDecode(preferences.getString("infoUser"));
      StorageInfoUser().setInfoUser(UserResponse.fromJson(map));
    }
    if (StorageInfoUser()?.getInfoUser?.token != null) {
      isLogged = true;
    } else {
      isLogged = false;
    }
    runApp(GetMaterialApp(
      home: isLogged ? MainHomePage() : IntroPage(),
      theme: ThemeData(
        brightness: Brightness.light,
        fontFamily: fBarlow,
        accentColor: blueMainColorApp,
        primaryColor: blueMainColorApp,
      ),
      initialBinding: _Binding(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate
      ],
      supportedLocales: [
        Locale("vi"),
        Locale("en"),
      ],
    ));
  });
}

class _Binding extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<IntroController>(() => IntroController(), fenix: true);
    Get.lazyPut<ShiftController>(() => ShiftController(), fenix: true);
    Get.lazyPut<BusyController>(() => BusyController(), fenix: true);
    Get.lazyPut<RegisterController>(() => RegisterController(), fenix: true);
    Get.lazyPut<LoginController>(() => LoginController(), fenix: true);
    Get.lazyPut<UserManualController>(() => UserManualController(),
        fenix: true);
    Get.lazyPut<MainHomeController>(() => MainHomeController(), fenix: true);
    Get.lazyPut<HomeController>(() => HomeController(), fenix: true);
    Get.lazyPut<ShiftUserController>(() => ShiftUserController(), fenix: true);
    Get.lazyPut<ProfileController>(() => ProfileController(), fenix: true);
    Get.lazyPut<ListBusyTimeController>(() => ListBusyTimeController(),
        fenix: true);
    Get.lazyPut<EnterCodeController>(() => EnterCodeController(), fenix: true);
    Get.lazyPut<TutorialController>(() => TutorialController(), fenix: true);
    Get.lazyPut<RequestManagementController>(
        () => RequestManagementController(),
        fenix: true);
    Get.lazyPut<TakeLeaveController>(() => TakeLeaveController(), fenix: true);
    Get.lazyPut<OvertimeController>(() => OvertimeController(), fenix: true);
    Get.lazyPut<AccountController>(() => AccountController(), fenix: true);
    Get.lazyPut<LackPeopleController>(() => LackPeopleController(), fenix: true);
    Get.lazyPut<DetailShiftController>(() => DetailShiftController(), fenix: true);
  }
}
