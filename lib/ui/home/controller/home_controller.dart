import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/home_repositories.dart';
import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/data/response/news_response.dart';
import 'package:be_staff/data/response/pending_response.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_home.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';
import 'package:be_staff/helper/date_time_helper.dart';
import 'package:be_staff/ui/home/contract/home_contract.dart';
import 'package:get/get.dart';

class HomeController extends GetxController implements HomeContract {
  HomeContract contract;
  HomeRepositories repositories;
  NewsResponse mNews;
  LackShiftUserResponse mLackShiftUser;
  PendingResponse mPending;
  List<ShiftUserHome> mShiftHome;
  List<LackShiftUserHome> mLackShiftHome;
  String firstName;
  bool isSeeMore = false;
  ShiftNotificationResponse mShiftNotification;
  int pendingCount = 0;
  int successCount = 0;
  int rejectCount = 0;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    onGetFirstName();
    repositories = Injector().onGetHome;
    contract = this;
    onGetNews();
    onGetShiftUser();
    onGetLackShiftUser();
    onGetPending();
    onGetShiftNotification();
  }

  void onGetFirstName() {
    String fullName = StorageInfoUser()?.getInfoUser?.fullName;
    if (fullName.contains(" ")) {
      firstName =
          fullName.substring(fullName.lastIndexOf(" "), fullName.length);
    } else {
      firstName = fullName;
    }
    update();
  }

  void onGetNews() {
    repositories.onGetNews(0, 0, 10).then((value) {
      return contract.onGetNewsSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  void onPressedSeeMore() {
    isSeeMore = true;
    update();
    onGetShiftUser();
  }

  void onGetShiftUser() {
    String date;
    if (!isSeeMore) {
      date = DateTimeHelper.toYearMonthDay(DateTime.now().toIso8601String());
    } else {
      date = DateTimeHelper.toYearMonthDay(
          DateTime.now().add(Duration(days: 7)).toIso8601String());
    }
    repositories.onGetShiftUser(date: date).then((value) {
      return contract.onGetShiftUserSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  void onGetLackShiftUser() => repositories.onGetLackShiftUser().then((value) {
        return contract.onGetLackShiftUserSuccess(value);
      }).catchError((onError) {
        return contract.onError(onError);
      });

  void onGetPending() => repositories.onGetWaiting().then((value) {
        return contract.onGetPendingSuccess(value);
      }).catchError((onError) {
        return contract.onError(onError);
      });

  void onGetShiftNotification() {
    repositories.onGetShiftNotification().then((value) {
      return contract.onGetShiftNotificationSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  @override
  void onError(Exception exception) {
    // TODO: implement onError
    Get.snackbar("Error", exception.toString());
  }

  @override
  void onGetNewsSuccess(NewsResponse response) {
    // TODO: implement onGetNewsSuccess
    mNews = response;
  }

  @override
  void onGetShiftUserSuccess(ShiftUserResponse response) {
    // TODO: implement onGetShiftUserSuccess
    if (response.shiftUser != null) {
      List<ShiftUserHome> _shiftUserHome = List();
      response.shiftUser.forEach((element) {
        bool isAfter =
            element.getDateTime.difference(DateTime.now()).inDays >= 0;
        if ((element.dataItem != null && element.dataItem.isNotEmpty) &&
            isAfter) {
          element.dataItem.forEach((elementDataItem) {
            _shiftUserHome.add(ShiftUserHome(
                day: element.day,
                action: element.action,
                nameDay: element.nameDay,
                dataItem: elementDataItem));
          });
        }
      });
      if (isSeeMore) {
        mShiftHome.addAll(_shiftUserHome);
        isSeeMore = false;
      } else {
        mShiftHome = _shiftUserHome;
      }
    }
    update();
  }

  @override
  void onGetLackShiftUserSuccess(LackShiftUserResponse response) {
    // TODO: implement onGetMissingCase
    if (response.lackShiftUser != null) {
      List<LackShiftUserHome> _shiftUserHome = List();
      response.lackShiftUser.forEach((element) {
        bool isAfter =
            element.getDateTime.difference(DateTime.now()).inDays >= 0;
        if ((element.dataItem != null && element.dataItem.isNotEmpty) &&
            isAfter) {
          element.dataItem.forEach((elementDataItem) {
            _shiftUserHome.add(LackShiftUserHome(
                day: element.day,
                nameDay: element.nameDay,
                dataItem: elementDataItem));
          });
        }
      });
      mLackShiftHome = _shiftUserHome;
    }
    update();
  }

  Future<void> onRefresh () async {
    onGetNews();
    onGetShiftUser();
    onGetLackShiftUser();
    onGetPending();
    onGetShiftNotification();
  }

  @override
  void onGetPendingSuccess(PendingResponse response) {
    // TODO: implement onGetPendingSuccess
    mPending = response;
  }

  @override
  void onGetShiftNotificationSuccess(ShiftNotificationResponse response) {
    // TODO: implement onGetShiftNotificationSuccess
    mShiftNotification = response;
    // if (mShiftNotification.count != null && mShiftNotification.count.isNotEmpty) {
    //   mShiftNotification.count.forEach((element) {
    //     switch (element.status){
    //       case 1:
    //         pendingCount = element.quantity;
    //         break;
    //       case 2:
    //         rejectCount = element.quantity;
    //         break;
    //       case 3:
    //         successCount = element.quantity;
    //         break;
    //     }
    //   });
    // }
    pendingCount = response.countTotal.status1;
    rejectCount = response.countTotal.status2;
    successCount = response.countTotal.status3;
    update();
  }
}
