import 'dart:async';

import 'package:be_staff/values/strings.dart';
import 'package:get/get.dart';

class MainHomeController extends GetxController {
  int currentTabIndex = 0;
  int countTab = 0;

  void onUpdateCurrentTab(int value){
    currentTabIndex = value;
    update();
  }

  Future<bool> onDoubleTap() async{
    countTab++;
    if(countTab == 2){
      return Future.value(true);
    }else{
      Timer.periodic(Duration(seconds: 2), (timer) {
        if (timer.tick == 2) {
          Get.snackbar(null, double_tap_to_exit);
          countTab = 0;
          if(timer.isActive){
            timer.cancel();
          }
        }
      });
      return Future.value(false);
    }

  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }
}