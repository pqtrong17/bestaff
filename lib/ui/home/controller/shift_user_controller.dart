import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/timekeeping_repositories.dart';
import 'package:be_staff/data/request/check_out_request.dart';
import 'package:be_staff/ui/home/contract/shift_user_contract.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:get/get.dart';

class ShiftUserController extends GetxController implements ShiftUserContract {
  TimeKeepingRepositories repositories;
  ShiftUserContract contract;
  bool isCheckOut = false;
  bool isCalling = false;
  CheckOutRequest mCheckOutRequest;
  int publicIPAddress;

  bool isHiddenSeeMore = false;


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repositories = Injector().onGetTimeKeeping;
    contract = this;
  }

  void onHiddenSeeMore(){
    isHiddenSeeMore = true;
    update();
  }

  // Future<int> onGetIPAddress() async {
  //   for (var interface in await NetworkInterface.list()) {
  //     print('== Interface: ${interface.name} ==');
  //     for (var addr in interface.addresses) {
  //       print(
  //           '${addr.address} ${addr.host} ${addr.isLoopback} ${addr.rawAddress} ${addr.type.name}');
  //       return int.parse(addr.address.replaceAll(".", ""));
  //     }
  //   }
  //   return -1;
  // }

  Future<void> onBuildRequest() async {
    mCheckOutRequest = CheckOutRequest();
    mCheckOutRequest.userId = StorageInfoUser().getInfoUser.userId;
    mCheckOutRequest.ipAddress = publicIPAddress;
    mCheckOutRequest.branchId = StorageInfoUser().getInfoUser.branchName[0].id;
    mCheckOutRequest.token = StorageInfoUser().getInfoUser.token;
    mCheckOutRequest.checkInByCode = 0;
  }

  Future<void> onPressedTimeKeeping() async {
    isCalling = true;
    update();
    String _publicIPString = await repositories.onGetPublicIPAddress();
    if(_publicIPString == null){
      Get.snackbar(null, get_ip_address_failed);
      return;
    }
    int _publicIPInt = int.parse(_publicIPString.replaceAll(".", ""));
    publicIPAddress = _publicIPInt;
    onCheckIn();
  }

  void onCheckIn() async {
    repositories.onCheckIn(publicIPAddress).then((value) {
      return contract.onCheckInSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  Future<void> onSaveTimeKeeping(CheckOutRequest request) async {
    repositories.onSaveTimeKeeping(request).then((value) {
      return contract.onSaveTimeKeepingSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  @override
  void onError(Exception exception) {
    // TODO: implement onError
    Get.dialog(MessageDialog(
      description: exception.toString(),
    ));
  }

  @override
  Future<void> onCheckInSuccess(int response) async {
    // TODO: implement onTimekeepingSuccess
    if(response != null){
      await onBuildRequest();
      onSaveTimeKeeping(mCheckOutRequest);
    }else{
      isCalling = false;
      update();
      Get.snackbar(null, please_go_to_work_place);
    }
  }

  @override
  void onSaveTimeKeepingSuccess(int response) {
    // TODO: implement onCheckOutSuccess
    isCheckOut = !isCheckOut;
    isCalling = false;
    update();
  }
}
