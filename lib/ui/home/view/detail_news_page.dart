import 'package:be_staff/data/response/news_response.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailNewsPage extends GetView {
  final News news;

  DetailNewsPage(this.news);

  @override
  Widget build(BuildContext context) {
    final urlImage = news.image;
    // TODO: implement build
    return InitialWidget(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            floating: true,
            leading: InkWell(
                onTap: () => Get.back(),
                child: Icon(Icons.arrow_back_outlined, color: Colors.black,)),
            expandedHeight: Get.height / 3,
            flexibleSpace: FlexibleSpaceBar(
              background: urlImage != null
                  ? ImageCustomized.network(
                      path: urlImage,
                      fit: BoxFit.cover,
                    )
                  : ImageCustomized(
                      path: ic_announced,
                      width: 100,
                      height: 100,
                fit: BoxFit.cover,
                    ),
            ),
          ),
          SliverFillRemaining(
            hasScrollBody: true,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextCustomized(
                    text: news.title,
                    weight: FontWeight.w600,
                    size: mediumSize,
                  ),
                  SizedBox(height: 20,),
                  TextCustomized(
                    text: news.content,
                    weight: FontWeight.w500,
                  ),
                ],
              ),
            ),
          )
        ],

      ),
    );
  }
}