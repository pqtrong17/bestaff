import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/home/controller/home_controller.dart';
import 'package:be_staff/ui/home/view/lack_of_people_of_shift_view.dart';
import 'package:be_staff/ui/home/view/news_view.dart';
import 'package:be_staff/ui/home/view/shift_user_view.dart';
import 'package:be_staff/ui/home/view/pending_view.dart';
import 'package:be_staff/ui/profile/view/account_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InitialWidget(
      body: GetBuilder<HomeController>(
        builder: (value) => RefreshIndicator(
          onRefresh: () => controller.onRefresh(),
          child: ListView(
            children: [
              _buildHeader(),
              SizedBox(
                height: 12,
              ),
              controller.mNews != null ? NewsView(controller.mNews) : Container(),
              SizedBox(
                height: 12,
              ),
              controller.mShiftHome != null
                  ? ShiftUserView(controller.mShiftHome)
                  : Container(),
              SizedBox(
                height: 12,
              ),
              controller.mLackShiftHome != null
                  ? LackOfPeopleOfShiftView(controller.mLackShiftHome)
                  : Container(),
              SizedBox(
                height: 12,
              ),
              controller.mPending != null && controller.mShiftNotification != null
                  ? PendingView()
                  : Container(),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildHeader() => Padding(
        padding: const EdgeInsets.only(top: 8, right: 16, left: 16),
        child: Row(
          children: [
            Expanded(
              child: Row(
                children: [
                  InkWell(
                    onTap: () => Get.to(AccountPage()),
                    child: Stack(
                      overflow: Overflow.visible,
                      children: [
                        Container(
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(colors: [
                                ColorHelper.parseColor("#7433FF"),
                                ColorHelper.parseColor("#FFA3FD"),
                              ])),
                          child: CircleImage(
                            path: ic_avatar_example,
                            insideRadius: 20,
                          ),
                        ),
                        Positioned(
                            left: 35,
                            child: CircleAvatar(
                              child: ImageCustomized(
                                  path: ic_settings, width: 12, height: 12),
                              backgroundColor: Colors.white,
                              radius: 6,
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  RichText(
                    text: TextSpan(
                        text: (DateTime.now().hour < 13
                                ? good_morning
                                : good_afternoon) +
                            ",",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: smallSize,
                        ),
                        children: [
                          TextSpan(
                              text: controller.firstName,
                              style: TextStyle(
                                  color: blueMainColorApp,
                                  fontSize: smallSize,
                                  fontWeight: FontWeight.w600)),
                        ]),
                  ),
                ],
              ),
            ),

            //For next version
            // InkWell(
            //   onTap: () => Get.snackbar(null, "Developing"),
            //   child: Stack(
            //     overflow: Overflow.visible,
            //     children: <Widget>[
            //       ImageCustomized(path: ic_notification, width: 20, height: 20,),
            //       new Positioned(
            //         left: 10,
            //         child: new Container(
            //           padding: EdgeInsets.all(1),
            //           decoration: new BoxDecoration(
            //             color: Colors.red,
            //             borderRadius: BorderRadius.circular(6),
            //           ),
            //           constraints: BoxConstraints(
            //             minWidth: 12,
            //             minHeight: 12,
            //           ),
            //           child: TextCustomized(
            //             text: "30",
            //             size: 10,
            //             weight: FontWeight.bold,
            //             color: Colors.white,
            //           ),
            //         ),
            //       )
            //     ],
            //   ),
            // )
          ],
        ),
      );
}
