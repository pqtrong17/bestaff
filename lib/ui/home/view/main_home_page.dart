import 'package:be_staff/ui/home/controller/main_home_controller.dart';
import 'package:be_staff/ui/home/view/home_page.dart';
import 'package:be_staff/ui/shift/view/shift_page.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

class MainHomePage extends GetView<MainHomeController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<MainHomeController>(
      builder: (value) => InitialWidget(
        onWillPop: () async => controller.onDoubleTap(),
        body: Stack(
          children: [
            IndexedStack(
              index: controller.currentTabIndex,
              children: [
                HomePage(),
                ShiftPage(),

                //For next version
                // Center(
                //   child: Text("This function is developing"),
                // ),
              ],
            ),
            Positioned(
              bottom: 16,
              right: 50,
              left: 50,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(40),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.15), blurRadius: 7)
                    ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: () => controller.onUpdateCurrentTab(0),
                      mouseCursor: SystemMouseCursors.zoomOut,
                      child: ImageCustomized(
                        path: controller.currentTabIndex == 0
                            ? ic_home_blue
                            : ic_home_gray,
                        width: 22,
                        height: 22,
                        padding: EdgeInsets.all(16),
                      ),
                    ),
                    InkWell(
                      onTap: () => controller.onUpdateCurrentTab(1),
                      mouseCursor: SystemMouseCursors.zoomOut,
                      child: ImageCustomized(
                        path: controller.currentTabIndex == 1
                            ? ic_calendar_home_blue
                            : ic_calendar_home_gray,
                        width: 22,
                        height: 22,
                        padding: EdgeInsets.all(16),
                      ),
                    ),

                    //For next version
                    // InkWell(
                    //   onTap: () => controller.onUpdateCurrentTab(2),
                    //   mouseCursor: SystemMouseCursors.zoomOut,
                    //   child: ImageCustomized(
                    //     path: controller.currentTabIndex == 2
                    //         ? ic_salary_blue
                    //         : ic_salary_gray,
                    //     width: 22,
                    //     height: 22,
                    //     padding: EdgeInsets.all(16),
                    //   ),
                    // ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}