import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/response/shift_response/shift_user_home.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/home/controller/home_controller.dart';
import 'package:be_staff/ui/home/controller/shift_user_controller.dart';
import 'package:be_staff/ui/shift/view/detail_shift_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShiftUserView extends GetView<ShiftUserController> {
  final List<ShiftUserHome> shiftUser;

  ShiftUserView(this.shiftUser);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<ShiftUserController>(
      builder: (value) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    TextCustomized(
                      text: your_shift,
                      weight: FontWeight.w600,
                      color: Colors.black,
                      size: mediumSize,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    InkWell(
                      onTap: () => controller.onPressedTimeKeeping(),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: controller.isCheckOut
                              ? Colors.green
                              : blueMainColorApp,
                        ),
                        child: Row(
                          children: [
                            controller.isCalling
                                ? Theme(
                                    data: ThemeData(
                                        cupertinoOverrideTheme:
                                            CupertinoThemeData(
                                                brightness: Brightness.dark)),
                                    child: CupertinoActivityIndicator(
                                      radius: 5,
                                    ))
                                : ImageCustomized(
                                    path: ic_watch,
                                    width: 14,
                                    height: 14,
                                  ),
                            SizedBox(
                              width: 7,
                            ),
                            TextCustomized(
                              text: controller.isCheckOut
                                  ? check_out_text
                                  : time_keeping,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                // TextCustomized(
                //   text: see_all_text + " (${shiftUser.length})",
                //   size: smallSize,
                // ),
              ],
            ),
          ),
          shiftUser.isNotEmpty
              ? Container(
                  height: 150,
                  padding: EdgeInsets.only(left: 24),
                  child: ListView.builder(
                    itemCount: shiftUser.length + 1,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => _buildItemShift(index),
                  ),
                )
              : TextCustomized(
                  text: no_shift,
                  isCenter: true,
                )
        ],
      ),
    );
  }

  Widget _buildItemShift(int index) {
    HomeController homeController = Get.find();
    bool isItemShift = index != shiftUser.length;
    String position;
    if(isItemShift){
      shiftUser[index].dataItem.dataUser.forEach((element) {
        if(element.userId == StorageInfoUser().getInfoUser.userId){
          position = element.namePosition;
        }
      });
    }
    return index != shiftUser.length
        ? InkWell(
      onTap: () => Get.to(DetailShiftPage(), arguments: shiftUser[index]),
      child: Row(
        children: [
          Container(
            width: Get.width * 2 / 3,
            margin: EdgeInsets.symmetric(vertical: 8),
            decoration: BoxDecoration(
                color: ColorHelper.parseColor("#FBFBFB"),
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                      color:
                      Colors.black.withOpacity(0.1),
                      blurRadius: 2,
                      offset: Offset(4, 4))
                ]),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: 8),
                    decoration: BoxDecoration(
                        color: blueMainColorApp,
                        borderRadius:
                        BorderRadius.circular(8)),
                    child: IntrinsicWidth(
                        child:
                        shiftUser[index].getTimes !=
                            null
                            ? _buildItemToday(index)
                            : _buildItemNotToday(index)),
                  ),
                ),
                SizedBox(
                  width: 7,
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    mainAxisAlignment:
                    MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        children: [
                          ImageCustomized(
                            path: ic_clock,
                            width: 8,
                            height: 8,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          TextCustomized(
                            text: shiftUser[index]
                                .dataItem
                                .startTime +
                                " - " +
                                shiftUser[index]
                                    .dataItem
                                    .endTime,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          ImageCustomized(
                            path: ic_rain,
                            width: 17,
                            height: 17,
                          )
                        ],
                      ),
                      Row(
                        children: [
                          ImageCustomized(
                            path: ic_place,
                            width: 8,
                            height: 8,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: TextCustomized(
                              text: shiftUser[index]
                                  .dataItem
                                  .dataUser
                                  .length !=
                                  0
                                  ? shiftUser[index]
                                  .dataItem
                                  ?.dataUser[0]
                                  .position
                                  : "",
                              textOverflow:
                              TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            width: 8,
                            height: 8,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: shiftUser[index]
                                    .dataItem
                                    .dataUser
                                    .length !=
                                    0
                                    ? ColorHelper
                                    .parseColor(
                                    shiftUser[
                                    index]
                                        .dataItem
                                        .dataUser[
                                    0]
                                        .color)
                                    : null),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          TextCustomized(
                            text: position,
                            color: shiftUser[index]
                                .dataItem
                                .dataUser
                                .length !=
                                0
                                ? ColorHelper.parseColor(
                                shiftUser[index]
                                    .dataItem
                                    .dataUser[0]
                                    .color)
                                : null,
                            size: normalSize,
                          )
                        ],
                      ),
                      // Row(
                      //   crossAxisAlignment:
                      //   CrossAxisAlignment.start,
                      //   children: [
                      //     CircleImage(
                      //         path: ic_avatar_example,
                      //         insideRadius: 12),
                      //     SizedBox(
                      //       width: 5,
                      //     ),
                      //     CircleImage(
                      //       path: ic_avatar_example,
                      //       insideRadius: 12,
                      //     ),
                      //     SizedBox(
                      //       width: 5,
                      //     ),
                      //     CircleImage(
                      //       path: ic_avatar_example,
                      //       insideRadius: 12,
                      //     ),
                      //     SizedBox(
                      //       width: 5,
                      //     ),
                      //     CircleImage(
                      //       path: ic_avatar_example,
                      //       insideRadius: 12,
                      //     ),
                      //   ],
                      // ),
                      SizedBox(
                        height: 24,
                        child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: shiftUser[index]
                                              .dataItem
                                              .dataUser
                                              .length <
                                          4
                                      ? shiftUser[index]
                                          .dataItem
                                          .dataUser
                                          .length
                                      : 4,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, position) {
                                    Widget item;
                                    if(shiftUser[index].dataItem.dataUser.length >= 4 && position == 3){
                                      final text = "+${(shiftUser[index].dataItem.dataUser.length - 4)}";
                                      item = CircleAvatar(
                                        backgroundColor: ColorHelper.parseColor("#E1E1E1"),
                                        radius: 12,
                                        child: TextCustomized(
                                          text: text,
                                          size: smallSize,
                                        ),
                                      );
                                    }else{
                                      final avatar = shiftUser[index].dataItem.dataUser[position].avatar;
                                      bool isUrl = avatar != null ? Uri.parse(avatar).isAbsolute : false;
                                      if(avatar != null && avatar != "" && isUrl){
                                        item = CircleImage.network(
                                          path: avatar,
                                          insideRadius: 12,
                                        );
                                      }else{
                                        final String fullName = shiftUser[index].dataItem.dataUser[position].fullName;
                                        List<String> stringNames = fullName.split(" ");
                                        var text;
                                        if(stringNames.length> 1){
                                          text = stringNames[stringNames.length -2][0] + stringNames[stringNames.length -1][0];
                                        }else{
                                          text = fullName[0].toUpperCase() + fullName[1].toUpperCase();
                                        }
                                        item =  CircleAvatar(
                                          backgroundColor: ColorHelper.parseColor("#E1E1E1"),
                                          radius: 12,
                                          child: TextCustomized(
                                            text: text,
                                            size: smallSize,
                                          ),
                                        );
                                      }
                                    }
                                    return Padding(padding: EdgeInsets.all(1), child: item,);
                                  }),
                            )
                          ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 10),
                  child: ImageCustomized(
                      path: ic_forward,
                      width: 5,
                      height: 14),
                )
              ],
            ),
          ),
          SizedBox(
            width: 25,
          )
        ],
      ),
    )
        : controller.isHiddenSeeMore
        ? Container()
        : Row(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                homeController.onPressedSeeMore();
                controller.onHiddenSeeMore();
              },
              child: CircleAvatar(
                radius: 17,
                backgroundColor: Colors.grey,
                child: CircleAvatar(
                  radius: 16,
                  child: Icon(Icons
                      .arrow_forward_ios_rounded),
                  backgroundColor: Colors.white,
                  foregroundColor: Colors.black54,
                ),
              ),
            ),
            SizedBox(
              height: 6,
            ),
            TextCustomized(
              text: see_more_text,
              color: Colors.grey.shade600,
            )
          ],
        ),
        SizedBox(
          width: 25,
        ),
      ],
    );
  }

  Widget _buildItemToday(int index) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextCustomized(
            text: shiftUser[index].getTimes.toUpperCase(),
            size: mediumSize,
            color: Colors.white,
          ),
          Container(
            height: 1,
            margin: EdgeInsets.symmetric(horizontal: 20),
            color: Colors.white,
            width: double.infinity,
          ),
          TextCustomized(
            text: shiftUser[index].nameDay.toUpperCase(),
            size: 25,
            color: Colors.white,
            isCenter: true,
          ),
          TextCustomized(
            text: shiftUser[index].getDate,
            size: 25,
            color: Colors.white,
          ),
        ],
      );

  Widget _buildItemNotToday(int index) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextCustomized(
            text: shiftUser[index].nameDay,
            size: 24,
            isCenter: true,
            color: Colors.white,
          ),
          Container(
            height: 1,
            margin: EdgeInsets.symmetric(horizontal: 20),
            color: Colors.white,
            width: double.infinity,
          ),
          TextCustomized(
            text: shiftUser[index].getDate,
            size: 30,
            color: Colors.white,
          ),
        ],
      );
}
