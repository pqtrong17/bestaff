import 'package:be_staff/data/models/user.dart';
import 'package:be_staff/data/response/user_response.dart';

abstract class LoginContract {
  void onLoginSuccess(UserResponse response);

  void onLoginError(Exception exception);
}