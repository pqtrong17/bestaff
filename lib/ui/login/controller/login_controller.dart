import 'dart:convert';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/models/user.dart';
import 'package:be_staff/data/repositories/login_repositories.dart';
import 'package:be_staff/data/request/login_request.dart';
import 'package:be_staff/data/response/user_response.dart';
import 'package:be_staff/ui/home/view/home_page.dart';
import 'package:be_staff/ui/home/view/main_home_page.dart';
import 'package:be_staff/ui/login/contract/login_contract.dart';
import 'package:be_staff/ui/register/view/enter_code_page.dart';
import 'package:be_staff/ui/register/view/tutorial_page.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController extends GetxController implements LoginContract {
  bool isShowPassword = true;
  bool isSaveData = true;
  bool isFocusTextField = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  FocusNode passWordFocus = FocusNode();

  LoginRepositories loginRepositories;
  LoginContract contract;
  LoginRequest request;

  bool isInvalidEmail = false;
  int typeInvalidEmail;
  bool isInvalidPassword = false;

  bool isFromRegisterPage = false;
  bool isSeeTutorial = false;


  @override
  void onInit() {
    super.onInit();
    print('onInit Login Controller');
    loginRepositories = Injector().onLogin;
    contract = this;
    onGetCacheLoginInfo().then((value) {
      if(value != null){
        String email = value['email'];
        String password = value['password'];
        emailController.text = email;
        passwordController.text = password;
      }
    });

    onGetIsSaveData().then((value) {
      if(value != null){
        isSaveData = value;
        update();
      }
    });
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    emailController.text = Get.arguments;
    if(emailController.text != ""){
      passWordFocus.requestFocus();
    }
    update();
  }

  void showPassword(bool value) {
    isShowPassword = value;
    update();
  }

  void saveData(bool value) {
    isSaveData = value;
    update();
  }

  void focusTextField(bool value) {
    isFocusTextField = value;
    update();
  }

  void isCheckInvalidEmail(){
    if(emailController.text != "" && GetUtils.isEmail(emailController.text)){
      isInvalidEmail = false;
    }else{
      isInvalidEmail = true;
      if(emailController.text == ""){
        typeInvalidEmail = 1;
        return;
      }
      if(!GetUtils.isEmail(emailController.text)){
        typeInvalidEmail = 2;
      }
    }
  }

  void isCheckInvalidPassword(){
    if(passwordController.text != ""){
      isInvalidPassword = false;
    }else{
      isInvalidPassword = true;
    }
  }

  void onLogin() {
    isCheckInvalidEmail();
    isCheckInvalidPassword();
    update();
    if (!isInvalidEmail && !isInvalidPassword) {
      Get.dialog(LoadingDialog());
      onCacheIsSaveData();
      if(isSaveData){
        onCacheLoginInfo();
      }else{
        onClearLoginInfo();
      }
      request = LoginRequest();
      request.password = passwordController.text;
      request.email = emailController.text;
      loginRepositories
          .onLogin(request)
          .then((value) => contract.onLoginSuccess(value))
          .catchError((onError) {
            return contract.onLoginError(onError);
      });
    }
  }

  void onLoginFromOutside(TextEditingController email, TextEditingController password, bool isRegister, bool isSeeTut){
    isFromRegisterPage = isRegister;
    emailController = email;
    passwordController = password;
    this.isSeeTutorial = isSeeTut;
    onLogin();
  }

  Future<void> onCacheLoginInfo() async {
    Map map = Map();
    map = {
      "email": emailController.text,
      "password": passwordController.text,
    };
    String json = jsonEncode(map);
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("loginInfo", json);
  }

  Future<void> onCacheIsSaveData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool("isSaveDataLogin", isSaveData);
  }

  Future<void> onClearLoginInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if(preferences.getString("loginInfo") != null){
      preferences.remove("loginInfo");
    }
  }

  Future<Map<String, dynamic>> onGetCacheLoginInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Map map;
    if(preferences.getString("loginInfo") != null) {
      String loginInfoJson = preferences.getString("loginInfo");
      map  = Map();
      map = jsonDecode(loginInfoJson);
    }
    return map;
  }

  Future<bool> onGetIsSaveData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool("isSaveDataLogin");
  }

  @override
  void onLoginError(Exception exception) {
    Get.back();
    Get.dialog(MessageDialog(
      title: error_text,
      description: info_login_wrong,
      onPressedOK: () => Get.back(),
    ));
  }

  @override
  Future<void> onLoginSuccess(UserResponse response) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("infoUser", jsonEncode(response.toJson()));
    StorageInfoUser().setInfoUser(response);
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.offAll(MainHomePage());
  }
}
