import 'package:be_staff/ui/login/controller/login_controller.dart';
import 'package:be_staff/ui/register/view/register_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/values/styles.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<LoginController> {

  @override
  Widget build(BuildContext context) {

    return GetBuilder<LoginController>(builder: (value) {
      return InitialWidget.expand(
        isShowLeadingIconAppBar: false,
        titleAppBar: login_be_staff,
        styleTitleAppBar: TextStyle(
          color: blueMainColorApp,
          fontSize: 30,
          fontWeight: FontWeight.w600,
        ),
        backgroundAppBar: Colors.transparent,
        body: Column(
          children: [
            _buildContentInput(),
            _buildBottomButton(),
          ],
        ),
      );
    });
  }

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      title: TextCustomized(
        text: login_be_staff,
        color: blueMainColorApp,
        font: fBarlowMedium,
        size: 30,
        weight: FontWeight.w600,
      ),
      centerTitle: true,
    );
  }

  Widget _buildContentInput() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 16,
          ),
          Container(
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: TextFieldCustomized(
                backgroundColor: f6GrayColor,
                borderRadius: 8,
                onFocused: (value){
                  controller.focusTextField(value);
                },
                controller: controller.emailController,
                hintText: enter_email_phone,
                hintStyle: styleHintInput,
                colorBorderEnable: e8GrayColor,
                errorText: controller.typeInvalidEmail != null
                    ? controller.typeInvalidEmail == 1
                        ? this_field_required
                        : email_wrong_format
                    : null,
                validator: () => controller.isInvalidEmail,
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: TextFieldCustomized(
              backgroundColor: f6GrayColor,
              borderRadius: 8,
              onFocused: (value){
                controller.focusTextField(value);
              },
              isObscured: controller.isShowPassword,
              controller: controller.passwordController,
              focusNode: controller.passWordFocus,
              hintText: password,
              hintStyle: styleHintInput,
              colorBorderEnable: e8GrayColor,
              errorText: this_field_required,
              validator: () => controller.isInvalidPassword,
              suffixIcon: GestureDetector(
                onTap: () {
                  controller.showPassword(!controller.isShowPassword);
                },
                // child: Center(
                //   child: TextCustomized(
                //     text: controller.isShowPassword ? show : hide,
                //     color: gray88Color,
                //     font: fBarlowMedium,
                //     weight: FontWeight.w500,
                //     size: 16,
                //     isCenter: true,
                //   ),
                // ),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: ImageCustomized(
                    path: !controller.isShowPassword ? ic_eyes_close :ic_eyes_open,
                    width: 40,
                    height: 40,
                  ),
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Checkbox(
                  value: controller.isSaveData,
                  onChanged: (value) {
                    controller.saveData(value);
                  }),
              TextCustomized(
                text: save_data,
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBottomButton() => Column(
    children: [
      ButtonCustomized(
        toUpperCase: false,
        text: login,
        weight: FontWeight.w600,
        backgroundColor: blueMainColorApp,
        borderRadius: 240,
        padding: EdgeInsets.symmetric(vertical: 16),
        margin: EdgeInsets.symmetric(horizontal: 20),
        width: Get.width,
        onPressed: () {
          controller.onLogin();
        },
      ),
      SizedBox(height: 20,),
      ButtonCustomized(
        toUpperCase: false,
        text: register,
        weight: FontWeight.w600,
        backgroundColor: blueMainColorApp,
        borderRadius: 240,
        width: Get.width,
        padding: EdgeInsets.symmetric(vertical: 16),
        margin: EdgeInsets.symmetric(horizontal: 20),
        onPressed: () {
          Get.to(RegisterPage());
        },
      ),
      Container(
        margin: EdgeInsets.only(bottom: 15, top: 15),
        child: InkWell(
          onTap: () => Get.snackbar(null, "Developing"),
          child: TextCustomized(
            text: forgot_pass,
            color: blueMainColorApp,
            weight: FontWeight.w600,
            size: mediumSize,
            isCenter: true,
          ),
        ),
      )
    ],
  );
}
