import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/profile_repositories.dart';
import 'package:be_staff/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AccountController extends GetxController {
  double rating = 0;
  int stateLoading = -1;
  bool isLoadingRating = false;
  TextEditingController contentRatingController = TextEditingController();

  ProfileRepositories repositories;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repositories = Injector().onGetProfile;
  }

  void onRating(){
    stateLoading = 1; // Loading
    update();
    final String star = rating.toInt().toString();
    final String content = contentRatingController.text;
    repositories.onRating(star, content).then((value) {
      stateLoading = 2; // Success
      update();
    }).catchError((onError){
      Get.log('========================== $onError');
      stateLoading = -1;
      update();
      Get.back();
      Get.snackbar(null, error_text);
    });
  }
}