import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/login/view/login_page.dart';
import 'package:be_staff/ui/profile/controller/account_controller.dart';
import 'package:be_staff/ui/profile/view/profile_page.dart';
import 'package:be_staff/ui/profile/view/rating_view.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:be_staff/ui/request_management/view/request_management_page.dart';

class AccountPage extends GetView {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InitialWidget(
      titleAppBar: account_text,
      styleTitleAppBar: TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: bigSize,
        color: Colors.black
      ),
      backgroundAppBar: Colors.transparent,
      backgroundColor: ColorHelper.parseColor("#F1F1F1"),
      leadingIconAppBar: InkWell(
        onTap: () => Get.back(),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: ImageCustomized(
            path: ic_back_account,
            width: 18,
            height: 18,
          ),
        ),
      ),
      isShowLeadingIconAppBar: true,
      isCenterTitleAppBar: false,
      body: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                SizedBox(height: 8,),
                InkWell(
                    onTap: () => Get.to(ProfilePage()),
                    child: _buildProfile()),
                SizedBox(height: 8,),
                Expanded(child: _buildInfoAccount()),
                // SizedBox(height: 8,),
                // _buildOtherItem(),
                // SizedBox(height: 8,),
                // Container(
                //   color: Colors.white,
                //   child: Material(
                //     color: Colors.transparent,
                //     child: InkWell(
                //       onTap: () => Get.log("---"),
                //       child: Padding(
                //         padding: EdgeInsets.symmetric(vertical: 8, horizontal: 28),
                //         child: Row(
                //           children: [
                //             ImageCustomized(path: ic_gift, width: 18, height: 18,),
                //             SizedBox(width: 12,),
                //             Expanded(
                //               child: TextCustomized(
                //                 text: gift_text,
                //                 size: mediumSize,
                //               ),
                //             ),
                //             Container(
                //               padding: EdgeInsets.all(4),
                //               decoration: BoxDecoration(
                //                 color: ColorHelper.parseColor("#E4F9FF"),
                //                 borderRadius: BorderRadius.circular(3)
                //               ),
                //               child: TextCustomized(
                //                 text: see_now,
                //                 color: ColorHelper.parseColor("#31A7FF"),
                //               ),
                //             )
                //           ],
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
                SizedBox(height: 20,),
                InkWell(
                    onTap: () => Get.bottomSheet(_BottomSheet()),
                    child: _buildRating()),
                SizedBox(height: 40,),

              ],
            ),
          ),
          _buildBottomRow(),
          SizedBox(height: 12,),
        ],
      ),
    );
  }

  Widget  _buildProfile()=> Stack(
    children: [
      Positioned.fill(child: ImageCustomized(path: bg_account, fit: BoxFit.cover, width: Get.width)),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: [
                    ColorHelper.parseColor("#7433FF"),
                    ColorHelper.parseColor("#FFA3FD"),
                  ])),
              child: CircleImage(
                path: ic_avatar_example,
                insideRadius: 41/2,
              ),
            ),
            SizedBox(width: 12,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextCustomized(
                    text: StorageInfoUser()?.getInfoUser?.fullName,
                    color: Colors.white,
                    weight: FontWeight.w500,
                    size: 20,
                  ),
                  TextCustomized(
                    text: see_your_profile,
                    color: Colors.white,
                    weight: FontWeight.w300,
                    size: smallSize,
                  ),

                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(3)
              ),
              padding: EdgeInsets.all(3),
              child: TextCustomized(
                text: unverified,
                color: ColorHelper.parseColor("#2A68F7"),
              ),
            ),
            SizedBox(width: 12,),
            ImageCustomized(path: ic_forward, color: Colors.white, width: 4, height: 13,)
          ],
        ),
      ),
    ],
  );

  Widget _buildInfoAccount() => Container(
    color: Colors.white,
    child: Column(
      children: [
        _buildItem(request_management, ic_request_management, description: description_request_management, isDivide: true, onPressed: () => Get.to(RequestManagementPage())),
        // _buildItem(notification_setting, ic_notification, isDivide: true),
        // _buildItem(general_setting, ic_setting_account, description: description_general),
      ],
    ),
  );

  Widget _buildOtherItem()=> Container(
    color: Colors.white,
    child: Column(
      children: [
        _buildItem(support_text, ic_support, description: description_support, isDivide: true),
        SizedBox(height: 9,),
        _buildItem(report_error, ic_report_error, isDivide: true),
        SizedBox(height: 9,),
        _buildItem(other_text, ic_other, description: description_other),
      ],
    ),
  );

  Widget _buildRating() => Column(
    children: [
      Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [ColorHelper.parseColor("#31A7FF"), ColorHelper.parseColor("#A8EAFF"), ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 36, horizontal: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: RatingBar(
                          ratingWidget: RatingWidget(
                              empty: ImageCustomized(
                                path: ic_star_rating,
                                width: 32,
                                height: 32,
                              ),
                              full: ImageCustomized(
                                path: ic_star_rating_full,
                                width: 32,
                                height: 32,
                              ),
                              half: Container()),
                          onRatingUpdate: (value) => Get.log("----- $value"),
                          itemCount: 5,
                          allowHalfRating: false,
                          ignoreGestures: true,
                          initialRating: 3.0,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextCustomized(
                            text: rating_be_staff,
                            weight: FontWeight.w600,
                            color: Colors.white,
                          ),
                          SizedBox(height: 10,),
                          // TextCustomized(
                          //   text: description_rating_be_staff,
                          //   weight: FontWeight.w300,
                          //   size: smallSize,
                          //   color: Colors.white,
                          // ),
                        ],
                      ),
                    ),

                  ],
                ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.centerRight,
                child: ImageCustomized(
                    path: bg_rating_banner,
                    height: 187,
                    width: 174,
                    fit: BoxFit.cover),
              ),
            ),
          ],
        ),
          ),
    ],
  );

  Widget _buildBottomRow() => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 30),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextCustomized(
          text: current_version_text,
          color: ColorHelper.parseColor("#666666"),
        ),
        InkWell(
          onTap: () => Get.dialog(_buildDialog()),
          child: Row(
            children: [
              Icon(Icons.logout, size: 18, color: Colors.black54,),
              SizedBox(width: 5,),
              TextCustomized(
                text: log_out_text,
                color: ColorHelper.parseColor("#666666"),
              ),
            ],
          ),
        )
      ],
    ),
  );

  Widget _buildDialog() => AlertDialog(
    content: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextCustomized(
          text: do_you_want_logout,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FlatButton(
              child: TextCustomized(
                text: yes_text,
                weight: FontWeight.w600
              ),
              onPressed: () async {
                await StorageInfoUser().clearAll();
                Get.offAll(() => LoginPage());
              },
            ),
            FlatButton(
              child: TextCustomized(
                text: no_text,
                weight: FontWeight.w600,
              ),
              onPressed: () {
                Get.back();
              },
            ),
          ],
        )
      ],
    ),
    title: TextCustomized(
      text: log_out_text,
      weight: FontWeight.w600,
    ),
  );

  Widget _buildItem(String title, String url, {String description, bool isDivide = false, Function onPressed}) => Material(
    color: Colors.transparent,
    child: InkWell(
      onTap: () => onPressed(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Column(
          children: [
            SizedBox(height: description == null ? 16 : 8,),
            Row(
              crossAxisAlignment: description != null ? CrossAxisAlignment.center : CrossAxisAlignment.start,
              children: [
                ImageCustomized(path: url, width: 18, height: 18,),
                SizedBox(width: 12,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextCustomized(
                        text: title,
                        color: Colors.black,
                        size: mediumSize,
                      ),
                      description != null ? Column(
                        children: [
                          SizedBox(height: 2,),
                          TextCustomized(
                            text: description,
                            weight: FontWeight.w100,
                          ),
                        ],
                      ) : Container(),
                      SizedBox(height: description == null ? 16 : 8,),
                      isDivide ? Divider(
                        color: ColorHelper.parseColor("#F0F0F0"),
                        height: 1,
                      ) : Container(),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    ),
  );

}

class _BottomSheet extends GetView<AccountController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AccountController>(
      builder: (value) => Container(
        color: Colors.white,
        padding: EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () => Get.back(),
                child: ImageCustomized(
                  path: ic_close,
                  width: 14,
                  height: 14,
                ),
              ),
              SizedBox(
                height: 6,
              ),
              controller.stateLoading != 2 ? Column(
                children: [
                  RatingView(
                    onRatingChanged: (value) {
                      controller.rating = value;
                      controller.update();
                    },
                    rating: controller.rating,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: ColorHelper.parseColor("#FCFCFC"),
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(
                            color: ColorHelper.parseColor("#E8E8E8"), width: 1)),
                    child: Column(
                      children: [
                        TextFieldCustomized(
                          controller: controller.contentRatingController,
                          hintText: send_feedback_for_us,
                          hintStyle:
                          TextStyle(color: ColorHelper.parseColor("#BDBDBD")),
                          contentPadding: EdgeInsets.all(5),
                          colorBorderEnable: Colors.transparent,
                          colorBorderFocus: Colors.transparent,
                          backgroundColor: Colors.transparent,
                          maxLine: 4,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            ImageCustomized(
                              path: ic_voice,
                              width: 17,
                              height: 17,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            TextCustomized(
                              text: press_to_recording,
                              color: ColorHelper.parseColor("#9D9D9D"),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  ButtonCustomized(
                          text: controller.stateLoading < 0 ? send_text : null,
                          child: controller.stateLoading == 1
                              ? Theme(
                                  data: ThemeData(
                                      cupertinoOverrideTheme:
                                          CupertinoThemeData(
                                              brightness: Brightness.dark)),
                                  child: CupertinoActivityIndicator(
                                    radius: 5,
                                  ))
                              : null,
                          colorText: Colors.white,
                          backgroundColor: blueMainColorApp,
                          width: Get.width,
                          borderRadius: 100,
                          onPressed: () {
                            controller.onRating();
                          },
                        ),
                      ],
              ) : _buildThanksView
            ],
          ),
        ),
      ),
    );
  }

  final _buildThanksView = Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      ImageCustomized(
        path: ic_thanks,
        width: 150,
        height: 150,
      ),
      SizedBox(
        height: 24,
      ),
      TextCustomized(
        text: send_feed_back_success,
        weight: FontWeight.w500,
        color: ColorHelper.parseColor("#454545"),
        isCenter: true,
      ),
      SizedBox(
        height: 24,
      ),
      ButtonCustomized(
        text: close_text,
        colorText: Colors.white,
        backgroundColor: blueMainColorApp,
        width: Get.width,
        borderRadius: 100,
        onPressed: () {
          Get.back();
        },
      )
    ],
  );
}
