import 'package:be_staff/data/response/detail_user_info.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/profile/controller/profile_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BasicInfoView extends GetView<ProfileController>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _BasicInfoItem(
          ic_phone,
          telephone_number_text,
          controller: controller.mobileController,
        ),
        _BasicInfoItem(
          ic_email,
          email_text,
          controller: controller.emailController,
        ),
        _BasicInfoItem(
          ic_home,
          permanent_address,
          controller: controller.addressController,
        ),

        //For next version
        // _BasicInfoItem(ic_identify_card, verification_account),

        _BasicInfoItem(ic_birthday, update_dob),
        _BasicInfoItem(ic_gender, insert_gender),
        _BasicInfoItem(
          ic_grid_round,
          pin_to_login,
          content: controller.mInfoUser.result.codeCompany,
          isShowForwardIcon: true,
        ),
        _BasicInfoItem(
          ic_position,
          position_text,
          content: controller.userPosition,
          isShowForwardIcon: true,
        ),
        _BasicInfoItem(ic_department, department_text,
            content: controller.mInfoUser.result.deptId),
      ],
    );
  }
}

class _BasicInfoItem extends GetView {
  final String pathImage;
  final String title;
  final String content;
  final bool isShowForwardIcon;
  final TextEditingController controller;

  _BasicInfoItem(this.pathImage, this.title,
      {this.content, this.isShowForwardIcon = false, this.controller});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Row(
          children: [
            Stack(
              children: [
                Padding(
                    padding: EdgeInsets.all(5),
                    child: ImageCustomized(
                      path: pathImage,
                      width: 18,
                      height: 18,
                    )),
                content != null || controller != null
                    ? Container()
                    : Positioned(
                        bottom: 2,
                        right: 2,
                        child: ImageCustomized(
                          path: ic_missing_field,
                          width: 10,
                          height: 10,
                        ))
              ],
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextCustomized(
                            text: title,
                            color: content == null && controller == null
                                ? blueMainColorApp
                                : ColorHelper.parseColor("#666666"),
                            size: content == null ? mediumSize : normalSize,
                          ),
                          controller != null
                              ? TextFieldCustomized(
                                  controller: controller,
                                  textStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: mediumSize,
                                  ),
                                  colorBorderEnable: Colors.transparent,
                                  colorBorderFocus: Colors.transparent,
                                  contentPadding: EdgeInsets.all(0),
                                )
                              : content != null
                                  ? TextCustomized(
                                      text: content,
                                      size: mediumSize,
                                      color: black,
                                    )
                                  : Container(),
                        ],
                      ),
                    ),
                    isShowForwardIcon
                        ? ImageCustomized(
                            path: ic_forward,
                            width: 4,
                            height: 13,
                          )
                        : Container()
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  color: ColorHelper.parseColor("#F0F0F0"),
                  height: 1,
                ),
              ],
            )),
          ],
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
