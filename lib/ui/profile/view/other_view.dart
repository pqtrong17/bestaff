import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/profile/controller/profile_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OtherView extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        _buildExperience(),
        SizedBox(height: 20,),
        _buildCertificate(),
        SizedBox(height: 20,),
        // _buildEducation()
      ],
    );
  }
  Widget _buildExperience() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      _OtherItem(experience, insert_experience, ic_experience),
      ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, position) => _SubOtherItem(
            controller.mExperience[position].dateTime,
            controller.mExperience[position].name,
            ic_experience),
        itemCount: controller.mExperience.length,
      )
    ],
  );

  Widget _buildCertificate() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      _OtherItem(certification, insert_certification, ic_certification),
      ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, position) => _SubOtherItem(
            controller.mDegree[position].dateTime,
            controller.mDegree[position].name,
            ic_experience),
        itemCount: controller.mDegree.length,
      )
    ],
  );

  Widget _buildEducation() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      _OtherItem(education, insert_education, ic_education),
      ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (context, index) => _SubOtherItem(
            "16 tháng 3, 2011 - 6 tháng 5, 2020",
            "QTKD tại Đại học Kinh Tế - Đà Nẵng",
            ic_experience),
        itemCount: 1,
      )
    ],
  );

}

class _OtherItem extends GetView{
  final String title;
  final String subTitle;
  final String pathIcon;

  _OtherItem(this.title, this.subTitle, this.pathIcon);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextCustomized(
          text: title,
          weight: FontWeight.w600,
          color: Colors.black,
          size: 20,
        ),
        SizedBox(height: 16,),
        Row(
          children: [
            ImageCustomized(path: pathIcon, width: 18, height: 18,),
            SizedBox(width: 9,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextCustomized(
                    text: subTitle,
                    color: blueMainColorApp,
                    size: mediumSize,
                  ),
                  SizedBox(height: 10,),
                  Divider(color: ColorHelper.parseColor("#F0F0F0"), height: 1,)
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}

class _SubOtherItem extends GetView {
  final String time;
  final String title;
  final String pathIcon;

  _SubOtherItem(this.time, this.title, this.pathIcon);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        SizedBox(height: 12,),
        Row(
          children: [
            ImageCustomized(path: pathIcon, width: 18, height: 18,),
            SizedBox(width: 9,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextCustomized(
                    text: time,
                    color: ColorHelper.parseColor("#666666"),
                  ),
                  TextCustomized(
                    text: title,
                    color: ColorHelper.parseColor("#666666"),
                  ),
                  SizedBox(height: 10,),
                  Divider(color: ColorHelper.parseColor("#F0F0F0"), height: 1,)
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}