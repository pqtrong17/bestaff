import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/profile/controller/profile_controller.dart';
import 'package:be_staff/ui/profile/view/basic_info_view.dart';
import 'package:be_staff/ui/profile/view/other_view.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class ProfilePage extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<ProfileController>(
      builder: (value)=> InitialWidget(
          body: controller.mInfoUser != null ? SingleChildScrollView(
            child: Stack(
              children: [
                _buildAvatar(),
                Container(
                  margin: EdgeInsets.only(top: 280, bottom: 20, left: 12, right: 12),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, -4))
                      ],
                      color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 14),
                    child: _TabBar(),
                  ),
                )
              ],
            ),
          ) : Center(child: CircularProgressIndicator(),)
      ),
    );
  }

  Widget _buildAvatar() {
    final avatar = controller.mInfoUser.result.avatar;
    Widget _itemAvatar;
    if (avatar != null && avatar != "") {
      _itemAvatar = CircleImage.network(
        path: avatar,
        insideRadius: 136 / 2,
      );
    } else {
      final String fullName = controller.mInfoUser.result.fullName;

      List<String> stringNames = fullName.split(" ");
      var text;
      if(stringNames.length <= 1){
        text = fullName[0].toUpperCase() + fullName[1];
      }else{
        text = stringNames[stringNames.length - 2][0] +
            stringNames[stringNames.length - 1][0];
      }

      _itemAvatar = CircleAvatar(
        backgroundColor: ColorHelper.parseColor("#E1E1E1"),
        radius: 136/2,
        child: TextCustomized(
          text: text,
          size: largeSize,
        ),
      );
    }
    return Stack(
      children: [
        ImageCustomized(
          path: bg_avatar_profile,
          height: 320,
          width: Get.width,
          fit: BoxFit.cover,
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => Get.back(),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ImageCustomized(
                          path: ic_back_account,
                          width: 18,
                          height: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Expanded(child: Container()),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => controller.onPressedSave(),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: TextCustomized(
                          text: save_text.toUpperCase(),
                          weight: FontWeight.w500,
                          color: Colors.white,
                          size: mediumSize,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 8,
            ),
            InkWell(
              onTap: () => Get.bottomSheet(
                  _buildBottomSheet(),
                  backgroundColor: Colors.white),
              child: Stack(
                children: [
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(colors: [
                          ColorHelper.parseColor("#7433FF"),
                          ColorHelper.parseColor("#FFA3FD"),
                        ])),
                    child: controller.avatarFile != null ? CircleImage.file(
                      file: controller.avatarFile,
                      insideRadius: 136/2,
                    ) : _itemAvatar,
                  ),
                  Positioned(
                    bottom: 8,
                    right: 6,
                    child: Container(
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: ColorHelper.parseColor("#F1F1F1"),
                      ),
                      child: ImageCustomized(
                        path: ic_camera,
                        width: 20,
                        height: 20,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 6,
            ),
            TextCustomized(
              text: StorageInfoUser()?.getInfoUser?.fullName,
              weight: FontWeight.w600,
              color: Colors.white,
              size: 28,
            )
          ],
        )
      ],
    );
  }

  Widget _buildBottomSheet() => Padding(
    padding: const EdgeInsets.symmetric(vertical: 16),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: () => controller.onEditAvatar(isFromCamera: true),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: TextCustomized(
                  text: from_camera,
                  size: mediumSize,
                  weight: FontWeight.w600,
                  isCenter: true,
                ),
              ),
            ],
          ),
        ),
        InkWell(
          onTap: () => controller.onEditAvatar(isFromCamera: false),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: TextCustomized(
                    text: from_gallery,
                    size: mediumSize,
                    isCenter: true,
                    weight: FontWeight.w600),
              ),
            ],
          ),
        )
      ],
    ),
  );
}

class _TabBar extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: InkWell(
                    onTap: () {
                      controller.onUpdateStateTabBar(false);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextCustomized(
                            text: basic_information,
                            color: !controller.isOtherTabBar ? blueMainColorApp : ColorHelper.parseColor("#CBCBCB"),
                            weight: !controller.isOtherTabBar ? FontWeight.w600 : FontWeight.w400,
                          ),
                          !controller.isOtherTabBar ? Container(
                            margin: EdgeInsets.only(top: 5),
                            width: 9,
                            height: 9,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: LinearGradient(
                                    colors: [
                                      blueMainColorApp,
                                      blueMainColorApp.withOpacity(0.27)
                                    ],
                                    begin: Alignment.bottomCenter,
                                    end: Alignment.topCenter)),
                          ) : Container(width: 9, height: 9, margin: EdgeInsets.only(top: 5),)
                        ],
                      ),
                    ))),
            Expanded(
                child: InkWell(
                    onTap: () {
                      controller.onUpdateStateTabBar(true);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextCustomized(
                            text: other_text.toUpperCase(),
                            color: controller.isOtherTabBar ? blueMainColorApp : ColorHelper.parseColor("#CBCBCB"),
                            weight: controller.isOtherTabBar ? FontWeight.w600 : FontWeight.w400,
                          ),
                          controller.isOtherTabBar ? Container(
                            margin: EdgeInsets.only(top: 5),
                            width: 9,
                            height: 9,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: LinearGradient(
                                    colors: [
                                      blueMainColorApp,
                                      blueMainColorApp.withOpacity(0.27)
                                    ],
                                    begin: Alignment.bottomCenter,
                                    end: Alignment.topCenter)),
                          ) : Container(width: 9, height: 9, margin: EdgeInsets.only(top: 5),)
                        ],
                      ),
                    ))),
          ],
        ),
        SizedBox(
          height: 17,
        ),
        controller.isOtherTabBar
            ? OtherView()
            : BasicInfoView()
      ],
    );
  }
}




