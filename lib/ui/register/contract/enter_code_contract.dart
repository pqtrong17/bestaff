import 'package:be_staff/data/response/user_response.dart';

abstract class EnterCodeContract {
  void onEnterSuccess();

  void onLoginSuccess(UserResponse response);

  void onError(Exception exception);

  void onEnterError(Exception exception);
}