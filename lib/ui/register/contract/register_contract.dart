import 'package:be_staff/data/response/register_response.dart';
import 'package:be_staff/data/response/user_response.dart';

abstract class RegisterContract {
  void onRegisterSuccess(RegisterResponse response);

  void onLoginSuccess(UserResponse response);

  void onError(Exception exception);
}