import 'dart:convert';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/login_repositories.dart';
import 'package:be_staff/data/repositories/register_repositories.dart';
import 'package:be_staff/data/request/login_request.dart';
import 'package:be_staff/data/request/register_request.dart';
import 'package:be_staff/data/response/register_response.dart';
import 'package:be_staff/data/response/user_response.dart';
import 'package:be_staff/ui/register/contract/register_contract.dart';
import 'package:be_staff/helper/extension.dart';
import 'package:be_staff/ui/register/view/enter_code_page.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterController extends GetxController implements RegisterContract {
  bool isSeeTutorial = true;
  bool isShowPassword = true;
  bool isFocusTextField = false;

  RegisterRepositories registerRepositories;
  RegisterContract contract;
  RegisterRequest request;

  TextEditingController fullNameController;
  TextEditingController mobileController;
  TextEditingController emailController;
  TextEditingController passwordController;

  bool fullNameInvalid = false;
  bool mobileInvalid = false;
  bool emailInvalid = false;
  bool passwordInvalid = false;

  String fullNameError = "";
  String mobileError = "";
  String emailError = "";
  String passwordError = "";

  LoginRepositories loginRepositories;



  void focusTextField(bool value) {
    isFocusTextField = value;
    update();
  }

  @override
  void onInit() {
    super.onInit();
    registerRepositories = Injector().onPostRegister;
    contract = this;
    fullNameController = TextEditingController();
    mobileController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    String previousPageInput = Get.arguments;
    if(previousPageInput.isEmail){
      emailController.text = previousPageInput;
    }else{
      mobileController.text = previousPageInput;
    }
  }

  void checkUserManual(bool value) {
    isSeeTutorial = value;
    update();
  }

  void showPassword(bool value) {
    isShowPassword = value;
    update();
  }

  void onCheckRegister() {
    checkValidName();
    checkValidPhone();
    checkValidEmail();
    checkValidPassword();
    update();
    if (!fullNameInvalid &&
        !emailInvalid &&
        !passwordInvalid &&
        !mobileInvalid) {
      Get.dialog(LoadingDialog());
      request = RegisterRequest();
      request.email = emailController.text;
      request.fullName = fullNameController.text;
      request.mobile = mobileController.text;
      request.password = passwordController.text;
      onRegister();
    }
  }

  void checkValidName() {
    if (fullNameController.text.isEmpty) {
      fullNameError = full_name_empty;
      fullNameInvalid = true;
    } else {
      fullNameInvalid = false;
    }
  }

  void checkValidPhone(){
    if (mobileController.text.isEmpty) {
      mobileError = phone_is_empty;
      mobileInvalid = true;
    } else {
      if (mobileController.text.checkPhoneNumber()) {
        mobileError = phone_validate;
        mobileInvalid = true;
      } else {
        mobileInvalid = false;
      }
    }
  }

  void checkValidEmail() {
    if (emailController.text.isEmpty) {
      emailError = email_is_empty;
      emailInvalid = true;
    } else {
      if (emailController.text.checkEmail()) {
        emailError = email_validate;
        emailInvalid = true;
      } else {
        emailInvalid = false;
      }
    }
  }

  void checkValidPassword(){
    if (passwordController.text.isEmpty) {
      passwordError = password_is_empty;
      passwordInvalid = true;
    } else {
      if (passwordController.text.length < 6) {
        passwordError = password_validate;
        passwordInvalid = true;
      } else
        passwordInvalid = false;
    }
  }

  void onRegister() {
    registerRepositories
        .onRegister(request)
        .then((value) => contract.onRegisterSuccess(value))
        .catchError((onError) => contract.onError(onError));
  }

  void onLogin() {
    loginRepositories = Injector().onLogin;
    LoginRequest request = LoginRequest();
    request.password = passwordController.text;
    request.email = emailController.text;
    loginRepositories
        .onLogin(request)
        .then((value) => contract.onLoginSuccess(value))
        .catchError((onError) {
      return contract.onError(onError);
    });
  }

  @override
  void onError(Exception exception) {
    if(Get.isDialogOpen){
      Get.back();
    }
    emailInvalid = true;
    emailError = exception.toString();
    update();
  }

  @override
  void onRegisterSuccess(RegisterResponse response) {
    onLogin();
  }

  @override
  Future<void> onLoginSuccess(UserResponse response) async {
    // TODO: implement onLoginSuccess
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("infoUser", jsonEncode(response.toJson()));
    StorageInfoUser().setInfoUser(response);
    final Map _arguments = {
      "email": emailController.text,
      "password": passwordController.text,
      "isTutorial": isSeeTutorial
    };
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.offAll(EnterCodePage(), arguments: _arguments);
  }
}
