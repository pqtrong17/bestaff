import 'package:be_staff/ui/home/view/main_home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TutorialController extends GetxController {
  PageController pageController;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    pageController = PageController();
  }

  void onNextPage() {
    if(pageController.page != 2){
      pageController.nextPage(duration: Duration(seconds: 1), curve: Curves.easeIn);
    }else{
      Get.offAll(MainHomePage());
    }
  }
}