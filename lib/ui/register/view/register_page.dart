import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/login/view/login_page.dart';
import 'package:be_staff/ui/register/controller/register_controller.dart';
import 'package:be_staff/ui/register/view/term_privacy_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/values/styles.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterPage extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(builder: (value) {
      return InitialWidget.expand(
        leadingIconAppBar: GestureDetector(
          child: Icon(
            Icons.clear,
            color: bdGrayColor,
          ),
          onTap: () => Get.back(),
        ),
        backgroundAppBar: Colors.transparent,
        titleAppBar: register,
        styleTitleAppBar: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w600, fontSize: 30),
        isCenterTitleAppBar: true,
        actionsAppBar: [
          InkWell(
            onTap: () => Get.to(LoginPage()),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(right: 16),
                child: TextCustomized(
                  text: login,
                  color: blueMainColorApp,
                  font: fBarlowMedium,
                  weight: FontWeight.w500,
                ),
              ),
            ),
          )
        ],
        body: onRenderBody(),
      );
    });
  }

  Widget onRenderBody() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          TextCustomized(
            text: reminder_register,
            color: ColorHelper.parseColor("#FF0000"),
            size: 14,
            isCenter: true,
          ),
          SizedBox(
            height: 16,
          ),
          onRenderField(),
          SizedBox(
            height: 8,
          ),
          Row(
            children: [
              SizedBox(
                width: 16,
                height: 16,
                child: Checkbox(
                    value: controller.isSeeTutorial,
                    onChanged: (value) {
                      controller.checkUserManual(value);
                    }),
              ),
              SizedBox(
                width: 7,
              ),
              TextCustomized(
                text: see_tutorial_for_use_app,
              )
            ],
          ),
          Expanded(
            child: SizedBox(
              height: 20,
            ),
          ),
          ButtonCustomized(
            toUpperCase: false,
            text: register,
            weight: FontWeight.w600,
            backgroundColor: blueMainColorApp,
            width: Get.width,
            borderRadius: 100,
            padding: EdgeInsets.symmetric(vertical: 16),
            margin: EdgeInsets.symmetric(horizontal: Get.width / 4),
            onPressed: () {
              controller.onCheckRegister();
            },
          ),
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: Get.width / 6, vertical: 7),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: reminder_register_step2_1 + " ",
                style: styleInterRegular,
                children: <TextSpan>[
                  TextSpan(
                      text: term_of_service,
                      style: styleUnderlinedBoldBlack,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Get.to(TermPrivacyPage(
                              "https://bestaff.io/dieu-khoan-su-dung/"));
                        }),
                  TextSpan(text: " " + and + " ", style: styleInterRegular),
                  TextSpan(
                      text: privacy_policy,
                      style: styleUnderlinedBoldBlack,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Get.to(TermPrivacyPage(
                              "https://bestaff.io/chinh-sach-bao-mat/"));
                        }),
                  TextSpan(
                      text: " " + of + " $app_name", style: styleInterRegular),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget onRenderField() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldCustomized(
            onFocused: (value) {
              controller.focusTextField(value);
            },
            validator: () => controller.fullNameInvalid,
            errorText: controller.fullNameError,
            controller: controller.fullNameController,
            hintText: full_name,
            hintStyle: styleHintInput,
            colorBorderEnable: f6GrayColor,
            borderRadius: 8,
            backgroundColor: ColorHelper.parseColor("#F6F6F6"),
          ),
          SizedBox(
            height: 16,
          ),
          TextFieldCustomized(
            onFocused: (value) {
              controller.focusTextField(value);
            },
            validator: () => controller.mobileInvalid,
            errorText: controller.mobileError,
            controller: controller.mobileController,
            hintText: mobile,
            hintStyle: styleHintInput,
            colorBorderEnable: f6GrayColor,
            borderRadius: 8,
            backgroundColor: ColorHelper.parseColor("#F6F6F6"),
          ),
          SizedBox(
            height: 16,
          ),
          TextFieldCustomized(
            onFocused: (value) {
              controller.focusTextField(value);
            },
            validator: () => controller.emailInvalid,
            errorText: controller.emailError,
            controller: controller.emailController,
            hintText: email_text,
            hintStyle: styleHintInput,
            colorBorderEnable: f6GrayColor,
            borderRadius: 8,
            backgroundColor: ColorHelper.parseColor("#F6F6F6"),
          ),
          SizedBox(
            height: 16,
          ),
          TextFieldCustomized(
            onFocused: (value) {
              controller.focusTextField(value);
            },
            validator: () => controller.passwordInvalid,
            isObscured: controller.isShowPassword,
            controller: controller.passwordController,
            errorText: controller.passwordError,
            hintText: password,
            hintStyle: styleHintInput,
            colorBorderEnable: f6GrayColor,
            borderRadius: 8,
            backgroundColor: ColorHelper.parseColor("#F6F6F6"),
            suffixIcon: GestureDetector(
              onTap: () {},
              child: GestureDetector(
                onTap: () {
                  controller.showPassword(!controller.isShowPassword);
                },
                // child: Center(
                //   child: TextCustomized(
                //     text: controller.isShowPassword ? show : hide,
                //     color: gray88Color,
                //     font: fBarlow,
                //     weight: FontWeight.w500,
                //     size: 16,
                //     isCenter: true,
                //   ),
                // ),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: ImageCustomized(
                    path: !controller.isShowPassword ? ic_eyes_close :ic_eyes_open,
                    width: 40,
                    height: 40,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 6,
          ),
          TextCustomized(
            text: reminder_register_step2,
            color: ColorHelper.parseColor("#8C8C8C"),
          ),
        ],
      );
}
