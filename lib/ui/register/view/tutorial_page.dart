import 'package:be_staff/ui/register/controller/tutorial_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class TutorialPage extends GetView<TutorialController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<TutorialController>(
      builder: (value) => InitialWidget(
        body: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Center(
                  child: TextCustomized(
                    text: tutorial_use_app,
                    weight: FontWeight.bold,
                    size: mediumSize,
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: PageView(
                  controller: controller.pageController,
                  children: [
                    _View(
                        true,
                        declare_busy_calendar,
                        bg_declare_busy_calendar,
                        description_busy_calendar),
                    _View(
                        false,
                        time_keeping,
                        bg_time_keeping,
                        description_time_keeping),
                    _View(
                        false,
                        shift_management_text,
                        bg_shift_management,
                        description_shift_management),
                  ],
                ),
              ),
              SizedBox(height: 24,),
              SmoothPageIndicator(
                controller: controller.pageController,
                count: 3,
                effect: WormEffect(
                    dotWidth: 10,
                    dotHeight: 10,
                    activeDotColor: blueMainColorApp),
              ),
              SizedBox(height: 24,),
              ButtonCustomized(
                text: next_text,
                backgroundColor: blueMainColorApp,
                borderRadius: 100,
                width: Get.width,
                toUpperCase: false,
                margin: EdgeInsets.symmetric(horizontal: Get.width/4),
                onPressed: () => controller.onNextPage(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _View extends GetView {
  final bool isFirst;
  final String title;
  final String image;
  final String description;

  _View(this.isFirst, this.title, this.image, this.description);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 2,
          child: ImageCustomized(
            path: image,
            width: Get.width,
            fit: BoxFit.cover,
            radius: 8,
          ),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextCustomized(
                text: title,
                weight: FontWeight.bold,
              ),
              TextCustomized(
                text: description,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
