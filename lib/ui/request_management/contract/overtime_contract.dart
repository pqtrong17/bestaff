import 'package:be_staff/data/response/information_overtime_response.dart';

abstract class OvertimeContract {
  void onCreateOvertime(String message);

  void onGetInfoSuccess(InformationOvertimeResponse response);

  void onError(Exception exception);
}
