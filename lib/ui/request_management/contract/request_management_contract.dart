import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/switch_shift_pending_response.dart';

abstract class RequestManagementContract {
  void onGetSwitchPendingShiftSuccess(List<SwitchShiftPending> response);

  void onAcceptSuccess(String message);

  void onRejectSuccess(String message);

  void onGetShiftNotificationSuccess(ShiftNotificationResponse response);

  void onError(String error);
}
