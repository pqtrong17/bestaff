import 'package:be_staff/data/response/information_overtime_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/request_management/controller/overtime_controller.dart';
import 'package:be_staff/ui/request_management/controller/take_leave_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class OverTimePage extends GetView<OvertimeController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<OvertimeController>(
      builder: (value) => InitialWidget(
        titleAppBar: choose_time_of_over_time,
        backgroundAppBar: blueMainColorApp,
        body: ListView(
          shrinkWrap: true,
          children: [
            SfDateRangePicker(
              selectionMode: DateRangePickerSelectionMode.range,
              startRangeSelectionColor: Colors.black,
              endRangeSelectionColor: Colors.black,
              rangeSelectionColor: ColorHelper.parseColor("#DCDCDC"),
              navigationDirection: DateRangePickerNavigationDirection.vertical,
              monthViewSettings: DateRangePickerMonthViewSettings(
                enableSwipeSelection: false,
              ),
              onSelectionChanged: (args) => controller.onUpdateDate(args),
            ),
            _buildInputInfo()
          ],
        ),
      ),
    );
  }

  Widget _buildInputInfo() => Padding(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () => DatePicker.showTimePicker(Get.context,
                        locale: LocaleType.vi,
                        showSecondsColumn: false,
                        onConfirm: (date){
                          controller.onUpdateTimeIn(date);
                        } ),
                    child: Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: controller.hasDataTimeIn ? ColorHelper.parseColor("#E8E8E8") : Colors.red,
                              width: 1),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        children: [
                          Expanded(
                              child: TextCustomized(
                            text: controller.timeIn ?? time_in,
                            color: ColorHelper.parseColor("#4B4B4B"),
                            size: mediumSize,
                          )),
                          Icon(Icons.keyboard_arrow_down)
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: InkWell(
                    onTap: () => DatePicker.showTimePicker(Get.context,
                      locale: LocaleType.vi,
                      showSecondsColumn: false,
                        onConfirm: (date){
                          controller.onUpdateTimeOut(date);
                        } ),
                    child: Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: controller.hasDataTimeOut ? ColorHelper.parseColor("#E8E8E8") : Colors.red,
                              width: 1),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        children: [
                          Expanded(
                              child: TextCustomized(
                            text: controller.timeout ?? time_out,
                            color: ColorHelper.parseColor("#4B4B4B"),
                            size: mediumSize,
                          )),
                          Icon(Icons.keyboard_arrow_down)
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      final result = await Get.bottomSheet(_BottomSheet(controller.mPosition));
                      controller.onUpdatePosition(result);
                    },
                    child: Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: controller.hasDataPosition ? ColorHelper.parseColor("#E8E8E8") : Colors.red,
                              width: 1),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        children: [
                          Expanded(
                              child: TextCustomized(
                            text: controller.dataPosition?.name ?? position_text,
                            color: ColorHelper.parseColor("#4B4B4B"),
                            size: mediumSize,
                                maxLine: 1,
                                textOverflow: TextOverflow.ellipsis,
                          )),
                          Icon(Icons.keyboard_arrow_down)
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      final result = await Get.bottomSheet(_BottomSheet(controller.mType));
                      controller.onUpdateTypeOvertime(result);
                    },
                    child: Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: controller.hasDataOvertime ? ColorHelper.parseColor("#E8E8E8") : Colors.red,
                              width: 1),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        children: [
                          Expanded(
                              child: TextCustomized(
                            text: controller.typeOvertime?.name ?? type_of_over_time,
                            color: ColorHelper.parseColor("#4B4B4B"),
                            size: mediumSize,
                                maxLine: 1,
                                textOverflow: TextOverflow.ellipsis,
                          )),
                          Icon(Icons.keyboard_arrow_down)
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: controller.hasDataReason ? ColorHelper.parseColor("#E8E8E8") : Colors.red, width: 1),
                  borderRadius: BorderRadius.circular(8)),
              child: TextFieldCustomized(
                controller: controller.reasonController,
                maxLine: 4,
                hintText: enter_content,
                hintStyle: TextStyle(
                    color: ColorHelper.parseColor("#4B4B4B"),
                    fontSize: mediumSize),
                contentPadding: EdgeInsets.all(0),
                backgroundColor: Colors.transparent,
                colorBorderEnable: Colors.transparent,
                colorBorderFocus: Colors.transparent,
                onChanged: (value) {
                  controller.onChangeReasonController(value);
                },
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextCustomized(
              text: "${controller.countLength}/256 từ",
              size: smallSize,
              color: ColorHelper.parseColor("#848484"),
            ),
            SizedBox(
              height: 30,
            ),
            ButtonCustomized(
              text: send_request,
              colorText: blueMainColorApp,
              backgroundColor: ColorHelper.parseColor("#C3E6FF"),
              width: Get.width,
              borderRadius: 100,
              onPressed: () => controller.onCreateOvertimeRequest(),
            )
          ],
        ),
      );

  Widget _buildBottomSheet() => Container(
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(16),
              topLeft: Radius.circular(16),
            )),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: TextCustomized(
                    text: choose_time_of_over_time,
                    size: largeSize,
                    weight: FontWeight.w600,
                  ),
                ),
                Positioned(
                  right: 0,
                  top: 0,
                  child: InkWell(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.close,
                      size: 20,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 40,
            ),
            ButtonCustomized(
              text: done_text,
              colorText: blueMainColorApp,
              backgroundColor: ColorHelper.parseColor("#C3E6FF"),
              width: Get.width,
              borderRadius: 100,
              margin: EdgeInsets.symmetric(horizontal: Get.width / 4),
            )
          ],
        ),
      );
}

class _BottomSheet extends GetView {
  final dynamic data;

  _BottomSheet(this.data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(16),
            topLeft: Radius.circular(16),
          )),
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: TextCustomized(
                  text: choose_position,
                  size: largeSize,
                  weight: FontWeight.w600,
                ),
              ),
              Positioned(
                right: 0,
                top: 0,
                child: InkWell(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.close,
                    size: 20,
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 14,
          ),
          Divider(
            color: ColorHelper.parseColor("#F0F0F0"),
            height: 1,
          ),
          SizedBox(
            height: 20,
          ),
          ListView.builder(
            shrinkWrap: true,
            itemBuilder: (context, position) {
              return InkWell(
                onTap: () {
                  Get.back(result: data[position]);
                },
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: TextCustomized(
                      text: data[position].name,
                      weight: FontWeight.w600,
                      size: mediumSize,
                    ),
                  ),
                ),
              );
            },
            itemCount: data.length,
          )
        ],
      ),
    );
  }

}
