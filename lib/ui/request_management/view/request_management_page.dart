import 'dart:math';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/response/shift_notification_response.dart';
import 'package:be_staff/data/response/switch_shift_pending_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/request_management/controller/request_management_controller.dart';
import 'package:be_staff/ui/request_management/view/overtime_page.dart';
import 'package:be_staff/ui/request_management/view/take_leave_page.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RequestManagementPage extends GetView<RequestManagementController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<RequestManagementController>(
      builder: (value) => InitialWidget(
        titleAppBar: request_management,
          backgroundAppBar: blueMainColorApp,
          floatingActionButton: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _buildBubble(),
              SizedBox(height: 10,),
              _buildFloatingActionButton(),
            ],
            //Hidden bottom App bar. Waiting for next update version.
          ),
          // bottomAppbar: PreferredSize(
          //   preferredSize: Size(
          //     Get.width,
          //     50
          //   ),
          //   child: Padding(
          //     padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          //     child: Row(
          //       children: [
          //         Expanded(child: Container(
          //           padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
          //           decoration: BoxDecoration(
          //             color: ColorHelper.parseColor("#4FC0FF"),
          //             borderRadius: BorderRadius.circular(100)
          //           ),
          //           child: Row(
          //             children: [
          //               ImageCustomized(path: ic_calendar_week, width: 12, height: 14,),
          //               SizedBox(width: 10,),
          //               TextCustomized(
          //                 text: "Mọi lúc",
          //                 color: ColorHelper.parseColor("#DCF2FF"),
          //                 size: mediumSize,
          //               )
          //             ],
          //           ),
          //         )),
          //         SizedBox(width: 40,),
          //         Expanded(child: Container(
          //           padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
          //           decoration: BoxDecoration(
          //               color: ColorHelper.parseColor("#4FC0FF"),
          //               borderRadius: BorderRadius.circular(100)
          //           ),
          //           child: Row(
          //             children: [
          //               Expanded(
          //                 child: TextCustomized(
          //                   text: "Tìm kiếm",
          //                   color: ColorHelper.parseColor("#DCF2FF"),
          //                   size: mediumSize,
          //                 ),
          //               ),
          //               SizedBox(width: 10,),
          //               ImageCustomized(path: ic_calendar_week, width: 12, height: 14,),
          //             ],
          //           ),
          //         )),
          //       ],
          //     ),
          //   ),
          // ),
          body: DefaultTabController(
        length: 4,
        child: Column(
          children: [
            //Hidden to waiting for next update version
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 16),
            //   child: TabBar(controller: controller.tabController, tabs: [
            //     _Tab(controller.indexTab == 0, all_text),
            //     _Tab(controller.indexTab == 1, pending_text),
            //     _Tab(controller.indexTab == 2, canceled_text),
            //     _Tab(controller.indexTab == 3, approved_text),
            //   ]),
            // ),
            // Expanded(
            //     child: TabBarView(
            //   controller: controller.tabController,
            //   children: [
            //     _ViewPage(controller.mSwitchShiftPending),
            //     _ViewPage(controller.mSwitchShiftPending),
            //     _ViewPage(controller.mSwitchShiftPending),
            //     _ViewPage(controller.mSwitchShiftPending),
            //   ],
            // ))
            Expanded(child: _ViewShiftNotification())
          ],
        ),
      )),
    );
  }

  Widget _buildFloatingActionButton() => FloatingActionButton(
    onPressed: () {
      controller.onFabTranslation();
    },
    child: AnimatedBuilder(
      builder: (context, builder){
        return Transform.rotate(
            angle: pi * controller.fabAnimationController.value,
            alignment: FractionalOffset.center,
            child: controller.isFabOpen ? Icon(Icons.close) : Icon(Icons.add));
      },
      animation: controller.fabAnimationController,
    ),
    isExtended: true,
  );

  Widget _buildBubble() => controller.isFabOpen ? Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      CustomPaint(
        painter: _BubblePaint(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  controller.onFabTranslation();
                  Get.to(TakeLeavePage());
                },
                child: TextCustomized(
                  text: take_leave,
                  color: ColorHelper.parseColor("#6E7191"),
                  weight: FontWeight.w500,
                  size: largeSize,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  controller.onFabTranslation();
                  Get.to(OverTimePage());
                },
                child: TextCustomized(
                  text: over_time,
                  color: ColorHelper.parseColor("#6E7191"),
                  size: largeSize,
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  ) : Container();
}

class _Tab extends GetView {
  final bool isSelect;
  final String title;

  _Tab(this.isSelect, this.title);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        TextCustomized(
          text: title,
          color: isSelect ? blueMainColorApp : ColorHelper.parseColor("#C6E4FF"),
          size: mediumSize,
          weight: FontWeight.w600,
        ),
        SizedBox(height: 4,),
        isSelect ? Container(
          margin: EdgeInsets.only(top: 5),
          width: 9,
          height: 9,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                  colors: [
                    blueMainColorApp,
                    blueMainColorApp.withOpacity(0.27)
                  ],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter)),
        ) : Container()
      ],
    );
  }

}

class _ViewPage extends GetView {
  final List<SwitchShiftPending> data;

  _ViewPage(this.data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return data != null ? data.isNotEmpty ? ListView.builder(
      itemCount: data.length,
        itemBuilder: (context, position) => InkWell(
          // onTap: () => Get.dialog(MessageDialog(
          //   title: data[position].fullName,
          //   description: reject_or_accept_request,
          //   textOK: accept_text,
          //   textCancel: reject_text,
          //   isShowCancel: true,
          //   onPressedOK: () => controller.onAccept(position),
          //   onPressedCancel: () => controller.onReject(position),
          // )),
          onTap: () => Get.bottomSheet(_BottomSheet()),
          child: Container(
            color: position % 2 == 0 ? ColorHelper.parseColor("#F0FAFF") : Colors.white,
            padding: EdgeInsets.symmetric(vertical: 7, horizontal: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        //Avatar
                        Container(
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: LinearGradient(colors: [
                                  ColorHelper.parseColor("#7433FF"),
                                  ColorHelper.parseColor("#FFA3FD"),
                                ])),
                            child: CircleImage.network(
                              path:
                                  "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png",
                              insideRadius: 20,
                            )),
                        SizedBox(width: 7,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  TextCustomized(
                                    text: data[position].fullName,
                                    size: mediumSize,
                                    weight: FontWeight.w600,
                                  ),
                                  SizedBox(width: 12,),
                                  Container(
                                    transform: Matrix4.translationValues(0, -10 , 0),
                                    child: ImageCustomized(
                                      path: ic_message_request_management,
                                      width: 12,
                                      height: 10,
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  TextCustomized(
                                    text: want_to_change_shift,
                                    color: ColorHelper.parseColor("#515151"),
                                  ),
                                  SizedBox(width: 4,),
                                  Row(
                                    children: [
                                      ImageCustomized(path: ic_place, width: 6, height: 7,),
                                      SizedBox(width: 8,),
                                      TextCustomized(
                                        text: "Leevins Cafe",
                                        color: ColorHelper.parseColor("#B0B0B0"),
                                      ),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        ButtonCustomized(
                          text: new_text,
                          colorText: blueMainColorApp,
                          weight: FontWeight.w500,
                          size: mediumSize,
                          backgroundColor: Colors.white,
                          borderRadius: 100,
                          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 40),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              ImageCustomized(
                                path: ic_dashed_above,
                                width: 24,
                                height: 4,
                              ),
                              ImageCustomized(
                                path: ic_clock_request_management,
                                width: 8,
                                height: 8,
                              ),
                              SizedBox(width: 4,),
                              TextCustomized(
                                text: "T4. 19.2 | 17:30 - 22:30",
                                color: ColorHelper.parseColor("#515151"),
                              ),
                              SizedBox(width: 4,),
                              Container(
                                width: 6,
                                height: 6,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(width: 4,),
                              TextCustomized(
                                text: "Phục vụ",
                                color: ColorHelper.parseColor("#515151"),
                              ),
                              SizedBox(width: 4,),
                              TextCustomized(
                                text: "(Phan)",
                                color: ColorHelper.parseColor("#B0B0B0"),
                              )
                            ],
                          ),
                          Container(
                            transform: Matrix4.translationValues( -(11/2), 0, 0),
                              child: ImageCustomized(path: ic_change, width: 11, height: 11,)),
                          Row(
                            children: [
                              ImageCustomized(
                                path: ic_dashed_below,
                                width: 24,
                                height: 4,
                              ),
                              ImageCustomized(
                                path: ic_clock_request_management,
                                width: 8,
                                height: 8,
                              ),
                              SizedBox(width: 4,),
                              TextCustomized(
                                text: "T4. 19.2 | 17:30 - 22:30",
                                color: ColorHelper.parseColor("#515151"),
                              ),
                              SizedBox(width: 4,),
                              Container(
                                width: 6,
                                height: 6,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(width: 4,),
                              TextCustomized(
                                text: "Phục vụ",
                                color: ColorHelper.parseColor("#515151"),
                              ),
                              SizedBox(width: 4,),
                              TextCustomized(
                                text: "(Bạn)",
                                color: ColorHelper.parseColor("#B0B0B0"),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 6,),
                    Padding(
                      padding: const EdgeInsets.only(left: 46),
                      child: TextCustomized(
                        text: "Vừa xong",
                        color: blueMainColorApp,
                        size: smallSize,
                      ),
                    )
                  ],
                ),
          ),
        )) : Center(child: TextCustomized(text: no_request,),): Center(child: CircularProgressIndicator(),);
  }

}

class _ViewShiftNotification extends GetView<RequestManagementController> {

  @override
  Widget build(BuildContext context) {
    final data = controller.mAll ?? null;
    // TODO: implement build
    return RefreshIndicator(
      onRefresh: () => controller.onRefresh(),
      child: data != null ? ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, position) => InkWell(
            onTap: () => Get.bottomSheet(_BottomSheet()),
            child: Container(
              color: position % 2 == 0 ? ColorHelper.parseColor("#F0FAFF") : Colors.white,
              padding: EdgeInsets.symmetric(vertical: 7, horizontal: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      //Avatar
                      Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(colors: [
                                ColorHelper.parseColor("#7433FF"),
                                ColorHelper.parseColor("#FFA3FD"),
                              ])),
                          child: CircleImage.network(
                            path:
                            "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png",
                            insideRadius: 20,
                          )),
                      SizedBox(width: 7,),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextCustomized(
                              text: data[position].fullName,
                              size: mediumSize,
                              weight: FontWeight.w600,
                            ),
                            Row(
                              children: [
                                TextCustomized(
                                  text: "Muốn nhượng ca",
                                  color: ColorHelper.parseColor("#515151"),
                                ),
                                SizedBox(width: 4,),
                                Row(
                                  children: [
                                    ImageCustomized(path: ic_place, width: 6, height: 7,),
                                    SizedBox(width: 8,),
                                    TextCustomized(
                                      text: StorageInfoUser().getInfoUser.branchName[0].name,
                                      color: ColorHelper.parseColor("#B0B0B0"),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Row(
                              children: [
                                // ImageCustomized(
                                //   path: ic_clock_request_management,
                                //   width: 8,
                                //   height: 8,
                                // ),
                                // SizedBox(width: 4,),
                                Expanded(
                                  child: TextCustomized(
                                    text: data[position].content,
                                    color: ColorHelper.parseColor("#515151"),
                                  ),
                                ),
                                // SizedBox(width: 4,),
                                // Container(
                                //   width: 6,
                                //   height: 6,
                                //   decoration: BoxDecoration(
                                //     shape: BoxShape.circle,
                                //     color: Colors.black,
                                //   ),
                                // ),
                                // SizedBox(width: 4,),
                                // TextCustomized(
                                //   text: "Phục vụ",
                                //   color: ColorHelper.parseColor("#515151"),
                                // ),
                              ],
                            ),
                            TextCustomized(
                              text: "Vừa xong",
                              color: blueMainColorApp,
                              size: smallSize,
                            )
                          ],
                        ),
                      ),
                      ButtonCustomized(
                        text: new_text,
                        colorText: blueMainColorApp,
                        weight: FontWeight.w500,
                        size: mediumSize,
                        backgroundColor: Colors.white,
                        borderRadius: 100,
                        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 40),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )) : ListView(
        children: [
          Center(
            child: TextCustomized(
              text: no_data_available,
            ),

          ),
        ],
      ),
    );
  }

}

class _BubblePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    double height = size.height;
    double width = size.width;
    final Path path = Path();
    final double angle = 2;
    final double startAngle = 10;
    final double endAngle = 10;
    path
    ..moveTo(startAngle, 0)
    ..lineTo(width - startAngle, 0)
    ..quadraticBezierTo(width - angle, angle, width, endAngle)
      ..lineTo(width, height - startAngle)
      ..quadraticBezierTo(width-angle, height-angle, width-endAngle, height)
      ..lineTo(width-endAngle * 2, height)
      ..lineTo(width - endAngle * 2.5, height + 10)
      ..lineTo(width-endAngle * 3, height)
      ..lineTo(startAngle, height)
      ..quadraticBezierTo(angle, height - angle, 0, height-endAngle)
      ..lineTo(0, startAngle)
      ..quadraticBezierTo(angle, angle, endAngle, 0)
    ..close();
    final paint = Paint()
      ..color = ColorHelper.parseColor("#EFF0F6")
      ..style = PaintingStyle.fill
      ..strokeWidth = 0;
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }

}

class _BottomSheet extends GetView {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: const EdgeInsets.all(24.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(16),
          topLeft: Radius.circular(16),
        )
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(colors: [
                        ColorHelper.parseColor("#7433FF"),
                        ColorHelper.parseColor("#FFA3FD"),
                      ])),
                  child: CircleImage(
                    path: ic_avatar_example,
                    insideRadius: 12,
                  )
              ),
              SizedBox(width: 16,),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: ColorHelper.parseColor("#FCFCFC"),
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                          color: ColorHelper.parseColor("#E8E8E8"),
                          width: 1
                      )
                  ),
                  child: TextCustomized(
                    text: "Hải có tiệc chia tay bạn thân vào Thứ 4 nên hoán đổi ca này giùp Hải với hì",
                    color: ColorHelper.parseColor("#5B5B5B"),
                  ),
                ),
              ),
              SizedBox(width: 44,),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(width: 44,),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: ColorHelper.parseColor("#FCFCFC"),
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                          color: ColorHelper.parseColor("#E8E8E8"),
                          width: 1
                      )
                  ),
                  child: Column(
                    children: [
                      TextFieldCustomized(
                          controller: Get.find<RequestManagementController>()
                              .bottomSheetController,
                      hintText: leave_message,
                      hintStyle: TextStyle(
                        color: ColorHelper.parseColor("#BDBDBD")
                      ),
                        contentPadding: EdgeInsets.all(5),
                        colorBorderEnable: Colors.transparent,
                        colorBorderFocus: Colors.transparent,
                        backgroundColor: Colors.transparent,
                      ),
                      SizedBox(height: 4,),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.end,
                      //   children: [
                      //     ImageCustomized(path: ic_voice, width: 17, height: 17,),
                      //     SizedBox(width: 4,),
                      //     TextCustomized(
                      //       text: press_to_recording,
                      //       color: ColorHelper.parseColor("#9D9D9D"),
                      //     )
                      //   ],
                      // ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 16,),
              Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(colors: [
                        ColorHelper.parseColor("#7433FF"),
                        ColorHelper.parseColor("#FFA3FD"),
                      ])),
                  child: CircleImage(
                    path: ic_avatar_example,
                    insideRadius: 12,
                  )
              )
            ],
          ),
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 44),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: blueMainColorApp,
                            width: 1
                        ),
                        borderRadius: BorderRadius.circular(100),
                        color: Colors.white
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ImageCustomized(path: ic_tick_blue, width: 12, height: 12,),
                        SizedBox(width: 4,),
                        TextCustomized(
                          text: accept_text,
                          color: blueMainColorApp,
                          size: mediumSize,
                          weight: FontWeight.w600,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 16,),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.red,
                            width: 1
                        ),
                        borderRadius: BorderRadius.circular(100),
                        color: Colors.white
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ImageCustomized(path: ic_cancel_red, width: 12, height: 12,),
                        SizedBox(width: 4,),
                        TextCustomized(
                          text: reject_text,
                          color: Colors.red,
                          size: mediumSize,
                          weight: FontWeight.w600,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}