import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/request_management/controller/take_leave_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:be_staff/widgets/original/text_field_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class TakeLeavePage extends GetView<TakeLeaveController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<TakeLeaveController>(
      builder: (value) => InitialWidget(
        titleAppBar: choose_day_take_leave,
        backgroundAppBar: blueMainColorApp,
        onWillPop: () {
          if (controller.hasChange) {
            Get.dialog(_buildConfirmExitDialog());
            return Future.value(false);
          }
          return Future.value(true);
        },
        body: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  SfDateRangePicker(
                    selectionMode: DateRangePickerSelectionMode.range,
                    startRangeSelectionColor: Colors.black,
                    endRangeSelectionColor: Colors.black,
                    rangeSelectionColor: ColorHelper.parseColor("#DCDCDC"),
                    navigationDirection:
                        DateRangePickerNavigationDirection.vertical,
                    monthViewSettings: DateRangePickerMonthViewSettings(
                      enableSwipeSelection: false,
                    ),
                    onSelectionChanged: (DateRangePickerSelectionChangedArgs callback){
                      controller.onHandlingDate(callback);
                    },
                  ),
                  _buildInputInfo(),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            ButtonCustomized(
              text: send_request,
              colorText: blueMainColorApp,
              backgroundColor: ColorHelper.parseColor("#C3E6FF"),
              width: Get.width,
              borderRadius: 100,
              margin: EdgeInsets.symmetric(horizontal: 20),
              onPressed: () {
                controller.onTakeLeaveRequest();
              },
            ),
            SizedBox(
              height: 16,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildInputInfo() => Padding(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
        child: Column(
          children: [
            InkWell(
              onTap: () => Get.bottomSheet(_buildBottomSheet()),
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: ColorHelper.parseColor("#E8E8E8"), width: 1),
                    borderRadius: BorderRadius.circular(8)),
                child: Row(
                  children: [
                    Expanded(
                        child: TextCustomized(
                      text: controller.type ?? choose_type_of_take_leave,
                      color: ColorHelper.parseColor("#4B4B4B"),
                      size: mediumSize,
                    )),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: ColorHelper.parseColor("#E8E8E8"), width: 1),
                  borderRadius: BorderRadius.circular(8)),
              child: TextFieldCustomized(
                controller: controller.reasonController,
                maxLine: 4,
                hintText: enter_content,
                hintStyle: TextStyle(
                    color: ColorHelper.parseColor("#4B4B4B"),
                    fontSize: mediumSize),
                contentPadding: EdgeInsets.all(0),
                backgroundColor: Colors.transparent,
                colorBorderEnable: Colors.transparent,
                colorBorderFocus: Colors.transparent,
                onChanged: (value) {
                  controller.onChangeReasonController(value);
                },
                validator: () => controller.hasReasonError,
                errorText: reason_is_required,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextCustomized(
              text: "${controller.countLength}/256 từ",
              size: smallSize,
              color: ColorHelper.parseColor("#848484"),
            ),
          ],
        ),
      );

  Widget _buildBottomSheet() => Container(
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(16),
              topLeft: Radius.circular(16),
            )),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: TextCustomized(
                    text: choose_type_of_take_leave,
                    size: largeSize,
                    weight: FontWeight.w600,
                  ),
                ),
                Positioned(
                  right: 0,
                  top: 0,
                  child: InkWell(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.close,
                      size: 20,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 14,
            ),
            Divider(
              color: ColorHelper.parseColor("#F0F0F0"),
              height: 1,
            ),
            SizedBox(
              height: 20,
            ),
            controller.typeOfLeaveResponse != null ? ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context, position) {
                final _type = controller.typeOfLeaveResponse.result;
                return InkWell(
                  onTap: () => controller.onUpdateType(position),
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: TextCustomized(
                        text: _type[position].name,
                        weight: FontWeight.w600,
                        size: mediumSize,
                      ),
                    ),
                  ),
                );
              },
              itemCount: controller.typeOfLeaveResponse.result.length,
            ) : Container()
          ],
        ),
      );

  Widget _buildConfirmExitDialog() => MessageDialog(
        title: exit_text,
        description: do_you_want_exit,
        textOK: continue_text,
        onPressedOK: () {
          Get.back();
        },
        isShowCancel: true,
        textCancel: exit_text,
        onPressedCancel: () {
          Get.back();
          Get.back();
        },
      );
}
