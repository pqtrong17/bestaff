import 'package:be_staff/data/response/cede_shift_response.dart';

abstract class DetailShiftContract {
  void onGetUserSuccess(CedeShiftResponse response);

  void onCedeShift(String message);

  void onError(Exception exception);
}
