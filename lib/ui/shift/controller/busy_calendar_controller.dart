import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/busy_calendar_repositories.dart';
import 'package:be_staff/data/request/update_busy_calendar_request.dart';
import 'package:be_staff/data/response/busy_time_response.dart';
import 'package:be_staff/ui/shift/contract/busy_time_contract.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

class BusyController extends GetxController implements BusyTimeContract {
  final CalendarController calendarController = CalendarController();
  BusyCalendarRepositories repositories;
  BusyTimeContract contract;
  BusyTimeResponse mBusyTime;
  List<UpdateBusyCalendarRequest> updateRequests;
  String monthYearHeader = "";

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repositories = Injector().onGetBusyTime;
    contract = this;
    onGetBusyTime();
  }

  void monthYearFocusDate(){
    String month = calendarController.focusedDay.month.toString();
    String year = calendarController.focusedDay.year.toString();
    monthYearHeader = "Tháng $month, $year";
    update();
  }

  void onNextPage(){
    calendarController.nextPage();
    calendarController.setFocusedDay(calendarController.focusedDay);
    monthYearFocusDate();
    update();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    onNextPage();
  }

  void onSelectBusyCalendar(List<BusyTime> data, String date) {
    updateRequests = List();
    onUpdateBusy();
  }

  void onForToCreateListRequest(BusyCalendar data){
    for(int i = 0; i < data.busyTimes.length; i++){
      if(data.busyTimes[i].busy == 1){
        print('For ${data.date} : ${data.busyTimes[i].startTime}');
        if (data.busyTimes[i].busy != null) {
          updateRequests.add(UpdateBusyCalendarRequest(
              start: data.date + " " + data.busyTimes[i].startTime,
              end: data.date + " " + data.busyTimes[i].endTime,
              note: ""
          ));
        }
      }
    }
  }

  void onBuildUpdateRequestList(BusyTime busyTime, String date) {
    // if (updateRequests == null) {
    //   updateRequests = List();
    // }
    updateRequests = List();
    if (busyTime != null && date != null) {
      if (busyTime.busy != null) {
        updateRequests.add(UpdateBusyCalendarRequest(
          start: date + " " + busyTime.startTime,
          end: date + " " + busyTime.endTime,
          note: ""
        ));
      }
    }
  }

  void onUpdateBusy(){
    onForToCreateListRequest(mBusyTime.t2);
    onForToCreateListRequest(mBusyTime.t3);
    onForToCreateListRequest(mBusyTime.t4);
    onForToCreateListRequest(mBusyTime.t5);
    onForToCreateListRequest(mBusyTime.t6);
    onForToCreateListRequest(mBusyTime.t7);
    onForToCreateListRequest(mBusyTime .cn);
    repositories.onUpdateBusyCalendar(updateRequests).then((value) {
      return contract.onUpdateSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  void onGetBusyTime() {
    repositories.onGetBusyTime().then((value) {
      return contract.onGetBusyTimeSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }



  @override
  void onError(Exception exception) {
    // TODO: implement onError
  }

  @override
  void onGetBusyTimeSuccess(BusyTimeResponse response) {
    // TODO: implement onGetBusyTimeSuccess
    mBusyTime = response;
    update();
    if(Get.isDialogOpen){
      Get.back();
    }
  }

  @override
  void onUpdateSuccess(String message) {
    // TODO: implement onUpdateSuccess
    Get.snackbar(null, message, snackPosition: SnackPosition.TOP, maxWidth: Get.width/2, messageText: TextCustomized(
      text: message,
      isCenter: true,
    ));
    onGetBusyTime();
  }

}
