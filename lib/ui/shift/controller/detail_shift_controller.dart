import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/cede_shift_repositories.dart';
import 'package:be_staff/data/response/cede_shift_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_home.dart';
import 'package:be_staff/ui/shift/contract/detail_shift_contract.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:get/get.dart';

class DetailShiftController extends GetxController implements DetailShiftContract{
  CedeShiftRepositories repositories;
  DetailShiftContract contract;
  ShiftUserHome detailShift;
  List<DataUser> userCedeShifts;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repositories = Injector().onGetCedeShift;
    contract = this;
    detailShift = Get.arguments;
    onGetUser();
  }

  void onGetUser(){
    final int branchId = StorageInfoUser().getInfoUser.branchName[0].id;
    final int shiftDetailId = detailShift.dataItem.dataUser[0].shiftDetailId;
    repositories.onGetCedeShift(branchId, detailShift.day, shiftDetailId).then((value) {
      return contract.onGetUserSuccess(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  void onChooseCedeShift(int position){
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.dialog(LoadingDialog());
    int shiftId;
    detailShift.dataItem.dataUser.forEach((element) {
      if(element.userId == StorageInfoUser().getInfoUser.userId){
        shiftId = element.id;
      }
    });
    final int switchUser = userCedeShifts[position].id;
    repositories.onCedeShift(shiftId, switchUser).then((value) {
      return contract.onCedeShift(value);
    }).catchError((onError){
      return contract.onError(onError);
    });
  }

  @override
  void onCedeShift(String message) {
    // TODO: implement onCedeShift
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, message);
  }

  @override
  void onError(Exception exception) {
    // TODO: implement onError
    if(Get.isDialogOpen){
      Get.back();
    }
    Get.snackbar(null, no_data_available);
  }

  @override
  void onGetUserSuccess(CedeShiftResponse response) {
    // TODO: implement onGetUserSuccess
    userCedeShifts = List();
    userCedeShifts.addAll(response.dataUser);
    update();
    if(Get.isDialogOpen){
      Get.back();
    }
  }
}