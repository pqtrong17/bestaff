import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/receive_cancel_lack_shift_repository.dart';
import 'package:be_staff/ui/shift/contract/lack_people_contract.dart';
import 'package:be_staff/ui/shift/controller/shift_controller.dart';
import 'package:be_staff/widgets/original/loading_dialog.dart';
import 'package:get/get.dart';

class LackPeopleController extends GetxController
    implements LackPeopleContract {
  ReceiveCancelLackShiftRepository repository;
  LackPeopleContract contract;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    repository = Injector().onGetReceiveCancelLackShift;
    contract = this;
  }

  void onReceive(int shiftDetailId, int positionId) {
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.dialog(LoadingDialog());
    repository.onReceiveLack(shiftDetailId, positionId).then((value) {
      return contract.onReceiveSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  void onCancelShift(int shiftDetailUserId) {
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.dialog(LoadingDialog());
    repository.onCancelLack(shiftDetailUserId).then((value) {
      return contract.onCancelSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  void onGetCanReceiveShift(){
    ShiftController controller = Get.find();
    controller.onGetMissingPersonCase();
  }

  @override
  void onCancelSuccess(String message) {
    // TODO: implement onCancelSuccess
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.snackbar(null, message);
    onGetCanReceiveShift();
  }

  @override
  void onError(Exception error) {
    // TODO: implement onError
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.snackbar(null, error.toString());
  }

  @override
  void onReceiveSuccess(String message) {
    // TODO: implement onReceiveSuccess
    if (Get.isDialogOpen) {
      Get.back();
    }
    Get.snackbar(null, message);
    onGetCanReceiveShift();
  }
}
