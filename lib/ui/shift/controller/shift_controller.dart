import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/calendar_repositories.dart';
import 'package:be_staff/data/repositories/home_repositories.dart';
import 'package:be_staff/data/response/calendar_response.dart';
import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/data/response/other_shift_response.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';
import 'package:be_staff/ui/shift/contract/calendar_contract.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

class ShiftController extends GetxController
    implements CalendarContract {
  CalendarResponse mCalendar;
  LackShiftUserResponse mLackShiftUser;
  ShiftUserResponse mShiftUser;
  List<OtherShiftResponse> mOtherShift;
  CalendarRepositories repositories;
  HomeRepositories homeRepositories;
  CalendarContract contract;
  final CalendarController calendarController = CalendarController();
  bool isSeeAll = false;
  bool isLoadingOtherShift = false;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    contract = this;
    repositories = Injector().onGetWorkingCalendar;
    homeRepositories = Injector().onGetHome;
    // onGetWorkingCalendar();
    onGetMissingPersonCase();
    onGetShiftUser();
  }

  void onUpdateSeeAll(){
    isSeeAll = !isSeeAll;
    if(isSeeAll){
      isLoadingOtherShift = true;
      onGetAllShifts();
    }
    update();
  }

  // void onGetWorkingCalendar() =>
  //     repositories.onGetWorkingCalendar().then((value) {
  //       return contract.onGetWorkingCalendarSuccess(value);
  //     }).catchError((onError) {
  //       return contract.onError(onError);
  //     });

  void onGetMissingPersonCase() =>
      homeRepositories.onGetLackShiftUser().then((value) {
        return contract.onGetLackShiftUser(value);
      }).catchError((onError) {
        return contract.onError(onError);
      });

  void onGetShiftUser() => homeRepositories.onGetShiftUser().then((value) {
    return contract.onGetUserShift(value);
  }).catchError((onError) {
    return contract.onError(onError);
  });

  void onGetAllShifts() {

    DateTime dateNextWeek = DateTime.now().add(Duration(days: 7));
    String dateRequest = DateFormat("yyyy-MM-dd").format(DateTime.now());
    String branchId;
    if(StorageInfoUser()?.getInfoUser?.branchName != null && StorageInfoUser().getInfoUser.branchName.isNotEmpty){
      branchId = StorageInfoUser()?.getInfoUser?.branchName[0].id.toString();
    }
    homeRepositories.onGetOtherShift(dateRequest, branchId).then((value) {
      return contract.onGetAllShiftsSuccess(value);
    }).catchError((onError) {
      return contract.onError(onError);
    });
  }

  Future<void> onRefresh() async{
    isSeeAll = false;
    onGetMissingPersonCase();
    onGetShiftUser();
  }

  @override
  void onError(Exception exception) {
    // TODO: implement onGetWorkingCalendarError

  }

  @override
  void onGetLackShiftUser(LackShiftUserResponse response) {
    // TODO: implement onGetLackShiftUser
    mLackShiftUser = response;
    update();
  }

  @override
  void onGetUserShift(ShiftUserResponse response) {
    // TODO: implement onGetMyCaseSuccess
    mShiftUser = response;
    update();
  }

  @override
  void onGetAllShiftsSuccess(List<OtherShiftResponse> response) {
    // TODO: implement onGetAllShiftsSuccess
    if (response.isEmpty) {
      // Get.dialog(MessageDialog(
      //   title: error_text,
      //   description: "Bạn không thuộc chi nhánh nào.",
      // ));
      Get.snackbar(null, no_data_available, snackPosition: SnackPosition.BOTTOM);
    }
    mOtherShift = response;
    isLoadingOtherShift = false;
    update();
  }
}
