import 'package:be_staff/data/response/busy_time_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/shift/controller/busy_calendar_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusyView extends GetView {
  final BusyTimeResponse busyTimes;

  BusyView(this.busyTimes);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        busyTimes != null ? Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: _ListBusyTimeView(busyTimes.t2),),
              Expanded(child: _ListBusyTimeView(busyTimes.t3),),
              Expanded(child: _ListBusyTimeView(busyTimes.t4),),
              Expanded(child: _ListBusyTimeView(busyTimes.t5),),
              Expanded(child: _ListBusyTimeView(busyTimes.t6),),
              Expanded(child: _ListBusyTimeView(busyTimes.t7),),
              Expanded(child: _ListBusyTimeView(busyTimes.cn),),

            ],
          ),
        ) : Center(child: CircularProgressIndicator(),),
      ],
    );
  }

}

class _ListBusyTimeView extends GetView<ListBusyTimeController> {
  final BusyCalendar busyCalendar;

  _ListBusyTimeView(this.busyCalendar);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<ListBusyTimeController>(
      builder: (value) => ListView.builder(
        itemBuilder: (context, index) {
          BusyTime _busy = busyCalendar.busyTimes[index];
          bool isBusy = busyCalendar.busyTimes[index].busy != null;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                child: InkWell(
                  onTap: () {
                    if(_busy.busy != null){
                      _busy.busy = null;
                    }else{
                      _busy.busy = 1;
                    }
                    controller.update();
                    controller.onCreateList(_busy);
                    BusyController busyController = Get.find();
                    busyController.onSelectBusyCalendar(controller.busyTimeList, busyCalendar.date);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                    decoration: BoxDecoration(
                      color: isBusy ? ColorHelper.parseColor("#FF7A7A"):ColorHelper.parseColor("#FBFBFB"),
                      borderRadius: BorderRadius.circular(5),
                      // gradient: isBusy ? LinearGradient(
                      //   begin: Alignment.topLeft,
                      //   end: Alignment(-0.9, -0.9),
                      //   stops: [0.0, 0.5, 0.5, 1],
                      //   colors: [
                      //     ColorHelper.parseColor("#FF7575"),
                      //     ColorHelper.parseColor("#FF7575"),
                      //     Colors.white,
                      //     Colors.white,
                      //     // Colors.orange,
                      //     // Colors.orange,
                      //   ],
                      //   tileMode: TileMode.repeated,
                      // ) : null,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        isBusy ? Icon(Icons.radio_button_checked, size:  12,) : Icon(Icons.radio_button_unchecked, color: ColorHelper.parseColor("#BCB9B9"), size: 12,),
                        TextCustomized(
                          text: _busy.startTime,
                          color:ColorHelper.parseColor("#30242A"),
                          maxLine: 1,
                        ),
                        TextCustomized(
                          text: _busy.endTime,
                          color:ColorHelper.parseColor("#30242A"),
                          maxLine: 1,
                        ),
                        TextCustomized(
                          text: isBusy ? "Bận" : "",
                          color: ColorHelper.parseColor("#30242A"),
                          weight: FontWeight.bold,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              )
            ],
          );
        },
        shrinkWrap: true,
        itemCount: busyCalendar.busyTimes.length,
        physics: ClampingScrollPhysics(),
      ),
    );
  }
}

class ListBusyTimeController extends GetxController {
  List<BusyTime> busyTimeList;

  void onCreateList(BusyTime data){
    if(busyTimeList == null){
      busyTimeList = List();
    }
    if (data.busy != null) {
      busyTimeList.add(data);
    }
    update();
  }
}
