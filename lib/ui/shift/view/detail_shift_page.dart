import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/shift/controller/detail_shift_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailShiftPage extends GetView<DetailShiftController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<DetailShiftController>(
      builder: (value) => InitialWidget(
        titleAppBar: detail_shift,
        isCenterTitleAppBar: false,
        leadingIconAppBar: InkWell(
            onTap: () => Get.back(),
            child: Icon(Icons.arrow_back, color: blueMainColorApp)),
        backgroundAppBar: Colors.transparent,
        body: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Stack(
                      children: [
                        ImageCustomized(path: bg_detail_shift, width: Get.width, height: 50, fit: BoxFit.cover,),
                        Positioned(
                          top: 0,
                          bottom: 0,
                          child: Align(
                            alignment: Alignment.center,
                            child: TextCustomized(
                              text: controller.detailShift.dataItem.dataUser[0].position,
                              size: bigSize,
                              weight: FontWeight.w600,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: ListView(
                        children: [

                          SizedBox(height: 25,),
                          RichText(text: TextSpan(
                              text: "Ca A",
                              style: TextStyle(
                                  fontSize: mediumSize,
                                  color: Colors.black
                              ),
                              children: [
                                TextSpan(
                                  text: " - ${controller.detailShift.nameDay}, ${controller.detailShift.day}",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14
                                  ),
                                )
                              ]
                          )),
                          SizedBox(height: 20,),
                          Divider(height: 1, color: ColorHelper.parseColor("#F0F0F0"),),
                          SizedBox(height: 20,),
                          RichText(text: TextSpan(
                              text: "${controller.detailShift.dataItem.startTime} - ${controller.detailShift.dataItem.endTime}",
                              style: TextStyle(
                                  fontSize: mediumSize,
                                  color: Colors.black
                              ),
                              children: [
                                TextSpan(
                                  text: " - 5.5 giờ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w300
                                  ),
                                )
                              ]
                          )),
                          SizedBox(height: 20,),
                          Divider(height: 1, color: ColorHelper.parseColor("#F0F0F0"),),
                          // TextCustomized(
                          //   text: state_shift,
                          //   color: blueMainColorApp,
                          //   weight: FontWeight.w300,
                          // ),
                          // SizedBox(height: 20,),
                          // TextCustomized(
                          //   text: "Bạn đã hoán đổi ca này cho XXX và đang chờ cô ấy xác nhận",
                          //   color: blueMainColorApp,
                          //   weight: FontWeight.w300,
                          // ),
                          SizedBox(height: 20,),
                          Divider(height: 1, color: ColorHelper.parseColor("#F0F0F0"),),
                          SizedBox(height: 20,),
                          RichText(text: TextSpan(
                              text: controller.detailShift.dataItem.dataUser[0].namePosition,
                              style: TextStyle(
                                  fontSize: mediumSize,
                                  color: Colors.black
                              ),
                              children: [
                                TextSpan(
                                  text: " - 5.5 giờ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w300
                                  ),
                                )
                              ]
                          )),
                          // SizedBox(height: 20,),
                          // TextCustomized(
                          //   text: "Ghi chú của Ca: “Hôm nay có khách đoàn đông hơn thông thường chút nên mọi người ăn uống đầy đủ trước đi đi làm nhé”",
                          //   weight: FontWeight.w300,
                          // ),
                          SizedBox(height: 20,),
                          Divider(height: 1, color: ColorHelper.parseColor("#F0F0F0"),),
                          TextCustomized(
                            text: teammate,
                            weight: FontWeight.w300,
                          ),
                          _buildEmployeeOfShiftGrid(),
                          Divider(height: 1, color: ColorHelper.parseColor("#F0F0F0"),),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            _buildBottomRowButton()
          ],
        ),
      ),
    );
  }

  Widget _buildBottomRowButton() => Container(
    padding: EdgeInsets.all(14),
    decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 5,
          )
        ]
    ),
    child: Row(
      children: [
        Expanded(child: Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.blue,
                  width: 1
              ),
              borderRadius: BorderRadius.circular(100)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageCustomized(path: ic_watch, width: 14, height: 14, color: blueMainColorApp,),
              SizedBox(width: 4,),
              TextCustomized(text: point_in, color: blueMainColorApp, weight: FontWeight.w600, size: mediumSize,)
            ],
          ),
        )),
        SizedBox(width: 8,),
        // Expanded(child: Container(
        //   padding: EdgeInsets.all(12),
        //   decoration: BoxDecoration(
        //       border: Border.all(
        //           color: Colors.blue,
        //           width: 1
        //       ),
        //       borderRadius: BorderRadius.circular(100)
        //   ),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       ImageCustomized(path: ic_watch, width: 14, height: 14, color: blueMainColorApp,),
        //       SizedBox(width: 4,),
        //       TextCustomized(text: change_shift, color: blueMainColorApp, weight: FontWeight.w600, size: mediumSize,)
        //     ],
        //   ),
        // )),
        // SizedBox(width: 8,),
        Expanded(child: InkWell(
          onTap: () => Get.bottomSheet(_buildBottomSheet()),
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.blue,
                    width: 1
                ),
                borderRadius: BorderRadius.circular(100)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ImageCustomized(path: ic_watch, width: 14, height: 14, color: blueMainColorApp,),
                SizedBox(width: 4,),
                TextCustomized(text: concession_shift, color: blueMainColorApp, weight: FontWeight.w600, size: mediumSize,)
              ],
            ),
          ),
        )),
      ],
    ),
  );


  Widget _buildEmployeeOfShiftGrid() => GridView.builder(
      shrinkWrap: true,
      itemCount: controller.detailShift.dataItem.dataUser.length,
      padding: EdgeInsets.symmetric(vertical: 16),
      physics: ClampingScrollPhysics(),
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: (Get.width/2)/34, mainAxisSpacing: 10),
      itemBuilder: (context, position) {
        final String avatar = controller.detailShift.dataItem.dataUser[position].avatar;
        bool isUrl = avatar != null ? Uri.parse(avatar).isAbsolute : false;
        Widget _itemAvatar;
        if(avatar != null && avatar != "" && isUrl){
          _itemAvatar = CircleImage.network(
            path: avatar,
            insideRadius: 20,
          );
        }else{
          final String fullName = controller.detailShift.dataItem.dataUser[position].fullName;
          List<String> stringNames = fullName.split(" ");
          var text;
          if(stringNames.length <= 1){
            text = fullName[0].toUpperCase() + fullName[1];
          }else{
            text = stringNames[stringNames.length - 2][0] +
                stringNames[stringNames.length - 1][0];
          }
          _itemAvatar = CircleAvatar(
            backgroundColor: ColorHelper.parseColor("#E1E1E1"),
            radius: 20,
            child: TextCustomized(
              text: text,
              size: smallSize,
            ),
          );
        }

        return Row(
          children: [
            Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: [
                    ColorHelper.parseColor("#7433FF"),
                    ColorHelper.parseColor("#FFA3FD"),
                  ])),
              child: _itemAvatar,
            ),
            SizedBox(
              width: 10,
            ),
            TextCustomized(
              text: controller.detailShift.dataItem.dataUser[position].fullName,
              weight: FontWeight.w300,
            )
          ],
        );
      });

  Widget _buildBottomSheet() => Container(
    decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(16),
          topLeft: Radius.circular(16),
        )),
    padding: EdgeInsets.all(20),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Align(
                alignment: Alignment.center,
                child: TextCustomized(
                  text: choose_person_to_change_shift,
                  size: largeSize,
                  weight: FontWeight.w600,
                  isCenter: true,
                ),
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: InkWell(
                onTap: ()=> Get.back(),
                child: Icon(
                  Icons.close,
                  size: 20,
                ),
              ),
            )
          ],
        ),
        Divider(color: ColorHelper.parseColor("#F0F0F0"), height: 1,),
        SizedBox(height: 20,),
        SizedBox(
          height: 200,
          child: controller.userCedeShifts != null && controller.userCedeShifts.isNotEmpty ? ListView.builder(itemBuilder: (context, position) {
            final String avatar = controller.userCedeShifts[position].avatar;
            Widget _itemAvatar;
            if(avatar != null && avatar != ""){
              _itemAvatar = CircleImage.network(
                path: avatar,
                insideRadius: 20,
              );
            }else{
              final String fullName = controller.detailShift.dataItem.dataUser[position].fullName;
              List<String> stringNames = fullName.split(" ");
              var text;
              if(stringNames.length <= 1){
                text = fullName[0].toUpperCase() + fullName[1];
              }else{
                text = stringNames[stringNames.length - 2][0] +
                    stringNames[stringNames.length - 1][0];
              }
              _itemAvatar = CircleAvatar(
                backgroundColor: ColorHelper.parseColor("#E1E1E1"),
                radius: 20,
                child: TextCustomized(
                  text: text,
                  size: smallSize,
                ),
              );
            }
            return Column(
              children: [
                InkWell(
                  onTap: () {
                    if(Get.isDialogOpen || Get.isBottomSheetOpen){
                      Get.back();
                    }
                    Get.dialog(_buildConfirmDialog(position));
                  },
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              ColorHelper.parseColor("#7433FF"),
                              ColorHelper.parseColor("#FFA3FD"),
                            ])),
                        child: _itemAvatar,
                      ),
                      SizedBox(width: 4,),
                      Expanded(child: TextCustomized(
                        text: controller.userCedeShifts[position].fullName,
                        weight: FontWeight.w300,
                      )),
                      SizedBox(width: 4,),
                    ],
                  ),
                ),
                SizedBox(height: 20,)
              ],
            );
          }, itemCount: controller.userCedeShifts.length,) : Center(
            child: TextCustomized(
              text: no_data_available,
            ),
          ),
        ),
        ButtonCustomized(
          text: done_text,
          colorText: blueMainColorApp,
          backgroundColor: ColorHelper.parseColor("#C3E6FF"),
          width: Get.width,
          borderRadius: 100,
          margin: EdgeInsets.symmetric(horizontal: Get.width/4),
        )
      ],
    ),
  );

  Widget _buildConfirmDialog(int position) => MessageDialog(
    title: concession_shift,
    description: concession_shift_confirm,
    onPressedOK: () => controller.onChooseCedeShift(position),
    isShowCancel: true,
  );
}