import 'package:be_staff/data/response/lack_shift_user_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/shift/controller/lack_people_controller.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LackPeopleShiftView extends GetView{
  final LackShiftUserResponse lackOfPeople;

  LackPeopleShiftView(this.lackOfPeople);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextCustomized(
            text: miss_case_to_get,
            size: smallSize,
            weight: FontWeight.w600,
            color: Colors.black,
          ),
          SizedBox(height: 10,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[0])),
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[1])),
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[2])),
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[3])),
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[4])),
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[5])),
              Expanded(child: _LackOfPeopleView(lackOfPeople.lackShiftUser[6])),
            ],
          ),
        ],
      ),
    );
  }

}

class _LackOfPeopleView extends GetView<LackPeopleController> {
  final LackShiftUser lackOfPeopleShift;

  _LackOfPeopleView(this.lackOfPeopleShift);
  final List<LackCanReceive> mLackOfPeopleShiftCanReceive = List();

  @override
  Widget build(BuildContext context) {
    onSetData();
    return GetBuilder<LackPeopleController>(
      builder:(value) => ListView.builder(

        itemBuilder: (context, position) {
          final lack = mLackOfPeopleShiftCanReceive[position];
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                child: InkWell(
                  onTap: () => Get.dialog(_buildDialog(lack)),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                    decoration: BoxDecoration(
                        color: ColorHelper.parseColor("#FBFBFB"),
                        borderRadius: BorderRadius.circular(5),
                        gradient:  LinearGradient(
                            colors: [
                              ColorHelper.parseColor("#FF284F"),
                              ColorHelper.parseColor("#FF6969"),
                              ColorHelper.parseColor("#FF6A99")
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextCustomized(
                          text: lack.startTime,
                          color: Colors.white,
                          maxLine: 1,
                          size: smallSize,
                        ),
                        TextCustomized(
                          text: lack.endTime,
                          color: Colors.white,
                          maxLine: 1,
                          size: smallSize,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            ImageCustomized(
                              path: ic_people,
                              width: 14,
                              height: 14,
                            ),
                            TextCustomized(text: lack.user.toString(), color: Colors.white, weight: FontWeight.w500,)
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              )
            ],
          );
        },
        shrinkWrap: true,
        itemCount: mLackOfPeopleShiftCanReceive.length,
        physics: ClampingScrollPhysics(),
      ),
    );
  }

  Widget _buildDialog(LackCanReceive lack) {
    final bool canReceive = lack.action == 1;
    return MessageDialog(
      title: canReceive ? receive_shift : cancel_receive_shift,
      description: canReceive ? receive_shift_confirm : receive_cancel_shift_confirm,
      onPressedOK: () => canReceive ? controller.onReceive(lack.shiftDetailId, lack.positionId) : controller.onCancelShift(lack.shiftDetailUserId),
      isShowCancel: true,
    );
  }

  void onSetData() {
    mLackOfPeopleShiftCanReceive.clear();
    lackOfPeopleShift.dataItem.forEach((element) {
      element.dataPosition.forEach((element2) {
        if (element2.action != 0) {
          mLackOfPeopleShiftCanReceive.add(LackCanReceive(
              totalHours: element.totalHours,
              endTime: element.endTime,
              startTime: element.startTime,
              color: element2.color,
              action: element2.action,
              namePosition: element2.namePosition,
              position: element2.position,
              positionId: element2.positionId,
              shiftDetailId: element2.shiftDetailId,
              shiftDetailUserId: element2.shiftDetailUserId,
              user: element2.user
          ));
        }
      });
    });
  }
}

