import 'dart:math';

import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/response/other_shift_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OtherShiftView extends GetView {
  final OtherShiftResponse response;

  OtherShiftView(this.response);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(colors: [
                          ColorHelper.parseColor("#7433FF"),
                          ColorHelper.parseColor("#FFA3FD"),
                        ])),
                    child: StorageInfoUser().getInfoUser.avatar != null &&
                        StorageInfoUser().getInfoUser.avatar != ""
                        ? CircleImage.network(
                      path: "http://"+StorageInfoUser().getInfoUser.avatar,
                      insideRadius: 10,
                    )
                        : CircleImage(
                      path: ic_avatar_example,
                      insideRadius: 10,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  TextCustomized(
                    text: response.fullName,
                  ),
                  Expanded(
                    child: Container(),
                  ),
                ],
              ),
              _OtherView(response),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 12),
          color: ColorHelper.parseColor("#E1E1E1"),
          height: 1,
          width: double.infinity,
        )
      ],
    );
  }
}

class _OtherView extends GetView {
  final OtherShiftResponse otherShiftResponse;

  _OtherView(this.otherShiftResponse);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(child: _View(otherShiftResponse.dataDate[0])),
        Expanded(child: _View(otherShiftResponse.dataDate[1])),
        Expanded(child: _View(otherShiftResponse.dataDate[2])),
        Expanded(child: _View(otherShiftResponse.dataDate[3])),
        Expanded(child: _View(otherShiftResponse.dataDate[4])),
        Expanded(child: _View(otherShiftResponse.dataDate[5])),
        Expanded(child: _View(otherShiftResponse.dataDate[6])),
      ],
    );
  }

}

class _View extends GetView {
  final DataDate dataDate;

  _View(this.dataDate);

  @override
  Widget build(BuildContext context) {
    return dataDate.dataItem.isNotEmpty ? ListView.builder(
      itemBuilder: (context, index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Card(
              child: InkWell(
                onTap: () => Get.log("$index"),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                  decoration: BoxDecoration(
                    color: ColorHelper.parseColor("#FBFBFB"),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextCustomized(
                        text: dataDate.dataItem[index].start,
                        color: ColorHelper.parseColor("#AAAAAA").withOpacity(0.77),
                        maxLine: 1,
                        size: smallSize,
                      ),
                      TextCustomized(
                        text: dataDate.dataItem[index].end,
                        color: ColorHelper.parseColor("#AAAAAA").withOpacity(0.77),
                        maxLine: 1,
                        size: smallSize,
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //For next version

                      // Align(
                      //   alignment: Alignment.centerRight,
                      //   child: ImageCustomized(
                      //     path: Random().nextInt(100) % 2 == 0
                      //         ? ic_checked
                      //         : ic_missing,
                      //     width: 10,
                      //     height: 10,
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            )
          ],
        );
      },
      shrinkWrap: true,
      itemCount: dataDate.dataItem.length,
      physics: ClampingScrollPhysics(),
    ) : Container();
  }
}
