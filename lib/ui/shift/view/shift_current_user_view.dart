import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/data/response/shift_response/shift_user_response.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/shift/view/update_busy_calendar_page.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/circle_image.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShiftCurrentUserView extends GetView {
  final ShiftUserResponse response;

  ShiftCurrentUserView(this.response);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(colors: [
                          ColorHelper.parseColor("#7433FF"),
                          ColorHelper.parseColor("#FFA3FD"),
                        ])),
                    child: StorageInfoUser().getInfoUser.avatar != null &&
                        StorageInfoUser().getInfoUser.avatar != ""
                        ? CircleImage.network(
                      path: "http://"+StorageInfoUser().getInfoUser.avatar,
                      insideRadius: 10,
                    )
                        : CircleImage(
                      path: ic_avatar_example,
                      insideRadius: 10,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  TextCustomized(
                    text: StorageInfoUser().getInfoUser.fullName,
                    weight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  TextCustomized(
                    text: " (${response.totalHours.toString()}h)",
                    weight: FontWeight.w100,
                  ),
                  Expanded(child: SizedBox()),
                  InkWell(
                    onTap: () => Get.to(UpdateBusyCalendarPage()),
                    child: Row(
                      children: [
                        TextCustomized(
                          text: add_busy_time,
                          color: ColorHelper.parseColor("#F92828"),
                          weight: FontWeight.w600,
                          size: smallSize,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        ImageCustomized(
                          path: ic_plus_shift,
                          width: 19,
                          height: 19,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 10,),
              _ShiftCurrentUserView(response),
            ],
          ),
        ),

      ],
    );
  }
}

class _ShiftCurrentUserView extends GetView {
  final ShiftUserResponse shift;

  _ShiftCurrentUserView(this.shift);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(child: _View(shift.shiftUser[0])),
        Expanded(child: _View(shift.shiftUser[1])),
        Expanded(child: _View(shift.shiftUser[2])),
        Expanded(child: _View(shift.shiftUser[3])),
        Expanded(child: _View(shift.shiftUser[4])),
        Expanded(child: _View(shift.shiftUser[5])),
        Expanded(child: _View(shift.shiftUser[6])),
      ],
    );
  }

}
class _View extends GetView {
  final ShiftUser myCase;

  _View(this.myCase);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Card(
              child: InkWell(
                onTap: () => Get.log("$index"),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                  decoration: BoxDecoration(
                    color: ColorHelper.parseColor("#FBFBFB"),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextCustomized(
                        text: myCase.dataItem[index].startTime,
                        color: ColorHelper.parseColor("#AAAAAA").withOpacity(0.77),
                        maxLine: 1,
                        size: smallSize,
                      ),
                      TextCustomized(
                        text: myCase.dataItem[index].endTime,
                        color: ColorHelper.parseColor("#AAAAAA").withOpacity(0.77),
                        maxLine: 1,
                        size: smallSize,
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //For next version

                      // Align(
                      //   alignment: Alignment.centerRight,
                      //   child: ImageCustomized(
                      //     path: Random().nextInt(100) % 2 == 0
                      //         ? ic_checked
                      //         : ic_missing,
                      //     width: 10,
                      //     height: 10,
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            )
          ],
        );
      },
      shrinkWrap: true,
      itemCount: myCase.dataItem.length,
      physics: ClampingScrollPhysics(),
    );
  }
}
