import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/shift/controller/shift_controller.dart';
import 'package:be_staff/ui/shift/view/other_shift_view.dart';
import 'package:be_staff/ui/shift/view/shift_current_user_view.dart';
import 'package:be_staff/ui/shift/view/lack_people_shift_view.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

class ShiftPage extends GetView<ShiftController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InitialWidget(
      centerAppBar: TextCustomized(
        text: StorageInfoUser().getInfoUser.branchName[0].name,
        color: ColorHelper.parseColor("#838383"),
        size: smallSize,
      ),
        bottomAppbar: PreferredSize(
          preferredSize: Size(Get.width, 80),
          child: TableCalendar(
            calendarController: controller.calendarController,
            startingDayOfWeek: StartingDayOfWeek.monday,
            headerVisible: false,
            initialCalendarFormat: CalendarFormat.week,
            calendarStyle: CalendarStyle(
              selectedColor: blueMainColorApp,
              todayColor: blueMainColorApp.withOpacity(0.5),
            ),
            availableGestures: AvailableGestures.none,
            locale: "vi",
          ),
        ),
        body: GetBuilder<ShiftController>(
      builder: (value) => RefreshIndicator(
        onRefresh: () => controller.onRefresh(),
        child: ListView(
          children: [
            // _buildHeader(),
            SizedBox(
              height: 18,
            ),
            _buildLackOfPeopleOfShift(),
            Divider(
              color: ColorHelper.parseColor("#E1E1E1"),
              height: 1,
            ),
            _buildShiftCurrentUser(),
            Divider(
              color: ColorHelper.parseColor("#E1E1E1"),
              height: 1,
            ),
            controller.isLoadingOtherShift && controller.isSeeAll? Center(child: CircularProgressIndicator()) :Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () => controller.onUpdateSeeAll(),
                  child: Row(
                    children: [
                      TextCustomized(text: controller.isSeeAll ? collapse_text :see_all_text, weight: FontWeight.w600),
                      Padding(
                        padding: EdgeInsets.all(4),
                        child: Icon(Icons.arrow_drop_down),
                      )
                    ],
                  ),
                ),
              ],
            ),
            controller.isSeeAll ? _buildShiftOfOtherPeople() : Container()
          ],
        ),
      ),
    ));
  }

  Widget _buildHeader() => Padding(
        padding: const EdgeInsets.only(top: 16, left: 24, right: 24),
        child: Row(
          children: [
            // InkWell(
            //   onTap: () => Get.snackbar(null, "This function is developing"),
            //   child: Row(
            //     children: [
            //       TextCustomized(
            //         text:"Tháng 12",
            //         color: ColorHelper.parseColor("#838383"),
            //         size: normalSize,
            //         weight: FontWeight.w500,
            //       ),
            //       Icon(Icons.arrow_drop_down)
            //     ],
            //   ),
            // ),
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: ()=> Get.snackbar(null, "This function is developing"),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextCustomized(
                          text: StorageInfoUser().getInfoUser.branchName[0].name,
                          color: ColorHelper.parseColor("#838383"),
                          size: smallSize,
                        ),
                        // Icon(
                        //   Icons.arrow_drop_down,
                        //   color: ColorHelper.parseColor("#838383"),
                        // )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // Row(
            //   children: [
            //     SizedBox(
            //       width: 12,
            //     ),
            //     InkWell(
            //       onTap: () =>
            //           Get.snackbar(null, "This function is developing"),
            //       child: ImageCustomized(
            //         path: ic_setting,
            //         width: 19,
            //         height: 19,
            //       ),
            //     ),
            //   ],
            // )
          ],
        ),
      );

  Widget _buildLackOfPeopleOfShift() {
    return controller.mLackShiftUser != null &&
            controller.mLackShiftUser.lackShiftUser.isNotEmpty
        ? ListView.builder(
            itemCount: 1,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemBuilder: (context, index) =>
                LackPeopleShiftView(controller.mLackShiftUser),
          )
        : Container(
            child: TextCustomized(
              text: no_lack_of_people_shift,
            ),
          );
  }

  Widget _buildShiftCurrentUser() {
    return controller.mShiftUser != null
        ? ListView.builder(
            itemCount: 1,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemBuilder: (context, index) => ShiftCurrentUserView(controller.mShiftUser),
          )
        : Container(
            child: TextCustomized(
              text: no_shift,
            ),
          );
  }

  Widget _buildShiftOfOtherPeople() {
    return controller.mOtherShift != null && controller.mOtherShift.isNotEmpty ? ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemBuilder: (context, position) =>
      controller.mOtherShift[position] != null
          ? OtherShiftView(controller.mOtherShift[position])
          : Container(),
      itemCount: controller.mOtherShift.length,
    ) : Container();
  }
}





