import 'package:be_staff/data/global/storage_info_user.dart';
import 'package:be_staff/helper/color_helper.dart';
import 'package:be_staff/ui/shift/controller/busy_calendar_controller.dart';
import 'package:be_staff/ui/shift/view/busy_view.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/dimens.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/image_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

class UpdateBusyCalendarPage extends GetView<BusyController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<BusyController>(
      builder: (value) => InitialWidget(
        leadingIconAppBar: Container(
          padding: EdgeInsets.all(12),
          child: InkWell(
            onTap: () => Get.back(),
            child: ImageCustomized(
              path: ic_back,
              width: 30,
              height: 30,
            ),
          ),
        ),
        centerAppBar: Column(
          children: [
            TextCustomized(
              text: controller.monthYearHeader,
              color: Colors.black,
              size: largeSize,
              weight: FontWeight.w500,
            ),
            SizedBox(height: 2,),
            TextCustomized(
              text: StorageInfoUser().getInfoUser.branchName[0].name,
              color: ColorHelper.parseColor("#B0B0B0"),
              size: smallSize,
            ),
          ],
        ),
        // actionsAppBar: [
        //   InkWell(
        //     onTap: () => Get.snackbar(null, "This function is developing"),
        //     child: ImageCustomized(
        //       path: ic_setting,
        //       width: 19,
        //       height: 19,
        //     ),
        //   ),
        // ],
        bottomAppbar: PreferredSize(
          preferredSize: Size(Get.width, 80),
          child: TableCalendar(
            calendarController: controller.calendarController,
            startingDayOfWeek: StartingDayOfWeek.monday,
            headerVisible: false,
            initialCalendarFormat: CalendarFormat.week,
            calendarStyle: CalendarStyle(
              selectedColor: blueMainColorApp,
              todayColor: blueMainColorApp.withOpacity(0.5)
            ),
            availableGestures: AvailableGestures.none,
            locale: "vi",
          ),
        ),
        body: Column(
          children: [
            SizedBox(height: 16,),
            Expanded(
              child: onBuildBusy(),
            ),
            ButtonCustomized(
              text: done_text,
              backgroundColor: Colors.white,
              colorText: blueMainColorApp,
              border: Border.all(
                color: blueMainColorApp,
                width: 1
              ),
              borderRadius: 100,
              width: Get.width,
              margin: EdgeInsets.only(left: Get.width/4, right: Get.width/4, bottom: 20),
              onPressed: () => Get.back(),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() => Column(
    children: [
      Padding(
        padding: const EdgeInsets.only(top: 16, left: 24, right: 24),
        child: Row(
          children: [
            InkWell(
              onTap: () => Get.back(),
              child: ImageCustomized(
                path: ic_back,
                width: 30,
                height: 30,
              ),
            ),
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextCustomized(
                        text: controller.monthYearHeader,
                        color: Colors.black,
                        size: largeSize,
                        weight: FontWeight.w500,
                      ),
                      Icon(
                        Icons.arrow_drop_down,
                        color: ColorHelper.parseColor("#838383"),
                      )
                    ],
                  ),
                ],
              ),
            ),
            // InkWell(
            //   onTap: () => Get.snackbar(null, "This function is developing"),
            //   child: ImageCustomized(
            //     path: ic_setting,
            //     width: 19,
            //     height: 19,
            //   ),
            // ),
          ],
        ),
      ),
      SizedBox(height: 2,),
      TextCustomized(
        text: StorageInfoUser().getInfoUser.branchName[0].name,
        color: ColorHelper.parseColor("#B0B0B0"),
        size: smallSize,
      ),
      SizedBox(height: 18,),
      TableCalendar(
        calendarController: controller.calendarController,
        startingDayOfWeek: StartingDayOfWeek.monday,
        headerVisible: false,
        initialCalendarFormat: CalendarFormat.week,
        calendarStyle: CalendarStyle(
          selectedColor: blueMainColorApp,
        ),
        availableGestures: AvailableGestures.none,
        locale: "vi",
      ),
      // onBuildNextWeekCalendar(),
    ],
  );

  Widget onBuildBusy() => BusyView(controller.mBusyTime);
}

