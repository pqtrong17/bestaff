import 'package:be_staff/ui/home/view/home_page.dart';
import 'package:be_staff/ui/upload_avatar/controller/upload_avatar_controller.dart';
import 'package:be_staff/values/colors.dart';
import 'package:be_staff/values/fonts.dart';
import 'package:be_staff/values/images.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/button_customized.dart';
import 'package:be_staff/widgets/original/initial_widget.dart';
import 'package:be_staff/widgets/original/text_customized.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class UploadAvatarPage extends GetView<UploadAvatarController> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    // TODO: implement build
    return GetBuilder<UploadAvatarController>(
        builder: (value) => InitialWidget(
            body: Directionality(
              textDirection: TextDirection.rtl,
              child: SingleChildScrollView(
                reverse: true,
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    _buildStepFirst(title_upload_avatar,
                        content_upload_avatar, screenWidth, screenHeight),
                    Container(
                        margin: EdgeInsets.only(
                            top: 40, left: 20, right: 20),
                        child: ButtonCustomized(
                          toUpperCase: false,
                          text: take_a_photo,
                          font: fBarlow,
                          weight: FontWeight.w600,
                          backgroundColor: blueMainColorApp,
                          width: screenWidth,
                          height: screenHeight / 16,
                          borderRadius: 240,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          onPressed: () {
                            Get.to(HomePage());
                          },
                        )),
                    SizedBox(height: 20),
                    TextCustomized(
                        text: select_gallery,
                        font: fInterSemiBold,
                        size: screenWidth * 0.04,
                        weight: FontWeight.w600,
                        color: blueMainColorApp,
                        isCenter: true)
                  ],
                ),
              ),
            )));
  }
  Widget _buildStepFirst(String title, String description,
      double screenWidth, double screenHeight) => Container(
    child: Column(
      children: [
        Container(
          width: screenWidth,
          height: screenHeight /1.35,
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextCustomized(
                  text: title,
                  font: fRobotoRegular,
                  size: screenWidth * 0.07,
                  weight: FontWeight.w500,
                  color: black,
                  isCenter: true),
              SizedBox(
                height: 10,
              ),
              TextCustomized(
                  text: description,
                  font: fBarlow,
                  size: screenWidth * 0.04,
                  weight: FontWeight.w500,
                  isCenter: true)
            ],
          ),
        )
      ],
    ),
  );

}
