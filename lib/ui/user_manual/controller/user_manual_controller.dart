import 'package:be_staff/data/injector.dart';
import 'package:be_staff/data/repositories/register_repositories.dart';
import 'package:be_staff/data/request/register_request.dart';
import 'package:be_staff/data/response/register_response.dart';
import 'package:be_staff/ui/login/view/login_page.dart';
import 'package:be_staff/ui/register/contract/register_contract.dart';
import 'package:be_staff/helper/extension.dart';
import 'package:be_staff/values/strings.dart';
import 'package:be_staff/widgets/original/message_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class UserManualController extends GetxController {
  int currentPage = 3;
  PageController pageController;
  Duration pageTurnDuration = Duration(milliseconds: 500);
  Curve pageTurnCurve = Curves.ease;

  @override
  void onInit() {
    super.onInit();
    pageController =
        PageController(viewportFraction: 1, initialPage: currentPage);
  }

  void goForward(int page) {
    currentPage = page;
    pageController.nextPage(duration: pageTurnDuration, curve: pageTurnCurve);
    update();
  }

  void onChangePage(int page) {
    currentPage = page;
    update();
  }

  void goBack() {
    pageController.previousPage(
        duration: pageTurnDuration, curve: pageTurnCurve);
    update();
  }

}
