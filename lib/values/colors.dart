import 'package:be_staff/helper/color_helper.dart';
import 'package:flutter/material.dart';

final Color blueMainColorApp = ColorHelper.parseColor("#28A0F9");
final Color bdGrayColor = ColorHelper.parseColor("#BDBDBD");
final Color f6GrayColor = ColorHelper.parseColor("#F6F6F6");
final Color e5GrayColor = ColorHelper.parseColor("#E5E5E5");
final Color b0GrayColor = ColorHelper.parseColor("#B0B0B0");

final Color white = ColorHelper.parseColor("#FFFFFF");
final Color black = ColorHelper.parseColor("#000000");
final Color ffRedColor = ColorHelper.parseColor("#FF0000");
final Color e8GrayColor = ColorHelper.parseColor("#E8E8E8");
final Color gray88Color = ColorHelper.parseColor("#888888");
final Color gray89Color = ColorHelper.parseColor("#898A8D");
final Color backgroundColor = ColorHelper.parseColor("#FFFFFF");
final Color efGrayColor = ColorHelper.parseColor("#EFEFEF");
final Color gray66Color = ColorHelper.parseColor("#666666");
final Color invalidColor = ColorHelper.parseColor("#FF0000");