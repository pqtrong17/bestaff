import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

enum _ResourceType { NETWORK, FILE, NORMAL }

class ImageCustomized extends StatelessWidget {
  final _ResourceType type;
  final String path;
  final double width;
  final double height;
  final BoxFit fit;
  final Color color;
  final double radius;
  final Color backgroundColor;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final String package;
  final File file;

  ImageCustomized(
      {@required this.path,
      this.width = 20,
      this.height,
      this.fit,
      this.color,
      this.backgroundColor,
      this.margin,
      this.padding,
      this.package,
      this.radius})
      : type = _ResourceType.NORMAL,
        file = null;

  ImageCustomized.network(
      {@required this.path,
      this.width = 20,
      this.height,
      this.fit,
      this.color,
      this.backgroundColor,
      this.margin,
      this.padding,
      this.package,
      this.radius})
      : type = _ResourceType.NETWORK,
        file = null;

  ImageCustomized.file({
    @required this.file,
    this.width = 20,
    this.height,
    this.fit,
    this.color,
    this.backgroundColor,
    this.margin,
    this.padding,
    this.package,
    this.radius,
  })  : type = _ResourceType.FILE,
        path = null;

  @override
  Widget build(BuildContext context) {
    return type == _ResourceType.NETWORK
        ? Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(radius ?? 0),
                color: backgroundColor ?? Colors.transparent),
            margin: margin ?? EdgeInsets.all(0),
            padding: padding ?? EdgeInsets.all(0),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(radius ?? 0),
                child: onBuildNetworkImage(context, path, width, height, fit)),
          )
        : Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(radius ?? 0),
                color: backgroundColor ?? Colors.transparent),
            margin: margin ?? EdgeInsets.all(0),
            padding: padding ?? EdgeInsets.all(0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(radius ?? 0),
              child: type == _ResourceType.NORMAL
                  ? Image.asset(
                      path,
                      width: width,
                      height: height,
                      fit: fit,
                      color: color,
                      package: package ?? null,
                    )
                  : Image.file(
                      file,
                      width: width,
                      height: height,
                      fit: fit,
                      color: color,
                    ),
            ),
          );
  }

  Widget onBuildNetworkImage(BuildContext context, String url, double width,
      double height, BoxFit fit) {
    return Container(
      child: FutureBuilder(
        future: cacheImage(url, context),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.none ||
              snapshot.hasError) {
            return Center(
              child: Text('Error'),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container(
              width: width,
              height: height,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (snapshot.data == false) {
            return Container(
              width: width,
              height: height,
              child: Center(
                child: Icon(
                  Icons.error,
                  color: Colors.red,
                  size: width,
                ),
              ),
            );
          }
          return Image.network(
            url,
            width: width,
            height: height,
            fit: fit,
            color: color,
          );
        },
      ),
    );
  }

  Future<bool> cacheImage(String url, BuildContext context) async {
    bool hasNoError = true;
    var output = Completer<bool>();
    precacheImage(
      NetworkImage(url),
      context,
      onError: (e, stackTrace) => hasNoError = false,
    ).then((_) => output.complete(hasNoError));
    return output.future;
  }
}
