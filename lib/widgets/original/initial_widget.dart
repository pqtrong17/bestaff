import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InitialWidget extends StatefulWidget {
  final Widget body;
  final Widget appBar;
  final String titleAppBar;
  final Widget centerAppBar;
  final bool isUpperCaseTitleAppBar;
  final TextStyle styleTitleAppBar;
  final InitialTypeWidget type;
  final bool isShowLeadingIconAppBar;
  final Widget leadingIconAppBar;
  final bool isCenterTitleAppBar;
  final Color backgroundColor;
  final Widget floatingActionButton;
  final Color backgroundAppBar;
  final Future<bool> Function() onWillPop;
  final List<Widget> actionsAppBar;

  //Avoid system intrusions at the Top, Bottom, Left or Right
  final bool isSystemIntrusionTop;
  final bool isSystemIntrusionBottom;
  final bool isSystemIntrusionLeft;
  final bool isSystemIntrusionRight;

  //Only iOS
  final Brightness statusBarBrightness;

  // If no appbar, it will work. If app bar appear, it's not working.

  //Color of Text, Icon on status bar.
  // Brightness.dark -> Text, icon color is black.
  // Brightness.light -> Text, icon is white
  final Brightness statusBarIconBrightness;

  // If no appbar, it will work. If app bar appear, it's not working.
  final Color statusBarColor;

  // If app bar appear, it will active. Opposite, brightness of status bar will active.
  final Brightness appBarBrightness;

  final GlobalKey<ScaffoldState> keyScaffold;
  final BottomNavigationBar bottomNavigationBar;
  final PreferredSizeWidget bottomAppbar;
  final bool isPaddingContentKeyboardAppear;

  InitialWidget(
      {@required this.body,
      this.floatingActionButton,
      this.appBar,
      this.titleAppBar,
      this.centerAppBar,
      this.isUpperCaseTitleAppBar = false,
      this.styleTitleAppBar,
      this.isShowLeadingIconAppBar = false,
      this.isCenterTitleAppBar = true,
      this.backgroundColor = Colors.white,
      this.backgroundAppBar = Colors.white,
      this.statusBarColor = Colors.transparent,
      this.onWillPop,
      this.leadingIconAppBar,
      this.isSystemIntrusionTop = true,
      this.isSystemIntrusionBottom = true,
      this.isSystemIntrusionLeft = true,
      this.isSystemIntrusionRight = true,
      this.statusBarIconBrightness,
      this.appBarBrightness,
      this.statusBarBrightness,
      this.keyScaffold,
      this.bottomNavigationBar,
      this.bottomAppbar,
      this.isPaddingContentKeyboardAppear = true,
      this.actionsAppBar,
      this.type = InitialTypeWidget.NORMAL});

  InitialWidget.scroll(
      {@required this.body,
      this.floatingActionButton,
      this.titleAppBar,
      this.centerAppBar,
      this.appBar,
      this.isUpperCaseTitleAppBar = false,
      this.styleTitleAppBar,
      this.isCenterTitleAppBar = true,
      this.isShowLeadingIconAppBar = false,
      this.leadingIconAppBar,
      this.backgroundColor = Colors.white,
      this.statusBarColor = Colors.transparent,
      this.backgroundAppBar = Colors.white,
      this.onWillPop,
      this.isSystemIntrusionTop = true,
      this.isSystemIntrusionBottom = true,
      this.isSystemIntrusionLeft = true,
      this.isSystemIntrusionRight = true,
      this.statusBarIconBrightness,
      this.appBarBrightness,
      this.statusBarBrightness,
      this.keyScaffold,
      this.bottomNavigationBar,
      this.bottomAppbar,
      this.isPaddingContentKeyboardAppear = true,
      this.actionsAppBar,
      this.type = InitialTypeWidget.SCROLL});

  InitialWidget.expand(
      {@required this.body,
      this.floatingActionButton,
      this.titleAppBar,
      this.centerAppBar,
      this.appBar,
      this.isUpperCaseTitleAppBar = false,
      this.styleTitleAppBar,
      this.leadingIconAppBar,
      this.isCenterTitleAppBar = true,
      this.isShowLeadingIconAppBar = false,
      this.backgroundColor = Colors.white,
      this.statusBarColor = Colors.transparent,
      this.backgroundAppBar = Colors.white,
      this.onWillPop,
      this.isSystemIntrusionTop = true,
      this.isSystemIntrusionBottom = true,
      this.isSystemIntrusionLeft = true,
      this.isSystemIntrusionRight = true,
      this.statusBarIconBrightness,
      this.appBarBrightness,
      this.statusBarBrightness,
      this.keyScaffold,
      this.bottomNavigationBar,
      this.bottomAppbar,
      this.isPaddingContentKeyboardAppear = true,
      this.actionsAppBar,
      this.type = InitialTypeWidget.EXPAND});

  @override
  _InitialWidgetState createState() => _InitialWidgetState();
}

class _InitialWidgetState extends State<InitialWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //Status bar
    final Color _statusBarColor = widget.statusBarColor;
    final Luminance _luminanceOfBackgroundStatusBar =
        widget.statusBarColor == Colors.transparent
            ? onTypeLuminance(widget.backgroundColor)
            : onTypeLuminance(widget.statusBarColor);
    final Brightness _statusBarBrightness = widget.statusBarBrightness;
    final Brightness _statusBarIconBrightness =
        widget.statusBarIconBrightness != null
            ? widget.statusBarIconBrightness
            : _luminanceOfBackgroundStatusBar == Luminance.DARK
                ? Brightness.light
                : Brightness.dark;

    //App bar
    Luminance _luminanceOfBackgroundAppBar = Luminance.LIGHT;

    //Brightness app bar
    Brightness _brightnessAppBar;
    if (widget.appBarBrightness == null) {
      if (widget.backgroundAppBar == Colors.transparent) {
        _luminanceOfBackgroundAppBar = onTypeLuminance(widget.backgroundColor);
      } else {
        _luminanceOfBackgroundAppBar = onTypeLuminance(widget.backgroundAppBar);
      }
      _brightnessAppBar = _luminanceOfBackgroundAppBar == Luminance.DARK
          ? Brightness.dark
          : Brightness.light;
    } else {
      _brightnessAppBar = widget.appBarBrightness;
    }

    //Leading icon app bar
    bool _isShowLeadingIconAppBar =
        widget.isShowLeadingIconAppBar || widget.leadingIconAppBar != null;

    final Widget _leadingAppBar = _isShowLeadingIconAppBar
        ? widget.leadingIconAppBar ??
            FlatButton(
              child: Icon(
                Platform.isIOS
                    ? Icons.arrow_back_ios_outlined
                    : Icons.arrow_back_outlined,
                size: 20,
                color: _brightnessAppBar == Brightness.dark
                    ? Colors.white
                    : Colors.black,
              ),
              onPressed: () => Navigator.pop(context),
            )
        : null;

    //Title app bar
    final String _title = widget.titleAppBar != null
        ? widget.isUpperCaseTitleAppBar
            ? widget.titleAppBar.toUpperCase()
            : widget.titleAppBar
        : null;
    final TextStyle _styleTitle = widget.styleTitleAppBar ??
        TextStyle(
            fontSize: 20,
            color: _luminanceOfBackgroundAppBar == Luminance.DARK
                ? Colors.white
                : Colors.black);
    final Widget _titleAppBar = widget.centerAppBar != null
        ? widget.centerAppBar
        : _title != null
            ? Text(
                _title,
                style: _styleTitle,
              )
            : null;

    //Action icons
    final List<Widget> _action = widget.actionsAppBar;

    //Appear appbar or no?
    bool _isShowAppBar =
        _titleAppBar != null || _isShowLeadingIconAppBar || _action != null;

    //Center title app bar or no?
    final bool _isCenterTitleAppBar = widget.isCenterTitleAppBar;

    //Background appbar
    final Color _backgroundAppBar = widget.backgroundAppBar;

    //Bottom appbar
    final PreferredSizeWidget _bottomAppBar = widget.bottomAppbar;

    //Elevation app bar
    double _elevation = _backgroundAppBar == Colors.transparent ? 0 : null;

    //Build app bar
    final Widget _appBar = widget.appBar != null
        ? widget.appBar
        : _isShowAppBar
            ? AppBar(
                brightness: _brightnessAppBar,
                centerTitle: _isCenterTitleAppBar,
                backgroundColor: _backgroundAppBar,
                bottom: _bottomAppBar,
                elevation: _elevation,
                leading: _leadingAppBar,
                title: _titleAppBar,
                actions: _action,
              )
            : null;

    final Widget _floatingActionButton = widget.floatingActionButton;
    final Color _backgroundColor = widget.backgroundColor;
    final Widget _bottomNavigationBar = widget.bottomNavigationBar;
    final Future<bool> Function() _onWillPop = widget.onWillPop;

    //Safe area
    final bool _isSystemIntrusionTop = widget.isSystemIntrusionTop;
    final bool _isSystemIntrusionBottom = widget.isSystemIntrusionBottom;
    final bool _isSystemIntrusionLeft = widget.isSystemIntrusionLeft;
    final bool _isSystemIntrusionRight = widget.isSystemIntrusionRight;

    //Type of this widget
    final InitialTypeWidget _type = widget.type;

    //Body
    final Widget _body = widget.body;

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarBrightness: _statusBarBrightness,
          statusBarColor: _statusBarColor,
          statusBarIconBrightness: _statusBarIconBrightness),
      child: Scaffold(
        key: widget.keyScaffold ?? null,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        floatingActionButton: _floatingActionButton,
        backgroundColor: _backgroundColor,
        appBar: _appBar,
        bottomNavigationBar: _bottomNavigationBar,
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: SafeArea(
              top: _isSystemIntrusionTop,
              bottom: _isSystemIntrusionBottom,
              left: _isSystemIntrusionLeft,
              right: _isSystemIntrusionRight,
              child: _type == InitialTypeWidget.NORMAL
                  ? _body
                  : _type == InitialTypeWidget.SCROLL
                      ? Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: SingleChildScrollView(
                            child: _body,
                            reverse: widget.isPaddingContentKeyboardAppear,
                          ),
                        )
                      : LayoutBuilder(builder: (context, constraints) {
                        return SingleChildScrollView(
                          reverse: widget.isPaddingContentKeyboardAppear,
                          child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minHeight: constraints.maxHeight),
                              child: IntrinsicHeight(
                                child: _body,
                              )),
                        );
                      })),
        ),
      ),
    );
  }

  //Dark or Light color
  Luminance onTypeLuminance(Color color) {
    if (color.computeLuminance() > 0.5) {
      return Luminance.LIGHT;
    }
    return Luminance.DARK;
  }
}

enum Luminance { DARK, LIGHT }

enum InitialTypeWidget { NORMAL, SCROLL, EXPAND }
