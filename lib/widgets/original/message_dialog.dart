import 'dart:ui';
import 'package:flutter/material.dart';

class MessageDialog extends StatefulWidget {
  final String title;
  final String description;
  final Widget titleWidget;
  final Widget contentWidget;
  final Widget firstButton;
  final Widget secondButton;
  final String textOK;
  final String textCancel;
  final Widget headerIcon;
  final Function onPressedOK;
  final Function onPressedCancel;
  final bool isShowCancel;
  final double iconSize;
  final double borderRadius;
  final double spacingHorizontal;
  final EdgeInsets paddingContent;
  final bool isCenterContent;

  const MessageDialog(
      {Key key,
      this.title,
      this.description = "You must have provide a message",
      this.textOK = "OK",
      this.headerIcon,
      this.isShowCancel = false,
      this.textCancel,
      this.onPressedCancel,
      this.iconSize = 100,
      this.borderRadius = 4,
      this.spacingHorizontal = 24,
      this.titleWidget,
      this.contentWidget,
      this.firstButton,
      this.secondButton,
      this.isCenterContent = false,
      this.paddingContent = const EdgeInsets.all(12),
      this.onPressedOK})
      : super(key: key);

  @override
  _MessageDialogState createState() => _MessageDialogState();
}

class _MessageDialogState extends State<MessageDialog> {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: onBuildContentBox(),
    );
  }

  onBuildContentBox() => Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.spacingHorizontal),
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.only(
                    top: widget.headerIcon != null
                        ? widget.iconSize / 2 + 25
                        : widget.paddingContent.top,
                    bottom: widget.paddingContent.bottom,
                    right: widget.paddingContent.right,
                    left: widget.paddingContent.left),
                margin: EdgeInsets.only(top: widget.iconSize / 2),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(widget.borderRadius),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: widget.isCenterContent
                      ? CrossAxisAlignment.center
                      : CrossAxisAlignment.start,
                  children: [
                    onBuildTitle(),
                    onBuildContent(),
                    SizedBox(
                      height: widget.headerIcon != null ? 22 : 0,
                    ),
                    onBuildActionButton()
                  ],
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                child: widget.headerIcon != null
                    ? ConstrainedBox(
                        constraints: BoxConstraints(
                            maxWidth: widget.iconSize,
                            maxHeight: widget.iconSize),
                        child: CircleAvatar(
                          backgroundColor: Theme.of(context).accentColor,
                          radius: widget.iconSize,
                          child: ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(widget.iconSize),
                              child: widget.headerIcon),
                        ),
                      )
                    : Container(),
              ),
            ],
          ),
        ),
      );

  Widget onBuildTitle() => widget.titleWidget ?? widget.title != null
      ? Column(
          children: [
            Text(
              widget.title,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 15,
            ),
          ],
        )
      : Container();

  Widget onBuildContent() =>
      widget.contentWidget ??
      Text(
        widget.description,
        style: TextStyle(
          fontSize: 16,
        ),
        textAlign: TextAlign.center,
      );

  Widget onBuildActionButton() => Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          widget.firstButton ??
              FlatButton(
                  onPressed: () {
                    if (widget.onPressedOK != null) {
                      widget.onPressedOK();
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text(
                    widget.textOK ?? "OK",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  )),
          widget.secondButton ??
                  widget.isShowCancel
              ? FlatButton(
                  onPressed: () {
                    if (widget.onPressedCancel != null) {
                      widget.onPressedCancel();
                    } else {
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text(
                    widget.textCancel ?? "Cancel",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ))
              : Container(),
        ],
      );
}
