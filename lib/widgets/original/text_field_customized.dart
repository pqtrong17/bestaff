import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldCustomized extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final TextStyle hintStyle;
  final TextStyle textStyle;
  final TextInputAction textInputAction;
  final TextInputType textInputType;
  final String errorText;
  final ValueChanged<String> onChanged;
  final int maxLength;
  final int maxLine;
  final FocusNode focusNode;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final BoxConstraints prefixIconConstraints;
  final BoxConstraints suffixIconConstraints;
  final String labelText;
  final String fontLabel;
  final double textSizeLabel;
  final Color colorTextLabel;
  final TextStyle styleLabelErrorText;
  final List<TextInputFormatter> inputFormatter;
  final Color backgroundColor;
  final bool isEnable;
  final double widthEnableBorder;
  final double widthDisableBorder;
  final double widthFocusedBorder;
  final double borderRadius;
  final Color colorBorderEnable;
  final Color colorBorderDisable;
  final Color colorBorderFocus;
  final bool isObscured;
  final bool Function() validator;
  final EdgeInsets contentPadding;
  final TextAlignVertical textAlignVertical;
  final TextAlign textAlign;
  final String obscuringCharacter;
  final Color colorSuffixFocus;
  final Color colorSuffixUnFocus;
  final ValueChanged<bool> onPressedSuffix;
  final ValueChanged<bool> onFocused;
  final ValueChanged<bool> onValidate;

  TextFieldCustomized(
      {@required this.controller,
      this.hintText,
      this.hintStyle,
      this.textStyle,
      this.textInputAction,
      this.textInputType,
      this.errorText,
      this.onChanged,
      this.maxLength,
      this.maxLine,
      this.focusNode,
      this.prefixIcon,
      this.suffixIcon,
      this.prefixIconConstraints,
      this.suffixIconConstraints,
      this.labelText,
      this.styleLabelErrorText,
      this.inputFormatter,
      this.isEnable,
      this.colorBorderDisable,
      this.colorBorderEnable,
      this.colorBorderFocus,
      this.widthDisableBorder,
      this.widthEnableBorder,
      this.isObscured,
      this.validator,
      this.colorTextLabel,
      this.textSizeLabel,
      this.fontLabel,
      this.contentPadding,
      this.obscuringCharacter = "*",
      this.textAlignVertical,
      this.textAlign,
      this.backgroundColor,
      this.widthFocusedBorder,
      this.colorSuffixFocus,
      this.colorSuffixUnFocus,
      this.onPressedSuffix,
      this.borderRadius,
      this.onValidate,
      this.onFocused});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TextFieldCustomizedState();
  }
}

class _TextFieldCustomizedState extends State<TextFieldCustomized> {
  bool isValid;
  bool isFocus;
  FocusNode focusNode;
  bool isClick = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.focusNode != null) {
      focusNode = widget.focusNode;
    } else {
      focusNode = FocusNode();
    }
    focusNode.addListener(() {
      setState(() {
        isFocus = focusNode.hasFocus;
      });
      if (widget.onFocused != null) {
        widget.onFocused(focusNode.hasFocus);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextField(
      textAlign: widget.textAlign ?? TextAlign.left,
      maxLines: widget.isObscured != null && widget.isObscured
          ? 1
          : widget.maxLine ?? 1,
      // Obscured fields cannot be multiline.
      focusNode: focusNode,
      maxLength: widget.maxLength ?? null,
      obscureText: widget.isObscured ?? false,
      obscuringCharacter: widget.obscuringCharacter,
      controller: widget.controller,
      style: widget.textStyle,
      textAlignVertical: widget.textAlignVertical,
      textInputAction: widget.textInputAction ?? null,
      onSubmitted: (_) {
        if (widget.textInputAction != null) {
          switch (widget.textInputAction) {
            case TextInputAction.none:
              // TODO: Handle this case.
              break;
            case TextInputAction.unspecified:
              // TODO: Handle this case.
              break;
            case TextInputAction.done:
              // TODO: Handle this case.
              focusNode.unfocus();
              break;
            case TextInputAction.go:
              // TODO: Handle this case.
              break;
            case TextInputAction.search:
              // TODO: Handle this case.
              break;
            case TextInputAction.send:
              // TODO: Handle this case.
              break;
            case TextInputAction.next:
              // TODO: Handle this case.
              focusNode.nextFocus();
              break;
            case TextInputAction.previous:
              // TODO: Handle this case.
              break;
            case TextInputAction.continueAction:
              // TODO: Handle this case.
              break;
            case TextInputAction.join:
              // TODO: Handle this case.
              break;
            case TextInputAction.route:
              // TODO: Handle this case.
              break;
            case TextInputAction.emergencyCall:
              // TODO: Handle this case.
              break;
            case TextInputAction.newline:
              // TODO: Handle this case.
              break;
          }
        }
      },
      onChanged: (value){
        widget.onChanged(value);
      },
      keyboardType: widget.textInputType ?? null,
      enabled: widget.isEnable ?? true,
      inputFormatters: widget.inputFormatter ?? null,
      decoration: InputDecoration(
        filled: true,
        fillColor: widget.backgroundColor ?? Colors.white,
        labelText: widget.labelText ?? null,
        labelStyle: TextStyle(
            fontFamily: widget.fontLabel,
            fontSize: widget.textSizeLabel,
            color: widget.colorTextLabel),
        prefixIcon: widget.prefixIcon ?? null,
        suffixIcon: widget.suffixIcon ?? null,
        suffixIconConstraints: widget.suffixIconConstraints ??
            BoxConstraints(
              maxHeight: 48,
              maxWidth: 48,
            ),
        prefixIconConstraints: widget.prefixIconConstraints ??
            BoxConstraints(
              maxHeight: 48,
              maxWidth: 48,
            ),
        counterText: '',
        hintText: widget.hintText ?? null,
        hintStyle: widget.hintStyle,
        contentPadding: widget.contentPadding,
        isDense: true,
        errorText: (widget.validator != null && widget.validator()) ? widget.errorText ?? null : null,
        errorStyle: widget.styleLabelErrorText,
        errorBorder: OutlineInputBorder(
          borderRadius: widget.borderRadius != null
              ? BorderRadius.circular(widget.borderRadius)
              : BorderRadius.circular(10),
          borderSide: BorderSide(color: Theme.of(context).errorColor),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: widget.borderRadius != null
              ? BorderRadius.circular(widget.borderRadius)
              : BorderRadius.circular(10),
          borderSide: BorderSide(color: Theme.of(context).errorColor),
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: widget.borderRadius != null
                ? BorderRadius.circular(widget.borderRadius)
                : BorderRadius.circular(10),
            borderSide: BorderSide(
                color: widget.colorBorderFocus ?? Theme.of(context).accentColor,
                width: widget.widthFocusedBorder ?? 1)),
        disabledBorder: OutlineInputBorder(
            borderRadius: widget.borderRadius != null
                ? BorderRadius.circular(widget.borderRadius)
                : BorderRadius.circular(10),
            borderSide: BorderSide(
                color: widget.colorBorderDisable ??
                    Theme.of(context).disabledColor,
                width: widget.widthDisableBorder ?? 1)),
        enabledBorder: OutlineInputBorder(
            borderRadius: widget.borderRadius != null
                ? BorderRadius.circular(widget.borderRadius)
                : BorderRadius.circular(10),
            borderSide: BorderSide(
                color:
                    widget.colorBorderEnable ?? Theme.of(context).disabledColor,
                width: widget.widthEnableBorder ?? 1)),
      ),
    );
  }
}
